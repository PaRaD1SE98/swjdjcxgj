import os
import datetime
import requests
import xlsxwriter
from login import write_cookie
from csv_reader import read_csv
import traceback

from process_source import write_excel

Host = 'http://app2.dzgzpt.gdgs.tax'
affair = '352B0F8EF3280008E053C0A81418DFD1'
api = f'/ybsx_queryYbsx.do?gnmkId={affair}'
ADDR = Host + api


def get_cookie_from_txt():
    """
    从cookie.txt文件中获取值

    :return: str: cookie值
    """
    with open('cookie.txt', 'r') as f:
        content = f.readlines()
        for i in range(len(content)):
            content[i] = content[i][:len(content[i])]
    return str(content[0])


def make_header():
    """
    制作http请求头

    :return: dict: http请求头
    """
    cookie_value = get_cookie_from_txt()

    cookie = 'JSESSIONID=' + cookie_value

    header = {
        # 'Connection': 'keep-alive',
        # 'Accept': 'text/plain, */*; q=0.01',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        # 'Origin': 'http://app2.dzgzpt.gdgs.tax',
        # 'Accept-Language': 'zh-CN',
        # 'X-Requested-With': 'XMLHttpRequest',
        # 'Referer': 'http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html?gnmkId=352B0F8EF3280008E053C0A81418DFD1&timestamp=1619775810858',
        # 'Content-Length': '79',
        # 'User-Agent':Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) perfectworldarena/1.0.21042911 Chrome/76.0.3809.146 Electron/6.1.12 Safari/537.36',
        # 'Accept-Encoding': 'gzip, deflate',
        'Cookie': cookie,
    }
    return header


PAGE_SIZE = 100  # 定义每页的数据量, 服务端要求小于等于100


def get_input_params():
    """
    获取输入的查询参数

    :return: tuple： 税务机关， 申请开始， 申请结束， 办理开始， 办理结束， 选择税务局
    """
    chooseswjg = input('输入数字选择税务机关(1:天河税务局，2:第一税务所):')
    swryszswjg = ''
    # swry = ''  # 税务人员

    if chooseswjg == '1':
        swryszswjg = '14401060000'  # 天河税务局
    elif chooseswjg == '2':
        swryszswjg = '14401060011'  # 第一税务所

    blrqstartdate = datetime.datetime.strptime(input('办理日期开始(格式：20210131):'), "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期开始
    blrqenddate = datetime.datetime.strptime(input('办理日期截至(格式：20210131):'), "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期截至
    # sqsjstartdate = datetime.datetime.strptime(input('申请时间开始(格式：20210131):'), "%Y%m%d").strftime("%Y-%m-%d")  # 申请时间开始
    # sqsjenddate = datetime.datetime.strptime(input('申请时间截至(格式：20210131):'), "%Y%m%d").strftime("%Y-%m-%d")  # 申请时间截至
    sqsjstartdate = blrqstartdate
    sqsjenddate = blrqenddate
    return swryszswjg, sqsjstartdate, sqsjenddate, blrqstartdate, blrqenddate, chooseswjg


def get_search_params(pageIndex, pageSize=PAGE_SIZE, swry='', extra_data=False):
    """
    获取搜索参数

    :param int pageIndex: 页码
    :param int pageSize: 每页返回数量
    :param str swry: 税务人员
    :param bool extra_data: 是否查询多余的上月数据
    :return: 格式化查询参数
    """
    nsrsbh = ''
    swsxDm = ''
    """
    swsxDm(税务事项代码)
    
    票种核定: SXE080100000
    注销登记: SLSXA011005001
    变更登记: SXE010800000
    增值税一般纳税人登记: SXE010500000
    定期定额户核定定额: SXA071001002
    增值税专用发票最高开票限额申请: SXE080200000
    """
    blztDm = ''  # 不予受理: 02
    swryszswjg = SWRYSZSWJG  # 税务人员所在税务机关
    shuiwurenyuan = swry.encode('utf-8').decode('latin1')  # 税务人员 requests 参数含有中文需要转码
    sfyqbl = ''
    sqsjstartDate = SQSJSTARTDATE
    sqsjendDate = SQSJENDDATE
    blrqstartDate = BLRQSTARTDATE
    blrqendDate = BLRQENDDATE
    jrqd = ''
    pageIndex = pageIndex  # 当前页面
    pageSize = pageSize  # 单页长度
    sortField = ''
    sortOrder = ''

    extra_sqsjstartDate = (
            datetime.datetime.strptime(BLRQSTARTDATE, '%Y-%m-%d') + datetime.timedelta(days=-7)).strftime(
        '%Y-%m-%d')
    extra_sqsjendDate = (datetime.datetime.strptime(BLRQSTARTDATE, '%Y-%m-%d') + datetime.timedelta(days=-1)).strftime(
        '%Y-%m-%d')

    data = f'nsrsbh={nsrsbh}' + \
           f'&swsxDm={swsxDm}' + \
           f'&blztDm={blztDm}' + \
           f'&swryszswjg={swryszswjg}' + \
           f'&swry={shuiwurenyuan}' + \
           f'&sfyqbl={sfyqbl}' + \
           f'&sqsjstartDate={sqsjstartDate if not extra_data else extra_sqsjstartDate}' + \
           f'&sqsjendDate={sqsjendDate if not extra_data else extra_sqsjendDate}' + \
           f'&blrqstartDate={blrqstartDate}' + \
           f'&blrqendDate={blrqendDate}' + \
           f'&jrqd={jrqd}' + \
           f'&pageIndex={pageIndex}' + \
           f'&pageSize={pageSize}' + \
           f'&sortField={sortField}' + \
           f'&sortOrder={sortOrder}'
    return data


def get_total_count(swry='', extra_data=False):
    """
    获取总数据数量

    :param bool extra_data: 是否查询多余的上月数据
    :param str swry: 税务人员代码
    :return: int： 结果总数
    """
    search_data = requests.post(
        ADDR,
        headers=make_header(),
        data=get_search_params(pageIndex=0, pageSize=1, swry=swry, extra_data=extra_data)
    )
    result = search_data.json()['total']
    # print('get_total_count', result)
    return int(result)


def grab_data(page, swry='', extra_data=False):
    """
    获取某一页的数据

    :param extra_data: 是否查询多余的上月数据
    :param int page: 页码索引（从0开始）
    :param str swry: 税务人员代码
    :return: result: json数据
    """
    search_data = requests.post(
        ADDR,
        headers=make_header(),
        data=get_search_params(pageIndex=page, swry=swry, extra_data=extra_data)
    )
    result = search_data.json()['data']
    return result


def insert_thead(sheet):
    """
    录入表头

    :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
    """
    sheet.write(0, 0, '事项名称')
    sheet.write(0, 1, '纳税人识别号')
    sheet.write(0, 2, '纳税人名称')
    sheet.write(0, 3, '主管税务所')
    sheet.write(0, 4, '受理人')
    sheet.write(0, 5, '申请日期')
    sheet.write(0, 6, '受理日期')
    sheet.write(0, 7, '办理期限')
    sheet.write(0, 8, '办结时间')
    sheet.write(0, 9, '办理状态')
    sheet.write(0, 10, '实名信息-姓名')
    sheet.write(0, 11, '是否逾期办理')
    sheet.write(0, 12, '来源渠道')
    print('表头录入成功')


def insert_data(data, page, sheet):
    """
    录入数据

    :param data: 某一页的数据
    :param int page: 当前页码索引(从0开始)
    :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
    """
    for j in range(len(data)):
        sheet.write(j + PAGE_SIZE * page + 1, 0, data[j]['swsxMc'])
        sheet.write(j + PAGE_SIZE * page + 1, 1, data[j]['sqr'])
        sheet.write(j + PAGE_SIZE * page + 1, 2, data[j]['nsrmc'])
        try:
            sheet.write(j + PAGE_SIZE * page + 1, 3, data[j]['zgswskfj'])
        except:
            print('主管税务所第', j + PAGE_SIZE * page + 1, '条出错')
            try:
                print(data[j]['zgswskfj'])
            except:
                pass
        sheet.write(j + PAGE_SIZE * page + 1, 4, data[j]['blryMc'])
        sheet.write(j + PAGE_SIZE * page + 1, 5, data[j]['cjsj'])
        sheet.write(j + PAGE_SIZE * page + 1, 6, data[j]['blsj'])
        sheet.write(j + PAGE_SIZE * page + 1, 7, data[j]['blqx'])
        sheet.write(j + PAGE_SIZE * page + 1, 8, data[j]['bjsj'])
        sheet.write(j + PAGE_SIZE * page + 1, 9, data[j]['blztMc'])
        sheet.write(j + PAGE_SIZE * page + 1, 10, data[j]['smxxXm'])
        sheet.write(j + PAGE_SIZE * page + 1, 11, data[j]['sfyqblMc'])
        sheet.write(j + PAGE_SIZE * page + 1, 12, data[j]['jrqdmc'])


def process_data1():
    """
    制作excel表格(天河税务局)
    """
    total_count = get_total_count()
    print('总数据量:', total_count)
    # 计算总页数（如不能整除则多加一页录入剩下的数据）
    if not total_count % PAGE_SIZE == 0:
        total_page = total_count // PAGE_SIZE + 1
    else:
        total_page = total_count // PAGE_SIZE
    print('总页数:', total_page)

    # 创建excel文件
    workbook = xlsxwriter.Workbook('sources/source.xlsx')
    sheet = workbook.add_worksheet('源数据')

    # 录入表头(行row，列column，值value)
    insert_thead(sheet)
    # 录入数据
    for i in range(total_page):
        data = grab_data(i)
        # print('grab_data', data)
        insert_data(data, i, sheet)
        print('数据录入成功,第', i + 1, '页')

    # 保存文件
    workbook.close()
    print('保存成功')


def process_data2_middle(workbook, swry):
    """
    录入每个税务人员的数据

    :param xlsxwriter.Workbook workbook: excel表对象
    :param list swry: 税务人员资料 [代码，姓名]
    :return: workbook: 录入好的excel表对象
    """
    total_count = get_total_count(swry=swry[0])
    print('总数据量:', total_count)
    # 总页数（包括最后一页）
    if not total_count % PAGE_SIZE == 0:
        total_page = total_count // PAGE_SIZE + 1
    else:
        total_page = total_count // PAGE_SIZE
    print('总页数:', total_page)

    sheet = workbook.add_worksheet(swry[1])

    print(f'开始录入{swry[1]}的数据')
    # 录入表头(行row，列column，值value)
    insert_thead(sheet)

    # 录入数据
    for i in range(total_page):
        data = grab_data(i, swry=swry[0])
        # print('grab_data', data)
        insert_data(data, i, sheet)
        print('数据录入成功,第', i + 1, '页')

    return workbook


def process_data2(csv_filename):
    """
    制作excel表格(第一税务所)

    :param csv_filename: 税务人员列表文件名
    """
    member = read_csv(csv_filename)

    # 创建excel文件
    workbook = xlsxwriter.Workbook(f'sources/source_{csv_filename}.xlsx')

    for swry in member:
        process_data2_middle(workbook, swry)

    # 保存文件
    workbook.close()
    print('保存成功')


def main1():
    """
    天河税务局主函数
    """
    try:
        process_data1()
    except:
        print(traceback.format_exc())
        print('查询失败，需要重新登录')
        write_cookie()
        process_data1()


def main2(csv_filename):
    """
    第一税务所主函数
    """
    try:
        process_data2(csv_filename)
    except:
        print(traceback.format_exc())
        print('查询失败，需要重新登录')
        write_cookie()
        process_data2(csv_filename)


if __name__ == '__main__':
    if not os.path.exists('../sources'):
        os.mkdir('../sources')
    if len(os.listdir('../sources')) > 0:
        extra_choice = input('检测到source文件夹有数据，是否输出原始数据统计表？（y:是）')
        if extra_choice == 'y':
            write_excel()
            print('原始数据统计表输出成功，开始查询')
    SWRYSZSWJG, SQSJSTARTDATE, SQSJENDDATE, BLRQSTARTDATE, BLRQENDDATE, CHOOSESWJ = get_input_params()

    try:
        if CHOOSESWJ == '1':
            main1()
        elif CHOOSESWJ == '2':
            filenames = os.listdir('../swry')
            for index in range(len(filenames)):
                filenames[index] = filenames[index].replace('.csv', '')
            for file in filenames:
                main2(file)
            choice = input('是否输出处理后的数据？（y:是）')
            if choice == 'y':
                write_excel()
    except Exception:
        print(traceback.format_exc())
    finally:
        input('回车键退出')
