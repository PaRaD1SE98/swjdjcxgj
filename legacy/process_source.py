import os

# import openpyxl
import xlsxwriter
from csv_reader import read_csv2


def process(file_name):
    """
    对获得的excel数据表进行处理

    :return: 列表字典嵌套数据
    """
    data = []
    # 打开文件并获取所有表名
    workbook = openpyxl.load_workbook(f'sources/{file_name}.xlsx')
    sheetnames = workbook.sheetnames

    # 开始处理每个分表的数据
    for name in sheetnames:
        print('----------------------------------------')
        print(f'税务人员：{name}')
        person = {
            '姓名': name,
            '事项': [],
        }
        worksheet = workbook[name]

        # 获取事项名称列表并判断是否退件
        sx_list = []
        for cell, reject in zip(list(worksheet.columns)[0], list(worksheet.columns)[9]):
            is_rejected = False
            if reject.value == '不予受理':
                is_rejected = True
            sx_list.append((cell.value, is_rejected))
        sx_list.pop(0)
        # print(sx_list)

        # 去重列表事项名称
        sx_list_sorted = []
        for sx in sx_list:
            if sx[0] not in sx_list_sorted:
                sx_list_sorted.append(sx[0])

        # 统计每个事项总量及其退件数量
        for sxmc in sx_list_sorted:
            selected_total = [sx for sx in sx_list if sx[0] == sxmc]
            selected_rejected = [sx for sx in selected_total if sx[1]]
            print(sxmc, len(selected_total))
            print('退件数量', len(selected_rejected))
            sx = {
                '事项名称': sxmc,
                '总量': len(selected_total),
                '退件数量': len(selected_rejected),
            }
            person['事项'].append(sx)
        print('----------------------------------------')
        data.append(person)
    # print(data)
    return data


def insert_thead(sheet):
    """
    录入表头

    :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
    """
    sheet.write(0, 0, '事项名称/受理人')
    sheet.write(0, len(read_csv2('ShiXiang')) * 2 + 1, '总受理量（人）')
    sheet.write(0, len(read_csv2('ShiXiang')) * 2 + 2, '总退件量（每人）')

    for i, j in zip(read_csv2('ShiXiang'), range(len(read_csv2('ShiXiang')))):
        sheet.write(0, j * 2 + 1, i)
        sheet.write(0, j * 2 + 2, '退件数量')

    print('表头录入成功')


def insert_data(data, sheet):
    """
    录入数据

    :param list data: 数据
    :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
    """
    # choices = input('是否在空数据单元格填入"0"？(y：是)')
    # keep0 = True if choices == 'y' else False

    # 每人
    for i, j in zip(data, range(len(data))):
        # 统计总受理量和总退件量
        total = 0
        total_rejected = 0

        sheet.write(j + 1, 0, i['姓名'])

        # 每个事项
        for k, v in zip(read_csv2('ShiXiang'), range(len(read_csv2('ShiXiang')))):
            # 先录入0
            # if keep0:
            sheet.write(j + 1, v * 2 + 1, 0)
            sheet.write(j + 1, v * 2 + 2, 0)
            # 比对事项名称并统计和录入符合的
            for sx in i['事项']:
                if sx['事项名称'] == k:
                    total += sx['总量']
                    total_rejected += sx['退件数量']
                    sheet.write(j + 1, v * 2 + 1, sx['总量'])
                    sheet.write(j + 1, v * 2 + 2, sx['退件数量'])

        sheet.write(j + 1, len(read_csv2('ShiXiang')) * 2 + 1, total)
        sheet.write(j + 1, len(read_csv2('ShiXiang')) * 2 + 2, total_rejected)
    print('数据录入成功')


def write_excel():
    """
    制作excel表格(符合其他工程表结构)
    """
    files = os.listdir('../sources')
    file_dict = {}
    for i, j in zip(files, range(len(files))):
        files[j] = i.replace('.xlsx', '')
        file_dict[j + 1] = files[j]
    if len(file_dict) > 1:
        print('录入哪个excel文件的数据？')
        print(str(file_dict).replace('{', '').replace('}', ''))
        choice = input('输入序号：')
        filename = file_dict[int(choice)]
    else:
        filename = file_dict[1]

    # 创建excel文件
    if not os.path.exists('../results'):
        os.mkdir('../results')
    workbook = xlsxwriter.Workbook(f'results/result_{filename.replace("source_", "")}.xlsx')
    sheet = workbook.add_worksheet('原始数据')

    # 录入表头(行row，列column，值value)
    insert_thead(sheet)

    # 录入数据
    insert_data(process(filename), sheet)

    # 保存文件
    workbook.close()


if __name__ == '__main__':
    write_excel()
