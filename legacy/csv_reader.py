import csv


def read_csv(csv_filename):
    """
    读取swry文件夹的csv文件，输出税务人员代码和名字

    :param str csv_filename: csv文件名不带扩展名
    :return: list member [[税务人员代码， 姓名]]
    """
    with open(f'swry/{csv_filename}.csv', 'r', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter='\t')
        member = []
        for i in reader:
            member.append(i)
        # print(member)
    return member


def read_csv2(csv_filename):
    """
    读取query_params文件夹的csv文件，输出事项名称

    :param str csv_filename: csv文件名不带扩展名
    :return: list member [事项名称]
    """
    with open(f'query_params/{csv_filename}.csv', 'r', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter='\t')
        member = []
        for i in reader:
            member.append(i[0])
        # print(member)
    return member


if __name__ == '__main__':
    print(read_csv('WangShou'))
