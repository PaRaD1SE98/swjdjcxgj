"""
all_json_formats = [
    {
        '税务人员': '',
        '事项': [
            {
                '事项名称': data[j]['swsxMc'],
                '纳税人识别号': data[j]['sqr'],
                '纳税人名称': data[j]['nsrmc'],
                '主管税务所': data[j]['zgswskfj'],(可能出错)
                '受理人': data[j]['blryMc'],
                '申请日期': data[j]['cjsj'],
                '受理日期': data[j]['blsj'],
                '办理期限': data[j]['blqx'],
                '办结时间': data[j]['bjsj'],
                '办理状态': data[j]['blztMc'],
                '实名信息-姓名': data[j]['smxxXm'],
                '是否逾期办理': data[j]['sfyqblMc'],
                '来源渠道': data[j]['jrqdmc'],
            },
        ]
    },
]
desired_format = [
    {
        '姓名': '',
        '事项': [
            {
                '事项名称': 'sxmc',
                '总量': 'len(selected_total_list)',
                '退件数量': 'len(selected_rejected_list)',
            },
        ]
    },
]
"""
import datetime
import os
import traceback

import requests
import xlsxwriter

from YBSX_legacy import PAGE_SIZE, make_header, ADDR
from login import write_cookie
from process_source import insert_thead, insert_data

from csv_reader import read_csv


def get_input_params():
    """
    获取输入的查询参数

    :return: tuple： 税务机关， 申请开始， 申请结束， 办理开始， 办理结束， 选择税务局
    """
    swryszswjg = '14401060011'
    blny = input('办理年月（格式：202101）:')

    def last_day_of_month(any_day):
        """
        获取获得一个月中的最后一天
        :param any_day: 任意日期
        :return: string
        """
        next_month = any_day.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
        return next_month - datetime.timedelta(days=next_month.day)

    blrqstartdate = datetime.datetime.strptime(blny + "01", "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期开始
    blrqenddate = last_day_of_month(datetime.datetime.strptime(blny + '01', "%Y%m%d")).strftime("%Y-%m-%d")  # 办理日期截至
    sqsjstartdate = blrqstartdate
    sqsjenddate = blrqenddate

    extra_sqsjstartDate = (
            datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-7)).strftime(
        '%Y-%m-%d')
    extra_sqsjendDate = (datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-1)).strftime(
        '%Y-%m-%d')

    print(
        f'申请时间开始{sqsjstartdate}\n'
        f'申请时间截至{sqsjenddate}\n'
        f'办理时间开始{blrqstartdate}\n'
        f'办理时间截至{blrqenddate}\n'
        f'上月末尾申请时间开始{extra_sqsjstartDate}\n'
        f'上月末尾申请时间截至{extra_sqsjendDate}'
    )
    return swryszswjg, sqsjstartdate, sqsjenddate, blrqstartdate, blrqenddate


def get_search_params(pageIndex, pageSize=PAGE_SIZE, swry='', extra_data=False):
    """
    获取搜索参数

    :param int pageIndex: 页码
    :param int pageSize: 每页返回数量
    :param str swry: 税务人员
    :param bool extra_data: 是否查询多余的上月数据
    :return: 格式化查询参数
    """
    nsrsbh = ''
    swsxDm = ''
    """
    swsxDm(税务事项代码)

    票种核定: SXE080100000
    注销登记: SLSXA011005001
    变更登记: SXE010800000
    增值税一般纳税人登记: SXE010500000
    定期定额户核定定额: SXA071001002
    增值税专用发票最高开票限额申请: SXE080200000
    """
    blztDm = ''  # 不予受理: 02
    swryszswjg = SWRYSZSWJG  # 税务人员所在税务机关
    shuiwurenyuan = swry.encode('utf-8').decode('latin1')  # 税务人员 requests 参数含有中文需要转码
    sfyqbl = ''
    sqsjstartDate = SQSJSTARTDATE
    sqsjendDate = SQSJENDDATE
    blrqstartDate = BLRQSTARTDATE
    blrqendDate = BLRQENDDATE
    jrqd = ''
    pageIndex = pageIndex  # 当前页面
    pageSize = pageSize  # 单页长度
    sortField = ''
    sortOrder = ''

    extra_sqsjstartDate = (
            datetime.datetime.strptime(BLRQSTARTDATE, '%Y-%m-%d') + datetime.timedelta(days=-7)).strftime(
        '%Y-%m-%d')
    extra_sqsjendDate = (datetime.datetime.strptime(BLRQSTARTDATE, '%Y-%m-%d') + datetime.timedelta(days=-1)).strftime(
        '%Y-%m-%d')

    data = f'nsrsbh={nsrsbh}' + \
           f'&swsxDm={swsxDm}' + \
           f'&blztDm={blztDm}' + \
           f'&swryszswjg={swryszswjg}' + \
           f'&swry={shuiwurenyuan}' + \
           f'&sfyqbl={sfyqbl}' + \
           f'&sqsjstartDate={sqsjstartDate if not extra_data else extra_sqsjstartDate}' + \
           f'&sqsjendDate={sqsjendDate if not extra_data else extra_sqsjendDate}' + \
           f'&blrqstartDate={blrqstartDate}' + \
           f'&blrqendDate={blrqendDate}' + \
           f'&jrqd={jrqd}' + \
           f'&pageIndex={pageIndex}' + \
           f'&pageSize={pageSize}' + \
           f'&sortField={sortField}' + \
           f'&sortOrder={sortOrder}'
    return data


def get_total_count(swry='', extra_data=False):
    """
    获取总数据数量

    :param bool extra_data: 是否查询多余的上月数据
    :param str swry: 税务人员代码
    :return: int： 结果总数
    """
    search_data = requests.post(
        ADDR,
        headers=make_header(),
        data=get_search_params(pageIndex=0, pageSize=1, swry=swry, extra_data=extra_data)
    )
    result = search_data.json()['total']
    # print('get_total_count', result)
    return int(result)


def grab_data(page, swry='', extra_data=False):
    """
    获取某一页的数据

    :param extra_data: 是否查询多余的上月数据
    :param int page: 页码索引（从0开始）
    :param str swry: 税务人员代码
    :return: result: json数据
    """
    search_data = requests.post(
        ADDR,
        headers=make_header(),
        data=get_search_params(pageIndex=page, swry=swry, extra_data=extra_data)
    )
    result = search_data.json()['data']
    return result


def get_all_json(csv_filename):
    """
    输出一个包含全部内容的json

    :param csv_filename: 税务人员列表csv
    :return: json
    """
    print('开始获取数据')
    all_json = []
    member = read_csv(csv_filename)

    # 获取税务人员代码
    for swry in member:
        total_count = get_total_count(swry=swry[0])
        total_count_extra = get_total_count(swry=swry[0], extra_data=True)

        # 总页数（包括最后一页）
        if not total_count % PAGE_SIZE == 0:
            total_page = total_count // PAGE_SIZE + 1
        else:
            total_page = total_count // PAGE_SIZE

        # 上月末尾总页数（包括最后一页）
        if not total_count_extra % PAGE_SIZE == 0:
            total_page_extra = total_count_extra // PAGE_SIZE + 1
        else:
            total_page_extra = total_count_extra // PAGE_SIZE

        print(f'{swry[1]}:')
        print(f'本月总数据量:{total_count},总页数:{total_page}')
        print(f'上月末总数据量:{total_count_extra},总页数{total_page_extra}')

        # 构建每个税务人员的数据
        total_data = []
        # 获取每一页的数据
        for i in range(total_page):
            data = grab_data(i, swry=swry[0])
            # 获取一页中的每条数据
            for j in range(len(data)):
                sx_data = {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                }
                try:
                    sx_data['主管税务所'] = data[j]['zgswskfj']
                except KeyError:
                    print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                total_data.append(sx_data)
        # 获取上月末尾每一页的数据
        for i in range(total_page_extra):
            data = grab_data(i, swry=swry[0], extra_data=True)
            # 获取一页中的每条数据
            for j in range(len(data)):
                sx_data = {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                }
                try:
                    sx_data['主管税务所'] = data[j]['zgswskfj']
                except KeyError:
                    print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                total_data.append(sx_data)

        # 将每个人的数据加入all_json
        swry_data = {
            '税务人员': swry,
            '事项': total_data,
        }
        all_json.append(swry_data)

    print('数据获取成功')
    return all_json


def process_all_json(all_json):
    """
    输出用于生成统计表的json

    :param list all_json: 包含所有数据的json
    :return: json 用于生成excel的处理好的json
    """
    print('开始处理数据')
    data = []
    # 处理每个人的数据
    for i in all_json:
        print('----------------------------------------')
        print(f"税务人员：{i['税务人员'][1]}")
        person = {
            '姓名': i['税务人员'][1],
            '事项': [],
        }
        # 处理每个事项
        # 获取事项名称列表并判断是否退件
        sx_list = []
        for sx in i['事项']:
            is_rejected = False
            if sx['办理状态'] == '不予受理':
                is_rejected = True
            sx_list.append((sx['事项名称'], is_rejected))

        # 去重列表事项名称
        sx_list_sorted = []
        for sx in sx_list:
            if sx[0] not in sx_list_sorted:
                sx_list_sorted.append(sx[0])

        # 统计每个事项总量及其退件数量
        for sxmc in sx_list_sorted:
            selected_total_list = [sx for sx in sx_list if sx[0] == sxmc]
            selected_rejected_list = [sx for sx in selected_total_list if sx[1]]
            print(sxmc, len(selected_total_list))
            print('退件数量', len(selected_rejected_list))
            sx = {
                '事项名称': sxmc,
                '总量': len(selected_total_list),
                '退件数量': len(selected_rejected_list),
            }
            person['事项'].append(sx)
        print('----------------------------------------')
        data.append(person)
    print('数据处理成功')
    return data


def write_excel(csv_filename):
    """
    输出excel文件

    :param csv_filename: 税务人员列表csv
    :return:
    """
    # 创建excel文件
    if not os.path.exists('../results'):
        os.mkdir('../results')
    workbook = xlsxwriter.Workbook(f'results/{csv_filename}_{BLRQSTARTDATE + "-" + BLRQENDDATE}.xlsx')
    sheet = workbook.add_worksheet('原始数据')

    # 录入表头(行row，列column，值value)
    insert_thead(sheet)

    # 录入数据
    insert_data(process_all_json(get_all_json(csv_filename)), sheet)

    # 保存文件
    workbook.close()
    print('写入Excel成功')


def main():
    """
    天河税务局主函数
    """
    filenames = os.listdir('../swry')
    for index in range(len(filenames)):
        filenames[index] = filenames[index].replace('.csv', '')
    try:
        for file in filenames:
            write_excel(file)
    except:
        print('查询失败，需要重新登录')
        write_cookie()
        for file in filenames:
            write_excel(file)


if __name__ == '__main__':
    try:
        SWRYSZSWJG, SQSJSTARTDATE, SQSJENDDATE, BLRQSTARTDATE, BLRQENDDATE = get_input_params()
        main()
    except:
        print(traceback.format_exc())
    finally:
        input('回车键退出')
