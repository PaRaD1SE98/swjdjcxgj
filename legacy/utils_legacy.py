import openpyxl
import csv


def get_query_params():
    workbook = openpyxl.load_workbook('dev_src/网受业务量统计表2021年5月明细（包含海景、珠城、市政务、区政务数据）.xlsx')
    worksheet = workbook['海景转置简化表']
    thead = list(worksheet.rows)[0]
    shi_xiang = []
    for cell in thead:
        shi_xiang.append([cell.value])
    shi_xiang = shi_xiang[1:-2]
    while True:
        try:
            shi_xiang.remove(['退件数量'])
        except ValueError:
            break
    print(shi_xiang)
    with open('../query_params/ShiXiang.csv', 'w') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerows(shi_xiang)


if __name__ == '__main__':
    get_query_params()

# pyinstaller -p C:\Users\ldj\Desktop\shui-wu-ju-ybsx\venv\Lib\site-packages YBSXAllInOne.py -y
# pyinstaller -p C:\Users\ldj\Desktop\shui-wu-ju-ybsx\venv\Lib\site-packages YBSX.py -y
# pyinstaller -p C:\Users\ldj\Desktop\shui-wu-ju-ybsx\venv\Lib\site-packages YBSX_BYSL.py -y
# pyinstaller -p C:\Users\ldj\Desktop\shui-wu-ju-ybsx\venv\Lib\site-packages YBSX_NSRSBH.py -y
# pyinstaller -p C:\Users\ldj\Desktop\shui-wu-ju-ybsx\venv\Lib\site-packages YBSX_ALL.py -y
# pyinstaller -p C:\Users\ldj\Desktop\shui-wu-ju-ybsx\venv\Lib\site-packages DBSX.py -y
# pyinstaller -p C:\Users\ldj\Desktop\shui-wu-ju-ybsx\venv\Lib\site-packages SwjDjCxgj.py -y
