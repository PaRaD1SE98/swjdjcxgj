import traceback

from colorama import init

import DianJu

DEBUG = False

init(autoreset=True)

if __name__ == '__main__':
    print("\033[32m税务局电局查询工具\033[0m")
    print(f"\033[32m版本：{DianJu.__version__}\033[0m")
    if DEBUG:
        print('\033[31m正在调试模式\033[0m')
    while True:
        print(
            '选项：\n'
            '    0.保存登陆账号密码\n'
            '    1.已办事项（统计受理数与退件数）\n'
            '    2.已办事项（电局原格式）\n'
            '    3.已办事项_不予受理（电局原格式，添加办理意见）\n'
            '    4.已办事项_纳税人识别号（添加办理意见，审批意见）\n'
            '    5.待办事项（电局原格式，事项筛选）\n'
            '    6.待办事项（电局原格式，全部数据）\n'
            '    7.已办事项_事项代码获取工具（[8]的依赖）\n'
            '    8.已办事项_全年事项受理统计 \n'
            '    9.已办事项_日结数据 \n'
            '    10.已办事项_日结原始数据 \n'
            '    11.已办事项_日结数据分析 \n'
            '    12.发票服务_专用发票普通发票统计 \n'
        )
        arg = input('输入选项(回车键退出)：')
        try:
            if arg == '0':
                print(f'\033[32m{DianJu.account_setter.__doc__}\033[0m')
                DianJu.account_setter()
            elif arg == '1':
                print(f'\033[32m{DianJu.ybsx.__doc__}\033[0m')
                DianJu.YBSX()
            elif arg == '2':
                print(f'\033[32m{DianJu.ybsx_all.__doc__}\033[0m')
                DianJu.YBSX_ALL()
            elif arg == '3':
                print(f'\033[32m{DianJu.ybsx_bysl.__doc__}\033[0m')
                DianJu.YBSX_BYSL()
            elif arg == '4':
                print(f'\033[32m{DianJu.ybsx_nsrsbh.__doc__}\033[0m')
                DianJu.YBSX_NSRSBH()
            elif arg == '5':
                print(f'\033[32m{DianJu.dbsx.__doc__}\033[0m')
                DianJu.DBSX()
            elif arg == '6':
                print(f'\033[32m{DianJu.dbsx_all.__doc__}\033[0m')
                DianJu.DBSX_ALL()
            elif arg == '7':
                print(f'\033[32m{DianJu.ybsx_swsxdmgenerator.__doc__}\033[0m')
                DianJu.YBSX_SwsxDmGenerator()
            elif arg == '8':
                print(f'\033[32m{DianJu.ybsx_swsxtj.__doc__}\033[0m')
                DianJu.YBSX_SWSXTJ()
            elif arg == '9':
                print(f'\033[32m{DianJu.ybsx_daily.__doc__}\033[0m')
                DianJu.YBSX_Daily()
            elif arg == '10':
                print(f'\033[32m{DianJu.ybsx_daily_origin.__doc__}\033[0m')
                DianJu.YBSX_Daily_Origin()
            elif arg == '11':
                print(f'\033[32m{DianJu.ybsx_daily_analysis.__doc__}\033[0m')
                DianJu.YBSX_Daily_Analysis()
            elif arg == '12':
                print(f'\033[32m{DianJu.fpfw.__doc__}\033[0m')
                DianJu.FPFW()
            elif arg == 'test':
                print('test completed')
            else:
                break
            continue
        except Exception:
            traceback.print_exc()
            choices = input('重试[Y]/回车键退出：')
            if str.upper(choices) == 'Y':
                continue
            else:
                break

    input('再次回车键退出')
