var foreseeFp = {};
foreseeFp = {
	version : "1.0.0",
	constants:{
		/***
		 * 业务类型代码
		 * 01：发票发放，02：代开增值税专票，03：代开货运发票，04：代开普通发票，05：代开增值税普通发票，07：代开增值税电子普通发票
		 * 08：代开红字增值税电子普通发票
		 * 11：汇总代开增值税专用发票
		 * 12：汇总代开增值税普通发票
		 * 13：汇总代开增值税电子普通发票
		 * 21：汇总代开红字增值税电子普通发票
		 */
		ywlxdm:{
			fpfs:"01",
			dkzzszp:"02",
			dkhyfp:"03",
			dkptfp:"04",
			dkzzsptfp:"05",
            dkzzsdzptfp:"07",
			dkhzzzsdzptfp:"08",
			hzdkzzszyfp:"11",
			hzdkzzsptfp:"12",
			hzdkzzsdzptfp:"13",
			hzdkhzzzsdzptfp:"21"
        },
        /**
         * 房屋出租申请人类型  01代表出租方  02代表代理（包括承租人和代理人）
         */
        sqrlx:{
            czr:"01",
            dlr:"02"
        },
        /***
         * 操作类型
         */
        action: {
            create: "create",//新增
            modify: "modify",//修改
            view: "view",//查看
            audit: "audit"//审核
        },
        /***
         * 征收项目代码
         */
        zsxmDm: {
            zzs: "10101",// 增值税
            cswhjss: "10109",// 城市维护建设税
            jyffj: "30203",// 教育费附加
            dfjyfjs: "30216",// 地方教育附加
            grsds: "10106",// 个人所得税
            fcs: "10110",// 房产税
            yhs: "10111",// 印花税
            cztdsys: "10112"// 城镇土地使用税
        }
	},
	common : {

		ajaxCount : 0,//当前正在请求远程服务数量
        /**
         * 比较起止日期
         */
        checkRq: function (rqq, rqz,msgNull,msgQz) {
            if (rqq == "" || rqz == "" || rqq == null || rqz == null) {
                layer.alert(msgNull, {
                    icon: 5,
                    title: "提示"
                });
                return false;
            }
            if (new Date(Date.parse(rqq)) > new Date(Date.parse(rqz))) {
                layer.alert(msgQz, {
                    icon: 5,
                    title: "提示"
                });
                return false;
            }
            return true;
        },

		/**
		 * 模式窗体
		 */
		showDlgModel : function(options, callback) {
			$("#NoPermissioniframe").attr("src", options.srcUrl);
			var paramStr = '';
			if(options.params != null && options.params.length > 0){
				for(var i=0; i < options.params.length; i++){
					if(i == 0){
						paramStr += '?';
					}
					paramStr += options.params[i].name + '=' + options.params[i].value + '&';
				}
				if(paramStr.length > 0){
					paramStr = paramStr.substring(0, paramStr.length-1);
				}
			}
			options.width = options.width || "1000px";
			options.height = options.height || "700px";
			options.type = 2;
			options.shadeClose = false;
			options.shade = 0.5;
			options.area = [options.width, options.height];
			options.content = options.srcUrl + paramStr;
			options.cancel = function(index) {
				if (typeof callback == 'function') {
					callback.call(this);
				}
				layer.close(index);
			}

			var index = layer.open(options);
			return index;
		},
		/**
		 * 获取URL中值(index.htm?参数1=数值1&参数2=数值2&......)
		 *
		 * @param name
		 * @returns
		 */
		getvl : function(name) {
			var reg = new RegExp("(^|\\?|&)" + name + "=([^&]*)(\\s|&|$)", "i");
			if (reg.test(location.href)) {
				return unescape(RegExp.$2.replace(/\+/g, " "));
			} else {
				return "";
			}
		},
		/**
		 *  判断js中传入的var 不为空没有值
		 */
		haveValue: function (valule) {
			if (valule != null && valule != '' && valule != undefined && valule != 'undefined') {
				return true;
			}
			return false;
		},
		/**
		 *
		 * @param strUrl
		 *            反问的URL
		 * @param data
		 *            数据
		 * @param asyncs
		 *            false 为同步，true 为异步。默认为异步
		 * @param callback
		 *            回调函数
		 * @param errorCallback
		 *            失败回调函数
		 * @param isNotice
		 *            是否提示错误消息
		 */
		ajax : function(strUrl, data, async, callback, errorCallback,isNotice) {
			if (async == "false" || async == false) {
				async = false;
			} else {
				async = true;
			}
			if (isNotice == "false" || isNotice == false) {
				isNotice = false;
			} else {
				isNotice = true;
			}
			$.ajax({
						type : "POST",
						url : strUrl,
						data : data,
						timeout : 30000,
						dataType : "xml",
						headers : {
							"Accept" : "text/xml; charset=utf-8",
							"Content-Type" : "text/xml; charset=utf-8"
						},
						async : async,
						beforeSend : this.beforeSend('正在处理，请稍等...'),
						complete : function() {
							foreseeFp.common.ajaxCount--;
							if(!(foreseeFp.common.ajaxCount > 0)){//当前没有未完成的远程请求时关闭模态框
								foreseeFp.common.removeMask();
							}
						},
						success : function(result) {
							if($(result).find('replyCode') != null){
								if($(result).find('replyCode').text() == '0'){
									if (typeof callback == 'function') {
										bl = callback.call(this, result);
									}
								} else if($(result).find('replyMsg') != null){
									if (errorCallback != null && errorCallback != '' && typeof errorCallback != 'function') {
										layer.alert('回调函数类型有异常！', { icon : 2, skin : 'layer-ext-moon' });
									} else{
										if(isNotice){
											layer.alert($(result).find('replyMsg').text(), { icon : 2, skin : 'layer-ext-moon' }, function(index){
												if (typeof errorCallback == 'function') {
													errorCallback.call(this, result);
												}
												layer.close(index);
											});
										} else if(typeof errorCallback == 'function') {
											errorCallback.call(this, result);
										}
									}
								} else{
									if (errorCallback != null && errorCallback != '' && typeof errorCallback != 'function') {
										layer.alert('回调函数类型有异常！', { icon : 2, skin : 'layer-ext-moon' });
									} else{
										layer.alert('调用后台服务失败，请联系后台维护人员！', { icon : 2, skin : 'layer-ext-moon' }, function(index){
											if (typeof errorCallback == 'function') {
												errorCallback.call(this, result);
											}
											layer.close(index);
										});
									}
								}
							} else{
								if (errorCallback != null && errorCallback != '' && typeof errorCallback != 'function') {
									layer.alert('回调函数类型有异常！', { icon : 2, skin : 'layer-ext-moon' });
								} else{
									layer.alert('调用后台服务失败，请联系后台维护人员！', { icon : 2, skin : 'layer-ext-moon' }, function(index){
										if (typeof errorCallback == 'function') {
											errorCallback.call(this, result);
										}
										layer.close(index);
									});
								}
							}
						},
						error : function(xhr, errorMsg, errorThrown) {

							foreseeFp.common.ajaxCount--;
							if(!(foreseeFp.common.ajaxCount > 0)){//当前没有未完成的远程请求时关闭模态框
								foreseeFp.common.removeMask();
							}

							if (errorCallback != null && errorCallback != '' && typeof errorCallback != 'function') {
								layer.alert('回调函数类型有异常！', { icon : 2, skin : 'layer-ext-moon' });
							} else{
								layer.alert('系统异常，请稍后重试。', { icon : 2, skin : 'layer-ext-moon' }, function(index){
									if (typeof errorCallback == 'function') {
										errorCallback.call();
									}
									layer.close(index);
								});
							}
						}
					});
		},
		/**
		 *
		 * @param strUrl
		 *            访问的URL
		 * @param jsonp
		 *            jsonp回调函数名
		 * @param data
		 *            数据
		 * @param asyncs
		 *            false 为同步，true 为异步。默认为异步
		 * @param callback
		 *            回调函数
		 * @param errorCallback
		 *            失败回调函数
		 * @param timeout
		 *            超时时间
		 */
		ajaxJSONP : function(strUrl, jsonp, data, async, callback, errorCallback, timeout, msg) {
			if (async == "false" || async == false) {
				async = false;
			} else {
				async = true;
			}
			if(timeout==null||timeout==''||timeout==undefined){
				timeout = 30000;
			}
			if(msg==null||msg==''||msg==undefined){
				msg = '正在执行打印操作...';
			}
			$.ajax({
						type : "POST",
						url : strUrl,
						data : data,
						timeout : timeout,
						dataType : "jsonp",

						beforeSend : this.beforeSend(msg),
						complete : function() {
							foreseeFp.common.ajaxCount--;
							if(!(foreseeFp.common.ajaxCount > 0)){//当前没有未完成的远程请求时关闭模态框
								foreseeFp.common.removeMask();
							}
						},

				        jsonp:jsonp,
						headers : {
							"Accept" : "text/xml; charset=utf-8",
							"Content-Type" : "text/xml; charset=utf-8"
						},
//						async : async,   //jsonp不支持同步
						success : function(result) {
							if (typeof callback == 'function') {
								callback.call(this, result);
							}
						},
						error : function(xhr, errorMsg, errorThrown) {
							foreseeFp.common.ajaxCount--;
							if(!(foreseeFp.common.ajaxCount > 0)){//当前没有未完成的远程请求时关闭模态框
								foreseeFp.common.removeMask();
							}
							if (typeof errorCallback == 'function') {
								errorCallback.call();
							}
						}
					});
		},
		ajaxJson : function(strUrl, data, async, callback, errorCallback, isNotice, msg, contentType, encrypt) {
			if (async == "false" || async == false) {
				async = false;
			} else {
				async = true;
			}
			if (isNotice == "false" || isNotice == false) {
				isNotice = false;
			} else {
				isNotice = true;
			}
            if (contentType == undefined || contentType == '') {
                contentType = "application/x-www-form-urlencoded;charset=utf-8";
            }
            if(encrypt == null || encrypt == undefined){
                encrypt = true;
            }
            if(encrypt){
            	var dataStr = JSON.stringify(data);
            	dataStr = Base64.encode(dataStr);
                data = {"specialToken" : dataStr};
            }
			$.ajax({
				type : "POST",
				url : strUrl,
				data : data,
				timeout : 30000,
				dataType : "text",
				contentType : "application/x-www-form-urlencoded;charset=utf-8",
				async : async,
				beforeSend : this.beforeSend(""),
				complete : function() {
					foreseeFp.common.ajaxCount--;
					if(!(foreseeFp.common.ajaxCount > 0)){//当前没有未完成的远程请求时关闭模态框
						foreseeFp.common.removeMask();
					}
				},
				success : function(result) {
					if(result != null && result != undefined ){
                        if(encrypt){
                        	result = Base64.decode(result);
                        }
						if (typeof(JSON) == 'undefined' || typeof(JSON) == undefined){
							result = eval("("+result+")");
						}else{
							result = JSON.parse(result);
						}
						if (typeof callback == 'function') {
							bl = callback.call(this, result);
						}
					} else{
						if (errorCallback != null && errorCallback != '' && typeof errorCallback != 'function') {
							layer.alert('回调函数类型有异常！', {icon : 2, title : '温馨提示', skin : 'layer-ext-moon', offset : '200px'});
						} else{
							layer.alert('调用服务失败，请联系系统维护人员！', {icon : 2, title : '温馨提示', skin : 'layer-ext-moon', offset : '200px'}, function(index){
								layer.close(index);
								if (typeof errorCallback == 'function') {
									errorCallback.call();
								}
							});
						}
					}
				},
				error : function(xhr, errorMsg, errorThrown) {

					foreseeFp.common.ajaxCount--;
					if(!(foreseeFp.common.ajaxCount > 0)){//当前没有未完成的远程请求时关闭模态框
						foreseeFp.common.removeMask();
					}

					if (errorCallback != null && errorCallback != '' && typeof errorCallback != 'function') {
						layer.alert('回调函数类型有异常！', {icon : 2, title : '温馨提示', skin : 'layer-ext-moon', offset : '200px'});
					} else{
						layer.alert('系统异常，请稍后重试。', {icon : 2, title : '温馨提示', skin : 'layer-ext-moon', offset : '200px'}, function(index){
							layer.close(index);
							if (typeof errorCallback == 'function') {
								errorCallback.call();
							}
						});
					}
				}
			});
		},
		beforeSend : function(msg) {
			if ($("#myMaskLoading").length == 0) {
				var myMask = $('<div style="text-align:center;"></div>');
				myMask.attr('id', 'myMask');
				myMask.attr('class', 'mask');
				var maskStr = '<div>';
				if(msg != null && msg != '' && msg != undefined){
					maskStr += '<div id="loadingMsg" style="color:#FF0000;">'+msg+'</div>';
				}
				maskStr += '<div><img src="' + rootUrl + '/images/loading.gif"/></div>';
				maskStr += '</div>';
				var myMaskLoading = $(maskStr);
				myMaskLoading.attr('id', 'myMaskLoading');
				myMaskLoading.attr('class', 'mark_loading');
				$(document.body).append(myMask);
				$(document.body).append(myMaskLoading);
			} else {
				if(msg != null && msg != '' && msg != undefined){
					$("#loadingMsg").text(msg);
				}
				$("#myMask").show();
				$("#myMaskLoading").show();
			}
			foreseeFp.common.ajaxCount++;
		},
		removeMask : function() {
			if($("#myMaskLoading")!=null) {
				$("#myMaskLoading").hide();
			}
			if($("#myMask")!=null) {
				$("#myMask").hide();
			}
		},

		// 默认值为当月的第一天（格式:yyyy-mm-dd）
		loadMonthFirstDay : function() {
			var myDate = new Date();
			var year = myDate.getFullYear();
			var month = myDate.getMonth() + 1;
			if (month < 10) {
				month = "0" + month;
			}
			var firstDay = year + "-" + month + "-01";
			return firstDay;
		},

		// 返回当前日期(格式:yyyy-mm-dd)
		loadNowDay : function() {
			var myDate = new Date();
			var year = myDate.getFullYear();
			var month = myDate.getMonth() + 1;
			if (month < 10) {
				month = "0" + month;
			}
			var date = myDate.getDate();
			if (date < 10) {
				date = "0" + date;
			}
			var nowDay = year + "-" + month + "-" + date;
			return nowDay;
		},
		/**
		 * 计算两个日期相隔天数（入参格式：yyyy-MM-dd）
		 * @param sDate 开始日期
		 * @param eDate 结束日期
		 * @returns
		 */
		dateCompare : function(startDate, endDate){
			var sdate = new Date(startDate);
			var edate = new Date(endDate);
			var days = edate.getTime() - sdate.getTime();
			var day = parseInt(days / (1000 * 60 * 60 * 24));
			return day;
		},
		/**
		 * 初始化日期控件
		 * @param sDateId 开始日期id
		 * @param eDateId 结束日期id
		 * @param isLimit 是否限制起止日期选择范围，默认限制
		 * @param limitDays 限制起止日期选择范围的天数，默认起止日期的范围为30天
		 */
		initDatePlugin : function(sDateId, eDateId, isLimit, limitDays){
			if(isLimit==null || isLimit==undefined || isLimit===''){
				isLimit = true;
			}
			if(limitDays==null || limitDays==undefined || limitDays===''){
				limitDays = 30;
			}
			var laydate;
			layui.use(['laydate'], function () {
			    laydate = layui.laydate;

			 	$("#"+sDateId).val(foreseeFp.common.loadMonthFirstDay());
			 	$("#"+eDateId).val(foreseeFp.common.loadNowDay());
			 	$("#"+sDateId).attr("readonly", true);
			 	$("#"+eDateId).attr("readonly", true);

                var startDate = laydate.render({
                    elem: "#" + sDateId,
                    trigger: 'click',
                    done: function (value, date) {
                        if (value != "") {
                            //首次选择时，由于endDate还没触发限制startDate的最大可选日期，因此当startDate大于endDate时，把endDate的值置为等于startDate
                            if (foreseeFp.common.dateCompare(value, $("#" + eDateId).val()) < 0) {
                                $("#" + eDateId).val(value);
                            }
                            if (isLimit) {
                                //如果日期起与日期止间隔超过一个月，则把日期止置为所选日期起的一个月后的日期
                                if (foreseeFp.common.dateCompare(value, $("#" + eDateId).val()) > limitDays) {
                                    var year = date.year;
                                    var month = date.month + 1;
                                    var day = date.date;
                                    if (month > 12) {
                                        month = month - 12;
                                        year = year + 1;
                                    }
                                    if (month < 10) {
                                        month = '0' + month;
                                    }
                                    if (day < 10) {
                                        day = '0' + day;
                                    }
                                    $("#" + eDateId).val(year + '-' + month + '-' + day);
                                }
                            }
                            date.month = date.month - 1;
                            endDate.config.min = date;
                        }
                    }
                });
			    var endDate = laydate.render({
			        elem: "#"+eDateId,
                    trigger: 'click',
			        done: function(value, date){
			        	if(value!=""){
			         		//首次选择时，由于startDate还没触发限制endDate的最小可选日期，因此当endDate小于startDate时，把startDate的值置为等于endDate
			         		if(foreseeFp.common.dateCompare($("#"+sDateId).val(),value)<0){
				         		$("#"+sDateId).val(value);
			         		}
			         		if(isLimit){
				         		//如果日期起与日期止间隔超过一个月，则把日期起置为所选日期止的一个月前的日期
				         		if(foreseeFp.common.dateCompare($("#"+sDateId).val(),value)>limitDays){
				         			var year = date.year;
				         			var month = date.month-1;
				         			var day = date.date;
				         			if(month<=0){
				         				month=month+12;
				         				year=year-1;
				         			}
				         			if(month<10){
				         				month = '0'+month;
				         			}
				         			if(day<10){
				         				day = '0'+day;
				         			}
					         		$("#"+sDateId).val(year+'-'+month+'-'+day);
				         		}
			         		}
			         		date.month = date.month-1;
			             	startDate.config.max = date;
			         	}
			        }
			    });
			});
		},
		constainStr: function(srcStr, incStr, isIgnoreCase) {
			isIgnoreCase = isIgnoreCase || false;
			if (isIgnoreCase)
		    {
				srcStr = srcStr.toLowerCase();
				incStr = incStr.toLowerCase();
		    }

		    var startChar = incStr.substring(0, 1);
		    var strLen = incStr.length;

		    for (var j = 0; j<srcStr.length - strLen + 1; j++)
		    {
		         if (srcStr.charAt(j) == startChar)  //如果匹配起始字符,开始查找
		         {
		             if (srcStr.substring(j, j+strLen) == incStr)  //如果从j开始的字符与str匹配，那ok
		             {
		                 return true;
		             }
		         }
		    }
		    return false;
		},
		print: function(oper) {
			if(oper < 10) {
				bdhtml=window.document.body.innerHTML;//获取当前页的html代码
				sprnstr="<!--startprint"+oper+"-->";//设置打印开始区域
				eprnstr="<!--endprint"+oper+"-->";//设置打印结束区域
				prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr)+18); //从开始代码向后取html
				prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));//从结束代码向前取html
				window.document.body.innerHTML=prnhtml;
				window.print();
				window.document.body.innerHTML=bdhtml;
			} else {
				window.print();
			}
		}
	},

	ext : {
		/**
		 * 设置按钮可用
		 *
		 * @param {}
		 *            buttonId
		 */
		enableButton : function(buttonId) {
			$(buttonId).removeClass("a_btn_disable");
			$(buttonId).addClass("a_btn_enable");
		},

		/**
		 * 设置按钮不可用
		 *
		 * @param {}
		 *            buttonId
		 */
		disableButton : function(buttonId) {
			$(buttonId).removeClass("a_btn_enable");
			$(buttonId).addClass("a_btn_disable");
		}
	},
	api : {
		/**
		 * 根据当前登录帐号及传入的税务机关代码判断当前用户是否有权限获取其他分局查询权限
		 * 税务机关代码同城通办下拉数据源
		 * @param swjgDm 分局税务机关代码
		 * @crateDate2016-06-24 21
		 * @author li9581（李臻平）
		 */
		querySwjgList : function(swjgdm, handle) {
			// var strUrl = rootUrl + "/FpfwServlet.do?sid=Omni.swjg.ChildSwjgxxCxService";
            //
			// // 拼接请求
			// var fieldNames = new Array();
			// var fieldValues = new Array();
			// var i=0;
			// fieldNames[i] = "swjgdm";
			// fieldValues[i] = swjgdm;
			// i++;
			// var requestXml = getFieldsXmlRequest(fieldNames, fieldValues);
            //
			// // 发送请求
			// foreseeFp.common.ajax(strUrl, requestXml, true, callback);

            var strUrl = rootUrl + "/swry/dkfp/swjgcx/getChildSwjgxx";
            var jsonData = {'swjgdm': swjgdm,"swrySfDm":sfDm };
            foreseeFp.common.ajaxJson(strUrl, jsonData, true, callback);

			// 请求回调
			function callback(result) {
				if (typeof handle == 'function') {
					handle.call(this, result);
				} else if(typeof handle == 'object' && foreseeFp.common.isArray(handle)==true){
					for(var i=0;i<handle.length;i++) {
						handle[i].call(this, result);
					}
				}
			}
		},
		/**
		 * 查询发票领购申请单明细信息
		 */
		queryLyfpSqdMxxx : function(sqdh, handle) {
			var strUrl = rootUrl + "/FpfwServlet.do?sid=Omni.fp.fpfs.FpfsSqdMxcxService";

			// 拼接请求
			var fieldNames = new Array();
			var fieldValues = new Array();
			fieldNames[0] = "sqdh";
			fieldValues[0] = sqdh;

			fieldNames[1] = "sfDm";
			fieldValues[1] = sfDm;

			var requestXml = getFieldsXmlRequest(fieldNames, fieldValues);

			// 发送请求
			foreseeFp.common.ajax(strUrl, requestXml, true, callback);

			// 请求回调
			function callback(result) {
				if (typeof handle == 'function') {
					handle.call(this, result);
				} else if(typeof handle == 'object' && foreseeFp.common.isArray(handle)==true){
					for(var i=0;i<handle.length;i++) {
						handle[i].call(this, result);
					}
				}
			}
		},
		/**
		 * 格式化数量 4位小数
		 * @param cpsl
		 */
		getFormat_cpsl : function(cpsl) {
			var format_cpsl = "";//格式化cpsl
			if(cpsl != undefined && cpsl !="" && cpsl != 0){
				var cpslArr = cpsl.split(".");
				if((cpslArr.length>1 && cpslArr[1].length<=4) || cpslArr.length == 1){
					format_cpsl = (cpsl == undefined ||  cpsl == 0) ? "" : cpsl;
				} else if(cpslArr.length > 1 && cpslArr[1].length > 4){
					format_cpsl = (cpsl == undefined ||  cpsl == 0) ? "" : Number(cpsl).toFixed2(4);
				}
			}
			return format_cpsl==""?"":parseFloat(format_cpsl);
		},
		/**
		 * 格式化不含税单价8位小数
		 * @param bhsdj
		 */
		getFormat_bhsdj : function(bhsdj) {
			var format_bhsdj = "";//格式化bhsdj
			if(bhsdj != undefined && bhsdj !="" && bhsdj != 0){
				/*var bhsdjArr = bhsdj.split(".");
				if((bhsdjArr.length>1 && bhsdjArr[1].length <= 8) || bhsdjArr.length == 1){
					format_bhsdj = (bhsdj == undefined ||  bhsdj == 0) ? "&nbsp;" : bhsdj;
				}else if(bhsdjArr.length > 1 && bhsdjArr[1].length > 8){
					format_bhsdj = (bhsdj == undefined ||  bhsdj == 0) ? "&nbsp;" : Number(bhsdj).toFixed2(8);
				}*/
				format_bhsdj = Number(bhsdj).toFixed2(8);
			}
			return format_bhsdj==""?"":parseFloat(format_bhsdj);
		},
		/**
		 * 格式化含税销售额 8位小数
		 * @param hsxse
		 */
		getFormat_hsxse : function(hsxse) {
			var format_hsxse = "";//格式化hsxse
			if(hsxse != undefined && hsxse !="" && hsxse != 0){
				/*var hsxseArr = hsxse.toString().split(".");
				if((hsxseArr.length>1 && hsxseArr[1].length<=8) || hsxseArr.length == 1){
					format_hsxse = (hsxse == undefined ||  hsxse == 0) ? "&nbsp;" : hsxse;
				}else if(hsxseArr.length > 1 && hsxseArr[1].length > 8){
					format_hsxse = (hsxse == undefined ||  hsxse == 0) ? "&nbsp;" : Number(hsxse).toFixed2(8);
				}*/
				format_hsxse = Number(hsxse).toFixed2(8);
			}
			return format_hsxse==""?"":parseFloat(format_hsxse);
		},
		/**
		 * 格式化不含税金额 2位小数
		 * @param bhsje
		 */
		getFormat_bhsje : function(bhsje) {
			var format_bhsje = "";//格式化bhsje
			if(bhsje != undefined && bhsje !="" && bhsje != 0){
				// var bhsjeArr = bhsje.split(".");
//				if((bhsjeArr.length>1 && bhsjeArr[1].length<=2) || bhsjeArr.length == 1){
					format_bhsje = (bhsje == undefined ||  bhsje == 0) ? "&nbsp;" : Number(bhsje).toFixed2(2);
			/*	}else if(bhsjeArr.length > 1 && bhsjeArr[1].length > 2){
					format_bhsje = (bhsje == undefined ||  bhsje == 0) ? "&nbsp;" : Number(bhsje).toFixed2(2);
				}*/
			}
			return format_bhsje==""?"":parseFloat(format_bhsje);
		},
		/**
		 * 格式化扣除额 8位小数
		 * @param kce
		 */
		getFormat_kce : function(kce) {
			var format_kce = "";//格式化kce
			if(kce != undefined && kce !="" && kce != 0){
				/*var kceArr = kce.split(".");
				if((kceArr.length>1 && kceArr[1].length<=8) || kceArr.length == 1){
					format_kce = (kce == undefined ||  kce == 0) ? "&nbsp;" : kce;
				}else if(kceArr.length > 1 && kceArr[1].length > 8){
					format_kce = (kce == undefined ||  kce == 0) ? "&nbsp;" : Number(kce).toFixed2(8);
				}*/
				format_kce = Number(kce).toFixed2(8);
			}
			return format_kce==""?"":parseFloat(format_kce);
		}

	},

	fpControl:{

		/**
		 * 设置 税务机关 下拉框
		 * @param {} result 接口返回的数据
		 */
		set_lst_swjg : function(result) {
            var swjgList = result.body[0].swjgList;
            if (swjgList.length == 0){
				$("#lst_swjgdm").append("<option selected='selected' value ='" + $("#hid_swjgdm").val() + "'>" + $("#hid_swjgmc").val() + "</option>");
			} else {
				$("#lst_swjgdm").append("<option selected='selected' value =''>全部</option>");
                for (var i = 0; i < swjgList.length; i++) {
                    var swjgmc = swjgList[i].swjg_mc;
                    var swjgdm = swjgList[i].swjg_dm;
					$("#lst_swjgdm").append("<option value ='" + swjgdm + "'>" + swjgmc + "</option>");
				}
			}
		},

		 /**
	      * 显示身份证件，合同图片信息
	      * */
		showImgSfzjOrHt: function(uuid,type){
			var sid=null;
			if(type=='sfzj'){
				sid='Omni.fp.dkfp.DkfpZzsPtfpZjImgMxCxService';
			}
			if(type=='ht'){
				sid='Omni.fp.dkfp.DkfpZzsPtfpHtMxCxService';
			}
			var options = {
					srcUrl : rootUrl + "/user/plain/zzsppdk/lookImg",
					titile : "图片信息",
//					height : "400px",
					/*height : "80px",
					width : "500px",*/
					params : [ {
						name : 'uuid',
						value : uuid
					},{
						name : 'sid',
						value : sid
					},{
						name : 'type',
						value : type
					}]
				};
				var tpIndex = foreseeFp.common.showDlgModel(options);
				layer.full(tpIndex);
		},
		/**
		 * 获取打印控件版本号
		 */
		getExtPluginVersion: function(version, callback, errorCallback){
			var strUrl = dyUrlRoot+"/print?command=getversion";
			foreseeFp.common.ajaxJSONP(strUrl, 'funback', null, true, printCallback, printErrorCallback, 5000, '控件检测中，请稍等...');
			function printCallback(result){
				if(result.code!=0 && result.code != '0'){
					layer.alert("获取打印控件版本号出错,错误:"+result.msg, { icon : 2, title : '温馨提示', skin : 'layer-ext-moon',offset : '200px'});
				} else{
					if(version != null && version != '' && version != undefined){
						if(compareVersion(version,result.msg)){
							var msg = "当前打印控件版本为"+result.msg+"，请";
							msg += "<a href='"+rootUrl+"/ocx/ExtPluginSetup.exe' target='_blank'><b>点击下载</b></a>";
							msg += "安装最新版本！最新版本为"+version;
							layer.alert(msg, { icon : 2, title : '温馨提示', skin : 'layer-ext-moon',offset : '200px'});
						}
					}
				}
				if (typeof callback == 'function') {
					callback.call();
				}
			}

			function printErrorCallback(result){
				layer.alert("打印控件连接失败，请检查是否已安装并运行打印控件，如未安装请<a href='"+rootUrl+"/ocx/ExtPluginSetup.exe' target='_blank'><b>点击下载</b></a>安装！",
						{ icon : 2, title : '温馨提示', skin : 'layer-ext-moon',offset : '200px'});

				if (typeof errorCallback == 'function') {
					errorCallback.call();
				}
			}

			function compareVersion(currentVersion, plugVersion){
				var current = currentVersion.split('.');
				var plug = plugVersion.split('.');
				if(current.length>0 && plug.length>0){
					if(Number(current[0])>Number(plug[0])){
						return true;
					}
					if(Number(current[1])>Number(plug[1])){
						return true;
					}
					if(Number(current[2])>Number(plug[2])){
						return true;
					}
					if(Number(current[3])>Number(plug[3])){
						return true;
					}
				}
				return false;
			}
		},
		/**
		 * 推送消息到电子税务局消息中心(站内信（通知和提醒）)
		 * @param asyncs false同步
		 * @param type 站内信类型（N：通知；R：提醒）
		 */
		pushMsgToDzswj : function(djxh,content,title,userId,type,callback,asyncs){
			if(type==null||type==""){
				type = 'R';
			}
			if(asyncs=="false"||asyncs==false){
				asyncs = false;
			}else{
				asyncs = true;
			}
			var strUrl = rootUrl + "/msgCenter/pushMsg";
			var data = {"djxh":djxh, "content" : content, "title" : title , "userId" : userId, "type" :type};

			foreseeFp.common.ajaxJson(strUrl, data, asyncs, callback, null, false);
		},
		/**
		 * 根据手机号发送短信(电子税务局消息中心)
		 */
		sendSmsByPhoneNum : function(djxh,content,title,phoneNumber,callback,asyncs){
			if(asyncs=="false"||asyncs==false){
				asyncs = false;
			}else{
				asyncs = true;
			}
			var strUrl = rootUrl + "/msgCenter/sendSms";
			var data = {"djxh":djxh, "content" : content, "title" : title , "phoneNumber" : phoneNumber};

			foreseeFp.common.ajaxJson(strUrl, data, asyncs, callback, null, false);
		}

	}
};

function moneyArabiaToChinese(Num) {
	var isNegative = false;// 是否负数
	if (Num < 0) {
		Num = -Num;
		isNegative = true;
	}
	if (typeof Num == 'number') {
		Num = Num.toString();
	}
	for (var i = Num.length - 1; i >= 0; i--) {
		Num = Num.replace(",", "");// 替换money中的“,”
		Num = Num.replace(" ", "");// 替换money中的空格
	}
	Num = Num.replace("￥", "");// 替换掉可能出现的￥字符
	if (isNaN(Num)) { // 验证输入的字符是否为数字
		return;
	}
	// ---字符处理完毕，开始转换，转换采用前后两部分分别转换---//
	part = String(Num).split(".");
	newchar = "";
	// 小数点前进行转化
	for (var i = part[0].length - 1; i >= 0; i--) {
		if (part[0].length > 10) {
			alertWarning("位数过大，无法计算");
			return "";
		} // 若数量超过拾亿单位，提示
		tmpnewchar = "";
		perchar = part[0].charAt(i);
		switch (perchar) {
			case "0" :
				tmpnewchar = "零" + tmpnewchar;
				break;
			case "1" :
				tmpnewchar = "壹" + tmpnewchar;
				break;
			case "2" :
				tmpnewchar = "贰" + tmpnewchar;
				break;
			case "3" :
				tmpnewchar = "叁" + tmpnewchar;
				break;
			case "4" :
				tmpnewchar = "肆" + tmpnewchar;
				break;
			case "5" :
				tmpnewchar = "伍" + tmpnewchar;
				break;
			case "6" :
				tmpnewchar = "陆" + tmpnewchar;
				break;
			case "7" :
				tmpnewchar = "柒" + tmpnewchar;
				break;
			case "8" :
				tmpnewchar = "捌" + tmpnewchar;
				break;
			case "9" :
				tmpnewchar = "玖" + tmpnewchar;
				break;
		}
		switch (part[0].length - i - 1) {
			case 0 :
				tmpnewchar = tmpnewchar + "元";
				break;
			case 1 :
				if (perchar != 0)
					tmpnewchar = tmpnewchar + "拾";
				break;
			case 2 :
				if (perchar != 0)
					tmpnewchar = tmpnewchar + "佰";
				break;
			case 3 :
				if (perchar != 0)
					tmpnewchar = tmpnewchar + "仟";
				break;
			case 4 :
				tmpnewchar = tmpnewchar + "万";
				break;
			case 5 :
				if (perchar != 0)
					tmpnewchar = tmpnewchar + "拾";
				break;
			case 6 :
				if (perchar != 0)
					tmpnewchar = tmpnewchar + "佰";
				break;
			case 7 :
				if (perchar != 0)
					tmpnewchar = tmpnewchar + "仟";
				break;
			case 8 :
				tmpnewchar = tmpnewchar + "亿";
				break;
			case 9 :
				tmpnewchar = tmpnewchar + "拾";
				break;
		}
		newchar = tmpnewchar + newchar;
	}
	// 小数点之后进行转化
	if (Num.indexOf(".") != -1) {
		if (part[1].length > 2) {
			alertWarning("小数点之后只能保留两位,系统将自动截段");
			part[1] = part[1].substr(0, 2);
		}
		for (var i = 0; i < part[1].length; i++) {
			tmpnewchar = "";
			perchar = part[1].charAt(i);
			switch (perchar) {
				case "0" :
					tmpnewchar = "零" + tmpnewchar;
					break;
				case "1" :
					tmpnewchar = "壹" + tmpnewchar;
					break;
				case "2" :
					tmpnewchar = "贰" + tmpnewchar;
					break;
				case "3" :
					tmpnewchar = "叁" + tmpnewchar;
					break;
				case "4" :
					tmpnewchar = "肆" + tmpnewchar;
					break;
				case "5" :
					tmpnewchar = "伍" + tmpnewchar;
					break;
				case "6" :
					tmpnewchar = "陆" + tmpnewchar;
					break;
				case "7" :
					tmpnewchar = "柒" + tmpnewchar;
					break;
				case "8" :
					tmpnewchar = "捌" + tmpnewchar;
					break;
				case "9" :
					tmpnewchar = "玖" + tmpnewchar;
					break;
			}
			if (i == 0)
				tmpnewchar = tmpnewchar + "角";
			if (i == 1)
				tmpnewchar = tmpnewchar + "分";
			newchar = newchar + tmpnewchar;
		}
	}
	// 替换所有无用汉字
	while (newchar.search("零零") != -1)
		newchar = newchar.replace("零零", "零");
	newchar = newchar.replace("零亿", "亿");
	newchar = newchar.replace("亿万", "亿");
	newchar = newchar.replace("零万", "万");
	newchar = newchar.replace("零元", "元");
	newchar = newchar.replace("零角", "");
	newchar = newchar.replace("零分", "");

	if (newchar.charAt(newchar.length - 1) == "元" || newchar.charAt(newchar.length - 1) == "角") {
		newchar = newchar + "整";
	}

	if (isNegative) {
		newchar = '负' + newchar;
	}

	return newchar;
}

/**
 * 将数值四舍五入(保留2位小数)后格式化成金额形式
 *
 * @param num
 *            数值(Number或者String)
 * @return 金额格式的字符串,如'1,234,567.45'
 * @type String
 */
function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num)) {
		num = "0";
	}
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10) {
		cents = "0" + cents;
	}
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
		num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
	}
	return (((sign) ? '' : '-') + num + '.' + cents);
}

function alertWarning(msg){
	layer.open({
		  content: msg,
		  yes: function(index, layero){
		    layer.close(index); //如果设定了yes回调，需进行手工关闭
		  }
		});
}

/**
 * 请求参数转xml(含request节点)
 *
 * @param []
 *            fieldNames
 * @param []
 *            values
 */
function getFieldsXmlRequest(fieldNames, values) {
	var xml = '<?xml version="1.0" encoding="gbk"?>';
	xml += '<request>';
	for (var i = 0; i < fieldNames.length; i++) {
		var value =values[i];
		if("string" == typeof values[i]){
			value = value.replace(/</g, "&lt;");
			value = value.replace(/>/g, "&gt;");
			value = value.replace(/&/g, "&amp;");
		}
		if (!values[i]) {
			value = '';
		}
		xml += '<' + fieldNames[i] + '>' + value + '</' + fieldNames[i] + '>';
	}
	xml += '</request>';
	return xml;
}
/**
 * 请求参数转xml(含item节点)
 *
 * @param []
 *            fieldNames
 * @param []
 *            values
 */
function getFieldsXmlItem(fieldNames, values) {
	var xml = '<?xml version="1.0" encoding="gbk"?>';
	xml += '<item>';
	for (var i = 0; i < fieldNames.length; i++) {
		var value =values[i];
		if("string" == typeof values[i]){
			value = value.replace(/</g, "&lt;");
			value = value.replace(/>/g, "&gt;");
			value = value.replace(/&/g, "&amp;");
		}
		if (!values[i]) {
			value = '';
		}
		xml += '<' + fieldNames[i] + '>' + value + '</' + fieldNames[i] + '>';
	}
	xml += '</item>';
	return xml;
}
/**
 * 请求参数转xml
 *
 * @param []
 *            fieldNames
 * @param []
 *            values
 */
function getFieldsXml(fieldNames, values) {
	var xml = '<?xml version="1.0" encoding="gbk"?>';
	xml += '<item>';
	for (var i = 0; i < fieldNames.length; i++) {
		var value =values[i];
		if("string" == typeof values[i]){
			value = value.replace(/</g, "&lt;");
			value = value.replace(/>/g, "&gt;");
			value = value.replace(/&/g, "&amp;");
		}
		if (!values[i]) {
			value = '';
		}
		xml += '<' + fieldNames[i] + '>' + value + '</' + fieldNames[i] + '>';
	}
	xml += '</item>';
	return xml;
}

/**
 * 校验表单输入项
 *
 * @param id
 *            输入项id
 * @param itemName
 *            输入项中文名称
 * @param type
 *            校验类型（1-非空校验；2-最大长度校验；3-最小长度校验；4-正则表达式格式校验）
 * @param msg
 *            指定提示信息
 * @param length
 *            长度
 * @param regStr
 *            正则表达式字符串
 * @returns {Boolean}
 */
function validateInput(id, itemName, type, msg, length, regStr) {
	var tipMsg = msg;
	var pass = true;
	var object = $("#" + id);

	if (object == null || object.attr("id") == null) {
		alertWarning('无法找到ID为【' + id + '】的输入项！');
		return false;
	}

	if (type == 1) {// 非空校验
		if(object != null && object != undefined){
			if (object.val() == null || object.val() == '') {
				if (msg == null || msg == '') {
					tipMsg = '【' + itemName + '】不得为空！';
				}
				pass = false;
			}
		}
	} else if (type == 2) {// 最大长度校验
        if(object != null && object != undefined) {
            if (object.val().length > length) {
                if (msg == null || msg == '') {
                    tipMsg = '【' + itemName + '】不得超过' + length + '位字符！';
                }
                pass = false;
            }
        }
	} else if (type == 3) {// 最小长度校验
        if(object != null && object != undefined) {
            if (object.val().length < length) {
                if (msg == null || msg == '') {
                    tipMsg = '【' + itemName + '】不得小于' + length + '位字符！';
                }
                pass = false;
            }
        }
	} else if (type == 4) {// 正则表达式格式校验
        if(object != null && object != undefined) {
            if (!regStr.test(object.val())) {
                if (msg == null || msg == '') {
                    tipMsg = '【' + itemName + '】格式不正确！';
                }
                pass = false;
            }
        }
	}

	if (!pass) {
		object.addClass('redLight');
		object.attr('title', tipMsg);
		alertWarning(tipMsg);
		object.focus();
		return false;
	} else {
		object.removeClass('redLight');
		return true;
	}
}

function resizeGridHeight(height) {// 调整grid高度,可按需要传入参数进行微调

	if (height == null || height == '') {
		height = 168;
	}

	var containerHeight = $("#container").css("min-height").replace('px', '');// container默认为最小高度
	if ($(window).height() >= containerHeight) {// 当窗口高度不小于最小高度时container取窗口高度
		containerHeight = $(window).height();
	}
	$("#container").height(containerHeight);

	$("#grid-table").setGridHeight(containerHeight - $("#searchContainer").height() - height, true);
	$("#pageContent").height(containerHeight - 200);

}

/**
 * 判断手机号
 *
 * @returns
 */
String.prototype.isMobile = function() {
	return (/^1[34578]\d{9}$/i.test(this.Trim()));
};
/**
 * 判断座机号
 *
 * @returns
 */
String.prototype.isTel = function() {
	// "兼容格式: 国家代码(2到3位)-区号(2到3位)-电话号码(7到8位)-分机号(3位)"
	// return
	// (/^(([0\+]\d{2,3}-)?(0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/.test(this.Trim()));
	return (/^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/.test(this.Trim()));
};
String.prototype.Trim = function() {
	var m = this.match(/^\s*(\S+(\s+\S+)*)\s*$/);
	return (m == null) ? "" : m[1];
};
/**
 * 判断字符串是否以指定的子字符串开头
 *
 * @returns
 */
if (typeof String.prototype.startsWith != 'function') {
	 String.prototype.startsWith = function (prefix){
	  return this.slice(0, prefix.length) === prefix;
	 };
}
/**
 * 判断字符串是否包含指定的子字符串
 *
 * @returns
 */
if (typeof String.prototype.isContains != 'function') {
	String.prototype.isContains = function(str, substr) {
	    return str.indexOf(substr) >= 0;
	};
}
function xmlToString(xmlData) {
    var xmlString;
    //IE
    if (window.ActiveXObject){
        xmlString = xmlData.xml;
    }
    // code for Mozilla, Firefox, Opera, etc.
    else{
        xmlString = (new XMLSerializer()).serializeToString(xmlData);
    }
    return xmlString;
}

function copyDataToGrid(grid_selector, datastr) {
	$(grid_selector).jqGrid('setGridParam', {
		datatype : "xmlstring",
		datastr: datastr,
		page : 1
	}).trigger("reloadGrid"); // 重新载入
}

//function getPageSize_old() {
//	// http://www.blabla.cn/js_kb/javascript_pagesize_windowsize_scrollbar.html
//	var winW, winH;
//	var hasYScroll = false;
//	if (window.innerHeight) {// （chrome edge ie9 ie10）	//小		小（无纵向滚动条）		最大化	最大化（无纵向滚动条）
//		winW = window.innerWidth;										//856		856							1366	1366
//		var clientWidth = document.body.clientWidth;			//839		856							1349	1366	网页可见区域宽
//		var offsetWidth = document.body.offsetWidth;		//839		856							1349	1366	网页可见区域宽
//		var scrollWidth = document.body.scrollWidth;			//841		856							1351	1366	网页正文全文宽
//		var screenWidth = window.screen.width;					//1366 	1366						1366	1366	屏幕分辨率的宽
//		var screenAvailWidth = window.screen.availWidth;	//1366		1366						1366	1366	屏幕可用工作区宽度
//
//		alert(document.body.clientWidth + " " + document.body.offsetWidth + " " + document.body.scrollWidth + " " + window.screen.width + " " +  window.screen.availWidth);
//		alert(document.documentElement.clientWidth + " " + document.documentElement.offsetWidth + " " + document.documentElement.scrollWidth + " " + window.screen.width + " " +  window.screen.availWidth);
//		winH = window.innerHeight;
//		if(clientWidth != scrollWidth) {
//			hasYScroll = true;
//		}
//	} else if (document.documentElement && document.documentElement.clientHeight) {// IE 6 ie7 ie8
//																					// Strict
//																					// Mode
//		winW = document.documentElement.clientWidth;
//		var clientWidth = document.documentElement.clientWidth;			//839		856							1349	1366	网页可见区域宽
//		var offsetWidth = document.documentElement.offsetWidth;		//839		856							1349	1366	网页可见区域宽
//		var scrollWidth = document.documentElement.scrollWidth;			//841		856							1351	1366	网页正文全文宽
//		var screenWidth = window.screen.width;					//1366 	1366						1366	1366	屏幕分辨率的宽
//		var screenAvailWidth = window.screen.availWidth;	//1366		1366						1366	1366	屏幕可用工作区宽度
//		alert(clientWidth + " " + offsetWidth + " " + scrollWidth + " " + screenWidth + " " + screenAvailWidth);
//		winH = document.documentElement.clientHeight;
//	} else if (document.body) { // other
//		winW = document.body.clientWidth;
//		winH = document.body.clientHeight;
//	} // for small pages with total size less then the viewport
//	return {
//		WinW : winW,
//		WinH : winH
//	};
//}

function getPageSize() {
	// http://www.blabla.cn/js_kb/javascript_pagesize_windowsize_scrollbar.html
	var winW, winH;
	if (document.documentElement && document.documentElement.clientWidth) {
		var clientWidth = document.documentElement.clientWidth;		//839		856							1349	1366	网页可见区域宽
		var offsetWidth = document.documentElement.offsetWidth;		//839		856							1349	1366	网页可见区域宽
		var scrollWidth = document.documentElement.scrollWidth;			//841		856							1351	1366	网页正文全文宽
		var screenWidth = window.screen.width;										//1366 	1366						1366	1366	屏幕分辨率的宽
		var screenAvailWidth = window.screen.availWidth;						//1366		1366						1366	1366	屏幕可用工作区宽度
		winW = clientWidth;
		winH = document.documentElement.clientHeight;
	} else if (document.body) { // other
		winW = document.body.clientWidth;
		winH = document.body.clientHeight;
	} // for small pages with total size less then the viewport
	return {
		WinW : winW,
		WinH : winH
	};
}

function IETester(userAgent){
    var UA =  userAgent || navigator.userAgent;
    if(/msie/i.test(UA)){
        return UA.match(/msie (\d+\.\d+)/i)[1];
    }else if(~UA.toLowerCase().indexOf('trident') && ~UA.indexOf('rv')){
        return UA.match(/rv:(\d+\.\d+)/)[1];
    }
    return false;
}

function resizeJqGrid(grid_selector) {
	var ps = getPageSize();
	var ieVersion =  IETester(navigator.userAgent);
	if(ieVersion=="7.0") {//ie7会容易出横向滚动条
		$(grid_selector).jqGrid('setGridWidth', ps.WinW-35);
	} else if(ieVersion=="6.0") {//ie6会容易出横向滚动条
		$(grid_selector).jqGrid('setGridWidth', ps.WinW-35);
	} else {
		$(grid_selector).jqGrid('setGridWidth', ps.WinW-25);
	}
}

//加法
Number.prototype.add = function(arg) {
    var r1, r2, m;
    try{r1 = this.toString().split(".")[1].length;}catch(e){r1 = 0;}
    try{r2 = arg.toString().split(".")[1].length;}catch(e){r2 = 0;}
    m = Math.pow(10, Math.max(r1, r2));
    return (this * m + arg * m) / m;
};
// 减法
Number.prototype.sub = function(arg) {
    return this.add(-arg);
};

// 乘法
Number.prototype.mul = function(arg) {
	return this * arg;
};
// 除法
Number.prototype.div = function(arg) {
    var t1 = 0, t2 = 0, r1, r2;
    try {t1 = this.toString().split(".")[1].length;}catch(e){}
    try {t2 = arg.toString().split(".")[1].length;}catch(e){}
    r1 = Number(this.toString().replace(".", ""));
    r2 = Number(arg.toString().replace(".", ""));
    return (r1 / r2) * Math.pow(10, t2 - t1);
};

//toFixed 兼容Chrome/Firefox，IE 四舍五入
Number.prototype.toFixed2 = function(d) {
	 var s=this+"";
     if(!d)d=0;
     if(s.indexOf(".")==-1)s+=".";
     s+=new Array(d+1).join("0");
     if(new RegExp("^(-|\\+)?(\\d+(\\.\\d{0,"+(d+1)+"})?)\\d*$").test(s)){
         var s="0"+RegExp.$2,pm=RegExp.$1,a=RegExp.$3.length,b=true;
         if(a==d+2){
             a=s.match(/\d/g);
             if(parseInt(a[a.length-1])>4){
                 for(var i=a.length-2;i>=0;i--){
                     a[i]=parseInt(a[i])+1;
                     if(a[i]==10){
                         a[i]=0;
                         b=i!=1;
                     }else break;
                 }
             }
             s=a.join("").replace(new RegExp("(\\d+)(\\d{"+d+"})\\d$"),"$1.$2");

         }if(b)s=s.substr(1);
         return (pm+s).replace(/\.$/,"");
     }return this+"";
};

//保留两位小数，自动补充0
Number.prototype.returnFloat = function(value) {
    if(!value || value=="null") {
    	return '0.00';
    }else{
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            return value;
        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
            }
            return value;
        }
    }
};

/**
 *xml对象转json对象
 *xmlObj:xml对象
 *nodename:节点路径('ROOT/ITEM')
 *isarray:true,强制返回数组对象
 **/
function xmlToJson(xmlObj, nodename, isarray) {
    var obj = $(xmlObj);
    var itemobj = {};
    var nodenames = "";
    var getAllAttrs = function (node) {//递归解析xml 转换成json对象
        var _itemobj = {};
        var notNull = false;
        var nodechilds = node.childNodes;
        var childlenght = nodechilds.length;
        var _attrs = node.attributes;
        var firstnodeName = "#text";
        try {
            firstnodeName = nodechilds[0].nodeName;
        } catch (e) {
        }
        if ((childlenght > 0 && firstnodeName != "#text") || _attrs.length > 0) {
            var _childs = nodechilds;
            var _childslength = nodechilds.length;
            var _fileName_ = "";
            if (undefined != _attrs) {
                var _attrslength = _attrs.length;
                for (var i = 0; i < _attrslength; i++) {//解析xml节点属性
                    var attrname = _attrs[i].nodeName;
                    var attrvalue = _attrs[i].nodeValue;
                    _itemobj[attrname] = attrvalue;
                }
            }
            for (var j = 0; j < _childslength; j++) {//解析xml子节点
                var _node = _childs[j];
                var _fildName = _node.nodeName;
                if ("#text" == _fildName) {
                    break;
                }
                ;
                if (_itemobj[_fildName] != undefined) {//如果有重复的节点需要转为数组格式
                    if (!(_itemobj[_fildName] instanceof Array)) {
                        var a = _itemobj[_fildName];
                        _itemobj[_fildName] = [a];//如果该节点出现大于一个的情况 把第一个的值存放到数组中
                    }
                }
                var _fildValue = getAllAttrs(_node);
                try {
                    _itemobj[_fildName].push(_fildValue);
                } catch (e) {
                    _itemobj[_fildName] = _fildValue;
                    _itemobj["length"] = 1;
                }
            }
        } else {
            _itemobj = (node.textContent == undefined) ? node.text : node.textContent;
        }
        return _itemobj;
    };
    if (nodename) {
        nodenames = nodename.split("/")
    }
    for (var i = 0; i < nodenames.length; i++) {
        obj = obj.find(nodenames[i]);
    }
    $(obj).each(function (key, item) {
        if (itemobj[item.nodeName] != undefined) {
            if (!(itemobj[item.nodeName] instanceof Array)) {
                var a = itemobj[item.nodeName];
                itemobj[item.nodeName] = [a];
            }
            itemobj[item.nodeName].push(getAllAttrs(item));
        } else {
            if (nodenames.length > 0) {
                itemobj[item.nodeName] = getAllAttrs(item);
            } else {
                itemobj[item.firstChild.nodeName] = getAllAttrs(item.firstChild);
            }
        }
    });
    if (nodenames.length > 1) {
        itemobj = itemobj[nodenames[nodenames.length - 1]];
    }
    if (isarray && !(itemobj instanceof Array) && itemobj != undefined) {
        itemobj = [itemobj];
    }
    return itemobj;
};
/*加载xml数据*/
function loadXml(str) {
    if (str == null) {
        return null;
    }
    var doc = str;
    try{
        doc = createXMLDOM();
        doc.async = false;
        doc.loadXML(str);
    }catch(e){
        doc = $.parseXML(str);
    }
    return doc;
};

function getRealLength(str){
    var realLength = 0;
    var charCode = -1;
    for (var i = 0; i < str.length; i++) {
        charCode = str.charCodeAt(i);
        if (charCode >= 0 && charCode <= 128){
            realLength += 1;  //英文，数字
        } else{
            realLength += 2;  //中文2字符
        }
    }
    return realLength;
}

/**
 * 去除hsf服务器信息
 * @param str
 * @returns
 */
function removeServerInfo(str){
	if(str){
		var pattern1 = /(\[HSF-Provider).*(\])/g;
		var pattern2 = /(Exception\s+From\s+Host:).*(\])/g;
		var pattern3 = /(error\s+message\s+:\s+\[).*(-)/g;

		return str.replace(pattern1, '').replace(pattern2, '').replace(pattern3, '');
	}
	return str;
}
function guid(len, radix) {
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
	var uuid = [], i;
	radix = radix || chars.length;

	if (len) {
		// Compact form
		for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
	} else {
		// rfc4122, version 4 form
		var r;

		// rfc4122 requires these characters
		uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
		uuid[14] = '4';

		// Fill in random data.  At i==19 set the high bits of clock sequence as
		// per rfc4122, sec. 4.1.5
		for (i = 0; i < 36; i++) {
			if (!uuid[i]) {
				r = 0 | Math.random() * 16;
				uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
			}
		}
	}

	return uuid.join('');
}
/**
 * 反转义xml特殊字符
 *
 * @returns
 */
String.prototype.unescapeXml = function () {
	return this.replace(/&lt;/g, "<")
		.replace(/&gt;/g, ">")
		.replace(/&amp;/g, "&");
};