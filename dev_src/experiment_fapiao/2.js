/**
 * 税务机关树查询
 */

foreseeFp.swjgTree = {
		treeFunction : {
			initTree : function(id, onAfterCheck){
				var treeId = id+'SwjgTree';
				var menuBtnId = id+'MenuBtn';
				var dropDownMenuId = id+'DropdownMenu';

				if($('#'+menuBtnId).length<=0){//已存在则不增加
					$('#'+id).after('<input id="'+menuBtnId+'" class="menuBtn" type="button" value="∨"  style=" padding:0cm 0cm 0cm 0cm;"/>')
						.after('<div id="'+dropDownMenuId+'" style="display:none; position:absolute;z-index:999;height:200px; min-width:198px; background-color:white;border:1px solid;overflow-y:auto;overflow-x:auto;"></div>');
					$("#"+dropDownMenuId).append('<ul id="'+treeId+'" class="ztree"></ul>');
				}

				$("#"+dropDownMenuId).unbind('click');
				//点击税务机关触发按钮
				$("#"+dropDownMenuId).click(function(){
				});

				// $("body").unbind('mousedown');
			    $("body").bind("mousedown", function(event){
			    	if (!(event.target.id == dropDownMenuId || $(event.target).parents("#"+dropDownMenuId).length>0)) {
						if($("#"+dropDownMenuId).css("display") != "none"){
				    		foreseeFp.swjgTree.eventFunction.hideMenu(id);
						}
			        }
			    });
				$("#"+menuBtnId).unbind('click');
			    $("#"+menuBtnId).bind("click", function(event){
					if($("#"+dropDownMenuId).css("display") == "none"){
						if(onAfterCheck != null && onAfterCheck != undefined){
							if (typeof onAfterCheck == 'function') {
								foreseeFp.swjgTree.callbackFunction.onAfterCheck = onAfterCheck;
							}
						}
						foreseeFp.swjgTree.eventFunction.showMenu(id);
					}
					else{
						foreseeFp.swjgTree.eventFunction.hideMenu(id);
					}
			    });
				$("#"+id).unbind('click');
			    $("#"+id).bind("click", function(){
			    	$("#"+menuBtnId).click();
			    });

				$.fn.zTree.init($('#'+treeId), foreseeFp.swjgTree.treeFunction.swjgTreeSetting);
			},
			swjgTreeSetting : {
				view: {
					selectedMulti: false  //设置是否允许同时选中多个节点
				},
				check: {
					enable: true,  //设置 zTree 的节点上是否显示 checkbox / radio
					chkStyle: "radio", //勾选框类型
					radioType: "all"  //radio 的分组范围,all表示在整棵树范围内当做一个分组
				},
				async: {
					enable: true,  //开启异步加载模式
					url : rootUrl+'/swry/common/swjg/getSwjg',
					autoParam:["id", "name", "level"]
//								otherParam:{"swjgDm":swjgDm,"isRoot":'Y'},
//								dataFilter: filter
				},
				callback: {
					onClick: function(event, treeId, treeNode) {
						if (treeNode && !treeNode.nocheck) {
							var zTree = $.fn.zTree.getZTreeObj(treeId);
					    	if(treeNode.checked){
								zTree.checkNode(treeNode,false,false,true);
					    	}else{
								zTree.checkNode(treeNode,true,false,true);
					    	}
					    }
					},
					onCheck: function(event, treeId, treeNode) {
						if (treeNode && treeNode.checked) {
							if (treeNode) {
						 		var zTree = $.fn.zTree.getZTreeObj(treeId);
						 		var nodes = zTree.getCheckedNodes();
						 		var selectTreeId = treeId.substring(0,treeId.indexOf('SwjgTree'));
						  		if (nodes.length != 0) {
						  			foreseeFp.swjgTree.eventFunction.hideMenu(selectTreeId);
						            $("#"+selectTreeId).val(treeNode.name);

									if (typeof foreseeFp.swjgTree.callbackFunction.onAfterCheck == 'function') {
										foreseeFp.swjgTree.callbackFunction.onAfterCheck.call(this,treeNode);
									}
						 		}else{
						 			$("#"+selectTreeId).val('');
						 		}
						    }
						}
					}
				}
			},
			reloadSelectTree : function(selectTreeId){
				foreseeFp.swjgTree.eventFunction.hideMenu(selectTreeId);
		 		var zTree = $.fn.zTree.getZTreeObj(selectTreeId+'SwjgTree');
		 		zTree.reAsyncChildNodes(null, "refresh");
//				$.fn.zTree.init($('#'+foreseeFp.swjgTree.treeId), foreseeFp.swjgTree.treeFunction.swjgTreeSetting);
			}
		},
		eventFunction : {
			showMenu : function(selectTreeId){
				var selectTree = $("#"+selectTreeId);
				var selectOffset = $("#"+selectTreeId).offset();
				/*.css({left:selectOffset.left+"px",top:selectOffset.top+selectTree.outerHeight()+"px"})*/
				var dropDownMenuId = selectTreeId+'DropdownMenu';
				$("#"+dropDownMenuId).slideDown("fast");
			},
			hideMenu : function(selectTreeId) {
				var dropDownMenuId = selectTreeId+'DropdownMenu';
				$("#"+dropDownMenuId).fadeOut("fast");
			}
		},
		callbackFunction : {
			onAfterCheck : function(treeNode){}
		}
}