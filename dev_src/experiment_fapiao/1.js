var dzdaCommon = {
	showPdf : function(lsh,ywlx,fplx){
		var pdfUrl = rootUrl+'/getpdf?lsh='+lsh+'&ywlxdm='+ywlx+'&fplx='+fplx;
		var options = {
				srcUrl : pdfUrl,
				title : "申报单PDF信息"
		};
		var index = foreseeFp.common.showDlgModel(options);
		layer.full(index);
	}
};
/**
 * 电子档案接口应用api
 */
var foreseeDzda = {

    /**
     * 请求报文对象 事项代码：SLSXA021011006 资料代码：000160 资料名称：《代开增值税发票缴纳税款申报单》
     *
     * 事项代码：SLSXA021011008 资料代码：000162 资料名称：《代开增值税发票缴纳税款申报单》
     *
     * 事项代码：未知 资料代码：未知 资料名称：未知《发票发放申请单》
     */
    requestBody : {
        /**
         * 同步资料登记信息，用于新增时
         */
        tbzldjxx : {
            "bussid" : "bussid",
            "taxpayernum" : nsrsbh,
            "nsrmc" : "",
            "nsrdjxh" : "",
            "idnumber" : "",
            "typecode" : "",
            "typename" : "",
            "submittime" : "",
            "systype" : "DS",
            "swjgdm" : swjgdm,
            "nsrswjgdm" : swjgdm,
            "bsry" : "14400000000",// 税务人员代码 新增时候暂时没有
            "slrymc" : "发票代开",// 税务人员名称 新增时候暂时没有
            "isaudit" : "1",
            "isca" : "",
            "cjh" : "",
            "links" : [ {
                "systype" : "DS",
                "bussid" : "bussid",
                "blbj" : "1"
            } ],
            "attachments" : [ {
                "zltm" : "",
                "zlkey" : "zlkey",
                "zllxbm" : "A",
                "zltjsj" : "",
                "wsswjg" : "",
                "jswsid" : "",
                "zlbm" : "",
                "zltrue" : "1",
                "xzfwdz" : "xzfwdz",
                "lists" : [ {
                    "fjid" : "fjid",
                    "fjmc" : "",
                    "wjdx" : "wjdx",
                    "cainfo" : ""
                } ]
            } ]
        },
        addzlItem : {
            "bussid" : "bussid",
            "systype" : "DS",
            "attachments" : [ {
                "zlkey" : "zlkey",
                "zllxbm" : "A",
                "xzfwdz" : "xzfwdz",
                "zlbm" : "",
                "attrtype" : "2",
                "zltjsj" : "2017-02-20 21:22:12",
                "wsswjg" : "",
                "jswsid" : "",
                "lists" : [ {
                    "fjid" : "fjid",
                    "fjmc" : "",
                    "wjdx" : "",
                    "cainfo" : ""
                } ]
            } ]
        }
    }
};
var foreseeDzda_util = {
    /**
     * 获取时间戳
     *
     * @returns {String}
     */
    getTime : function(formartter) {
        var time = new Date();
        var second = foreseeDzda_util.formart(time.getSeconds());
        var minute = foreseeDzda_util.formart(time.getMinutes());
        var hour = foreseeDzda_util.formart(time.getHours());
        var year = foreseeDzda_util.formart(time.getFullYear());
        var day = foreseeDzda_util.formart(time.getDate());
        var month = foreseeDzda_util.formart(time.getMonth() + 1);
        if (formartter === "yyyy-MM-dd hh:MM:dd") {
            return year + '-' + month + '-' + day + ' ' + hour + ':' + minute
                + ':' + second;
        } else {
            return year + '' + month + '' + day + '' + hour + '' + minute + ''
                + second;
        }
    },
    /**
     * 格式化小于10的数字
     *
     * @param data
     * @returns {String}
     */
    formart : function(data) {
        if (data < 10) {
            data = "0" + data;
        }
        return data;
    },
    /**
     * 同步电子档案
     */
    tbdzdaxx : function(method, body, lsh, ywlx) {
        $.ajax({
            url : rootUrl + '/savepdf',
            method : 'get',
            contentType : 'application/json',
            async : true,
            data : {
                "lsh" : lsh,
                "ywlxdm" : ywlx,
                "method" : method,
                "json" : JSON.stringify(body)
            },
            dataType : "json",
            success : function(data) {
//				console.log(data);
//				var code = data.code;
//				var msg = data.msg;
//				if (code == '0') {

//				} else {
                // layer.alert(msg, {icon : 2, title : '温馨提示', skin :
                // 'layer-ext-moon', offset : '200px'});
//				}
            },
            error : function(data) {
                // layer.alert('请求服务失败，请稍后再试！', {icon : 2, title : '温馨提示', skin
                // : 'layer-ext-moon', offset : '200px'});
            },
            complete : function(data) {

            }
        });
    }
};
var dadzCommon = {
    /**
     * TODO 模板待完善 缺少领购的数据 上传PDF凭证 type 默认0 新增 1：修改
     */
    doUploadPDF : function(lsh, type, ywlx,swjg_dm_zrr) {
        var body = foreseeDzda.requestBody.addzlItem;
        var method = "addzlItem";
        if (type == '0') {
            body = foreseeDzda.requestBody.tbzldjxx;
            method = 'tbzldjxx';// 新增电子档案
            if (ywlx == '02') {
                body.attachments[0].zlbm = "000160";
                body.typecode = "SLSXA021011006";
                body.typename = "《代开增值税发票缴纳税款申报单》";
            } else if (ywlx == '05'|| ywlx == '07') {
                body.attachments[0].zlbm = "000162";
                body.typecode = "SLSXA021011008";
                body.typename = "《代开增值税发票缴纳税款申报单》";
            } else if (ywlx == '01') {
                body.attachments[0].zlbm = "未知";
                body.typecode = "未知";
                body.typename = "未知领购";
            }
            body.submittime = foreseeDzda_util.getTime('yyyy-MM-dd hh:MM:dd');
        } else if (type == '1') {
            // 新增电子档案
            body.attachments[0].attrtype = "2";
            if (ywlx == '02') {
                body.attachments[0].zlbm = "000160";
            } else if (ywlx == '05'|| ywlx == '07') {
                body.attachments[0].zlbm = "000162";
            } else if (ywlx == '01') {
                body.attachments[0].zlbm = "未知";
            }
        }
        if(swjgdm == null || swjgdm == ""){
            body.swjgdm = swjg_dm_zrr;
            body.nsrswjgdm = swjg_dm_zrr;
        }
        body.attachments[0].zltjsj = foreseeDzda_util.getTime('yyyy-MM-dd hh:MM:dd');
        foreseeDzda_util.tbdzdaxx(method, body, lsh, ywlx);
    }
};