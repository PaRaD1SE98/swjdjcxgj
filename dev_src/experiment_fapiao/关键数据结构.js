jQuery(function($) {
	//foreseeFp.ggcx.reloadTree();//税务机关树
	$("#lst_kprdm").append("<option value='"+czydm+"'>"+czymc+"</option>");

//	var options="<option value='zzszyfp' >增值税专用发票</option>";
//    options+="<option value='zzsptfp' >增值税普通发票</option>";
//    options+="<option value='zzsdzfp' >增值税电子发票</option>";
//	$("#lst_fplx").empty();  // 先清空再加值
//	$("#lst_fplx").append(options);
	/* region 初始加载 */
	foreseeFp.common.initDatePlugin('txt_tjrqq','txt_tjrqz');
	$("#lst_fplx").change(function(){
		var fplx=$("#lst_fplx").val();
		if(fplx == 'zzsdzfp'){
			$("#hid_dzfpbz").val("Y");
		}else{
			$("#hid_dzfpbz").val("N");
		}
	});
	foreseeFp.ggcx.searchField.addSearchField();

	var queryType = $("#lst_fplx").val();
	foreseeFp.ggcx.gridFunction.grid(queryType, 'N');// 普通发票申请单查询
	foreseeFp.ggcx.gridFunction.grid2();// 普通发票申请单查询

	//foreseeFp.ggcx.searchField.removeSearchField();

	$(window).resize(function() {
		$(grid_selector).setGridWidth(document.body.clientWidth - 24, true);
	});
	$(grid_selector).setGridWidth(document.body.clientWidth - 24, true);

	/**
	 * 初始化申请单状态下拉列表
	 */
	foreseeFp.ggcx.eventFunction.initSqdztList();

	$("#cx_more").toggle(//查询条件动态下拉/隐藏按钮
		function(){
			$("#cx_more").html("收起<i></i>");
			$("#cx_more").addClass("opened");
			$("ul:eq(1)").css("display","block");
		},
		function(){
			$("#cx_more").html("更多<i></i>");
			$("#cx_more").removeClass("opened");
			$("ul:eq(1)").css("display","none");
		}
	);
	/** 发票公共查询对打印* */
	$("#a_print").click(function() {
		foreseeFp.ggcx.eventFunction.print();
	});

	//导出发票公共查询列表
	$("#a_export").bind("click", function() {
		foreseeFp.ggcx.exportGgcxXx();
	});

	$("#txt_skfnsrsbh").blur(function() {
		//如果查询条件的纳税人识别号有值则不限制日期起止的选择范围
		var isLimit = true;
		if($(this).val()){
			isLimit = false;
		}
		//laydate不支持重新渲染，通过删掉原来的input来重新渲染
		$('#txt_tjrqq').parent().append($('#txt_tjrqq').remove().removeAttr("lay-key").prop("outerHTML"));
		$('#txt_tjrqz').parent().append($('#txt_tjrqz').remove().removeAttr("lay-key").prop("outerHTML"));
		foreseeFp.common.initDatePlugin('txt_tjrqq','txt_tjrqz',isLimit);
	});

	var setting = {
		view: {
			selectedMulti: false
		},
		check: {
			enable: true,
			chkStyle: "radio",
			radioType: "all"
		},
		async: {
			enable: true,
			url:rootUrl+"/swry/common/swjg/getSwjg",
			autoParam:["id", "name", "level"],
			//otherParam:{"swjgdm":swjgdm,djxh:"","lcLqfs":lcLqfs,"ywlx":"02"},
			dataFilter: filter
		},
		callback: {
			beforeClick: beforeClick,
			beforeAsync: beforeAsync,
			onAsyncError: onAsyncError,
			onAsyncSuccess: onAsyncSuccess,
			onCheck: onCheck,
			onClick: onClick
		}
	};
	$.fn.zTree.init($("#wdTree"), setting);

	/**
	 * ztree function
	 */
	function filter(treeId, parentNode, childNodes) {
		if (!childNodes) return null;
		for (var i=0, l=childNodes.length; i<l; i++) {
			childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
		}
		return childNodes;
	}
	function beforeClick(treeId, treeNode) {
	}
	function beforeAsync(treeId, treeNode) {
	}
	function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
	}
	function onAsyncSuccess(event, treeId, treeNode, msg) {
	}
	function onCheck(event, treeId, treeNode) {
		if (treeNode && treeNode.checked) {
//  			var tmp_swjg=swjgdm.substring(0,5)+"000000";
//  			if(treeNode.id==tmp_swjg){
//  				$("#hid_swjg").val("");
//  			}else{
			/*if(treeNode.isParent==true){
				$("#hid_swjg").val(treeNode.id+":isParent");
			}else{
				$("#hid_swjg").val(treeNode.id+":notParent");
			}*/
//  			}
			$("#hid_swjg").val(treeNode.id);
		}else{
			$("#hid_swjg").val("");
		}
		if (treeNode) {
			var zTree = $.fn.zTree.getZTreeObj("wdTree");
			var nodes = zTree.getCheckedNodes();
			if (nodes.length != 0) {
				$("#selectTree").val(treeNode.name);
				hideMenu();
			}else{
				$("#selectTree").val('');
			}
		}
	}

	function onClick(event, treeId, treeNode) {
		if (treeNode && !treeNode.nocheck) {
			var zTree = $.fn.zTree.getZTreeObj("wdTree");
			if(treeNode.checked){
				zTree.checkNode(treeNode,false,false,true);
			}else{
				zTree.checkNode(treeNode,true,false,true);
			}
		}
	}

	function showMenu(){
		var selectTree = $("#selectTree");
		var selectOffset = $("#selectTree").offset();
		$("#DropdownMenuBackground").css({left:selectOffset.left+"px",top:selectOffset.top+selectTree.outerHeight()+"px"}).slideDown("fast");
	}
	function hideMenu() {
		$("#DropdownMenuBackground").fadeOut("fast");
	}
	function reloadTree(){
		hideMenu();
		$.fn.zTree.init($("#wdTree"), setting);
	}
	reloadTree();
	$("#menuBtn").bind("click", function(){
		if($("#DropdownMenuBackground").css("display") == "none"){
			showMenu();
		}
		else{
			hideMenu();
		}
	});
	$("#selectTree").bind("click", function(){
		$("#menuBtn").click();
	});
	$("body").bind("mousedown", function(event){
		if (!(event.target.id == "DropdownMenuBackground" || $(event.target).parents("#DropdownMenuBackground").length>0)) {
			hideMenu();
		}
	});

});

var grid_selector = "#grid-table";
var pager_selector = "#grid-pager";

var grid_selector_for_print = "#grid-table_for_print";
var pager_selector_for_print = "#grid-pager_for_print";

foreseeFp.ggcx = {
	version : "1.0.0",

	urls : {
		// 专用发票查询url
		zyfpQueryApiUrl : rootUrl
			+ "/swry/fpDkZzsZyfp/queryZyfpGyxx",
		//+ "/FpfwServlet.do?sid=Omni.fp.dkfp.DkfpgyxxQueryService:fpcx",
		// 普通发票查询url
		ptfpQueryApiUrl : rootUrl
			//+ "/FpfwServlet.do?sid=Omni.fp.dkfp.DkfpZzsPtfpGyxxCxService:fpcx"
			+ "/swry/dkfp/ggcx/getPtfpGyxx"
	},

	gridFunction : {
		grid : function(queryType, cxbz) {
			$(grid_selector).jqGrid({
				//url : queryUrl,
				//mtype : "POST",
				//datatype : "xml",
				datatype : "xmlstring",
				xmlDataNodePath : "items/item",
				page:1,
				height : 325,
				colNames : [ '申请单号', '申请日期', '收款方纳税人名称',
					'收款方纳税人识别号', '税票号码', '发票代码', '发票号码',
					'金额', '税额', '领取方式','取票网点', '申请单状态' ,'审核时间', '审核人', '出租房屋标志','订单号','jq_row_xml'],
				colModel : [
					{
						name : 'lsh',
						index : 'lsh',
						align : 'center',
						width : 110,
						formatter : function(cellvalue,
											 options, rowObject) {
							var optType = $(
								rowObject.childNodes[7])
								.text(), ckStr = '<a href="#" style="text-decoration:underline;font-weight:bold;color:red;" onclick="foreseeFp.ggcx.view(\''
								+ $(
									rowObject.childNodes[0])
									.text()
								+ '\',\''
								+ $(
									rowObject.childNodes[12])
									.text()
								+ '\');">'
								+ cellvalue
								+ '</a>';
							return ckStr;
						},
						unformat : function(cellvalue,
											options, cell) {
							return $('a', cell).text();
						}
					}, {
						name : 'tjrq',
						index : 'tjrq',
						align : 'center',
						width : 90,
						formatter : 'date',
						formatoptions : {
							srcformat : 'Y-m-d H:i:s',
							newformat : 'Y-m-d H:i:s'
						}
					}, {
						name : 'xfnsrmc',
						index : 'xfnsrmc',
						align : 'left',
						width : 240
					}, {
						name : 'nsrsbh',
						index : 'nsrsbh',
						align : 'center',
						width : 160
					}, {
						name : 'sphm',
						index : 'sphm',
						align : 'center',
						width : 90
					}, {
						name : 'fpDm',
						index : 'fpDm',
						align : 'center',
						width : 90
					}, {
						name : 'fphm',
						index : 'fphm',
						align : 'center',
						width : 90
					}, {
						name : 'je',
						index : 'je',
						align : 'right',
						width : 90
					}, {
						name : 'se',
						index : 'se',
						align : 'right',
						width : 90
					}, {
						name : 'lpfsmc',
						index : 'lpfsmc',
						align : 'center',
						width : 140
					},  {
						name : 'wdmc',
						index : 'wdmc',
						align : 'center',
						width : 140
					},
					{
						name : 'fpztmc',
						index : 'fpztmc',
						align : 'center',
						width : 140
					},
					{
						name : 'sprq',
						index : 'sprq',
						align : 'center',
						width : 90,
						formatter : 'date',
						formatoptions : {
							srcformat : 'Y-m-d H:i:s',
							newformat : 'Y-m-d H:i:s'
						}
					},
					{
						name : 'sprmc',
						index : 'sprmc',
						align : 'center',
						width : 140
					},
					{
						name : 'czfwbz',
						index : 'czfwbz',
						align : 'center',
						hidden : true,
						width : 140
					}, {
						name : 'ddh',
						index : 'ddh',
						align : 'center',
						width : 140,
						formatter : function(cellvalue,options, rowObject) {
							var kddhHtml = "";
							if(cellvalue != null){
								kddhHtml = "<a href='###' style='text-decoration:underline;font-weight:bold;color:red;'" +
									" onclick='foreseeFp.ggcx.popupViewDdh("+cellvalue+")'>"+cellvalue+"</a>";
							}
							return  kddhHtml;
						}
					}, {
						name : 'jq_row_xml',
						index : 'jq_row_xml',
						hidden : true
					} ],
				multiboxonly : true,
				viewrecords : true,
				//loadonce : true,
				rowNum : 10,
				rowList : [10, 20, 30, 50],
				pager : pager_selector,
				multiselect : true,
				rownumbers : false,
				localPage : false,
				onPaging:function(pageBtn){
					var re_page = $(grid_selector).getGridParam('page');//获取返回的当前页
					var re_rowNum= $(grid_selector).getGridParam('rowNum');//获取每页数
					var re_total= $(grid_selector).getGridParam('lastpage');//获取总页数
					if(pageBtn === "records"){
						//获取要显示的总数
						re_rowNum = $("#grid-pager").find(".ui-pg-selbox").val();
						foreseeFp.ggcx.eventFunction.doQueryPage(Number(re_page), re_rowNum);
					}
					if(pageBtn === "user"){
						re_page = $("#grid-pager").find(".ui-pg-input").val();
						foreseeFp.ggcx.eventFunction.doQueryPage(Number(re_page), re_rowNum);
					}
					if(pageBtn==="next_grid-pager"){
						foreseeFp.ggcx.eventFunction.doQueryPage(Number(re_page).add(1), re_rowNum);
					}
					if(pageBtn==="prev_grid-pager"){
						if (re_page != 1){
							re_page = Number(re_page).sub(1);
						}
						foreseeFp.ggcx.eventFunction.doQueryPage(re_page, re_rowNum);
					}
					if (pageBtn==="first_grid-pager"){
						foreseeFp.ggcx.eventFunction.doQueryPage(1, re_rowNum);
					}
					if (pageBtn==="last_grid-pager"){
						foreseeFp.ggcx.eventFunction.doQueryPage(re_total, re_rowNum);
					}
				},
				loadComplete : function(result) {
					// 复制数据到grid2
					if(typeof result != 'undefined'){
						var dataStr = xmlToString(result);
						var start='<hlmxs>';
						var end='</hlmxs>';
						//alert(dataStr.indexOf('<hlmxs>'));
						var dataStr1;
						while(dataStr.indexOf('<hlmxs>') > 0){
							dataStr1=foreseeFp.ggcx.eventFunction.cutstr(dataStr,start,end);
							dataStr=dataStr.replace(dataStr1,'');
							//	alert(dataStr);
						}
						copyDataToGrid(grid_selector_for_print, dataStr);
					}

				},
				autowidth : false

			}).navGrid(pager_selector, {
				// 工具栏
				edit : false,
				add : false,
				del : false,

				search : false,
				refresh : false
			});
			//foreseeFp.ggcx.eventFunction.doQueryPage();
		},
		grid2 : function() {
			var czfwbz=$("#lst_czfwbz").val();
			var colNamestr;
			if(czfwbz=="Y"){
				colNamestr=[ '申请单号', '申请日期', '出租方纳税人名称',
					'出租方纳税人识别号', '税票号码', '发票代码', '发票号码',
					'金额', '税额', '领取方式', '申请单状态','订单号' ]
			}else{
				colNamestr=[ '申请单号', '申请日期', '收款方纳税人名称',
					'收款方纳税人识别号', '税票号码', '发票代码', '发票号码',
					'金额', '税额', '领取方式', '申请单状态','订单号' ]
			}
			$(grid_selector_for_print).jqGrid({
				datatype : "local",
				page : 1,
				height : 325,
				colNames : colNamestr,
				colModel : [
					{
						name : 'lsh',
						index : 'lsh',
						align : 'center',
						width : 100,

					}, {
						name : 'tjrq',
						index : 'tjrq',
						align : 'center',
						width : 60,
						formatter : 'date',
						formatoptions : {
							srcformat : 'Y-m-d H:i:s',
							newformat : 'Y-m-d H:i:s'
						}

					}, {
						name : 'xfnsrmc',
						index : 'xfnsrmc',
						align : 'left',
						width : 180
					}, {
						name : 'nsrsbh',
						index : 'nsrsbh',
						align : 'center',
						width : 160
					}, {
						name : 'sphm',
						index : 'sphm',
						align : 'center',
						width : 70
					}, {
						name : 'fpDm',
						index : 'fpDm',
						align : 'center',
						width : 90
					}, {
						name : 'fphm',
						index : 'fphm',
						align : 'center',
						width : 90
					}, {
						name : 'je',
						index : 'je',
						align : 'right',
						width : 60
					}, {
						name : 'se',
						index : 'se',
						align : 'right',
						width : 60
					}, {
						name : 'lpfsmc',
						index : 'lpfsmc',
						align : 'center',
						width : 90
					}, {
						name : 'fpztmc',
						index : 'fpztmc',
						align : 'center',
						width : 80
					} , {
						name : 'ddh',
						index : 'ddh',
						align : 'center',
						width : 140
					}],

				viewrecords : true,
				loadonce : true,
				rowNum : 1000,
				rowList : [10, 20, 30],
				pager : pager_selector_for_print,
				altRows : true,
				// toppager: true,

				multiselect : false,
				// multikey: "ctrlKey",
				// multiboxonly : true,
				rownumbers : false,

				loadComplete : function() {

				},
				autowidth : false

			}).navGrid(pager_selector_for_print, {
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true
			});
		}
	},

	view : function(lsh,czfwbz) {
		var fplx = $("#lst_fplx").val();
		var url = rootUrl + '/user/special/zzszpdk/ggcx/sbd?lsh=' + lsh + '&czfwbz='+czfwbz;
		var title = "网上代开增值税专用发票缴纳税款申报单";
		var title_1 = "网上代开增值税普通发票缴纳税款申报单";
		var url_1 = rootUrl + '/user/plain/zzsppdk/ggcx/sbd?lsh=' + lsh + '&fplx='+fplx+ '&action='+'0'+ '&dateTimps=' + new Date().getTime()+ '&czfwbz='+czfwbz + '&dzfpbz=' + $("#hid_dzfpbz").val();
		if(fplx == 'zzsptfp' || fplx == 'zzsdzfp'){
			options = {
				title : title_1,
				srcUrl : url_1
			};
			foreseeFp.common.showDlgModel(options);
		}else if(fplx == 'zzszyfp'){
			options = {
				title : title,
				srcUrl : url
			};
			foreseeFp.common.showDlgModel(options);
		}
	},
	eventFunction : {
		/**
		 * 截取字符串，处理打印金额重复
		 * */
		cutstr: function(text,start,end){
			var s = text.indexOf(start);
			if(s>-1){
				var text2 = text.substr(s);
				var s2 = text2.indexOf(end);
				if(s2>-1){
					result = text2.substr(0,s2+end.length);
				}else result = '';
			}else result = '';
			return result;
		},
		/**
		 * 执行查询操作
		 */
		doQueryPage : function(pageNo,pageSize) {
			if($("#txt_tjrqq").val()=='' || $("#txt_tjrqq").val()==null){
				layer.alert('申请日期起不能为空！', { icon : 2, title : '温馨提示', skin : 'layer-ext-moon'});
				return false;
			}
			if($("#txt_tjrqz").val()=='' || $("#txt_tjrqz").val()==null){
				layer.alert('申请日期止不能为空！', { icon : 2, title : '温馨提示', skin : 'layer-ext-moon'});
				return false;
			}
			foreseeFp.ggcx.searchField.addSearchField();
			var queryType = $("#lst_fplx").val();
			var queryUrl = "";
			if (queryType == "zzsptfp" || queryType == "zzsdzfp") {
				queryUrl = foreseeFp.ggcx.urls.ptfpQueryApiUrl;
			} else if (queryType == "zzszyfp") {
				queryUrl = foreseeFp.ggcx.urls.zyfpQueryApiUrl;
			}
			//获取查询条件出租房屋标志并修改表头
			var czfwbz=$("#lst_czfwbz").val();
			if(czfwbz=="Y"){
				$("#jqgh_grid-table_xfnsrmc").html("出租方纳税人名称");
				$("#jqgh_grid-table_nsrsbh").html("出租方纳税人识别号");
			}else{
				$("#jqgh_grid-table_xfnsrmc").html("收款方纳税人名称");
				$("#jqgh_grid-table_nsrsbh").html("收款方纳税人识别号");
			}
			var queryData = {
				action:"fpcx",
				pageNo:$(grid_selector).getGridParam("page"),
				pageSize:$(grid_selector).getGridParam("rowNum"),
				zfbz:$('#hid_zfbz').val(),
				dzfpbz:$('#hid_dzfpbz').val(),
				tjrqq:$('#txt_tjrqq').val(),
				tjrqz:$('#txt_tjrqz').val(),
				lpfs:$('#lst_lpfs').val(),
				nsrsbh:$('#txt_skfnsrsbh').val(),
				//fplx:$('#lst_fplx').val(),
				fpzt:$('#lst_fpzt').val(),
				lsh:$('#txt_lsh').val(),
				fpdm:$('#txt_fpdm').val(),
				fphm:$('#txt_fphm').val(),
				kprdm:$('#lst_kprdm').val(),
				lyqd:$('#lst_lyqd').val(),
				czfwbz:$('#lst_czfwbz').val(),
				swjgggcx:$('#hid_swjg').val()
			};
			if(pageNo != null){
				queryData.pageNo = pageNo;
			}
			if(pageSize != null){
				queryData.pageSize = pageSize;
			}
			foreseeFp.common.ajaxJson(queryUrl, queryData, false, callback);
			function callback(result) {
				if (result.code == "0") {
					var bodyArr = result.body;
					var count = bodyArr[0].totalCount;
					var gyxxList = bodyArr[0].items;
					var xmlStr = "<items>";
					xmlStr += "<count>" + count + "</count>";
					xmlStr += "<pageSize>" + bodyArr[0].pageSize + "</pageSize>";
					xmlStr += "<pageNo>" + bodyArr[0].pageNo + "</pageNo>";
					if(gyxxList !=null && gyxxList.length >0){
						for (var i = 0; i < gyxxList.length; i++) {
							var fpVo = gyxxList[i];
							xmlStr += "<item>";
							var tjrq = fpVo.tjrq==null?"":fpVo.tjrq;
							var sprq = fpVo.sprq==null?"":fpVo.sprq;
							var sprmc = fpVo.sprmc==null?"":fpVo.sprmc;
							var xfnsrmc = fpVo.xhfnsrmc==null?"":fpVo.xhfnsrmc;
							var nsrsbh = fpVo.xhfnsrsbh==null?"":fpVo.xhfnsrsbh;
							var sphm = fpVo.sphm==null?"":fpVo.sphm;
							var fpdm = fpVo.fpDm==null?"":fpVo.fpDm;
							var fphm = fpVo.fphm==null?"":fpVo.fphm;
							var je = fpVo.je==null?"":fpVo.je;
							var se = fpVo.se==null?"":fpVo.se;
							var lpfsmc = "";
							if ($("#lst_fplx").val() == "zzsptfp" || $("#lst_fplx").val() == "zzsdzfp") {
								lpfsmc = fpVo.lpfsMc==null?"":fpVo.lpfsMc;
							} else if ($("#lst_fplx").val() == "zzszyfp") {
								lpfsmc = fpVo.lpfsmc==null?"":fpVo.lpfsmc;
							}
							var wdmc = fpVo.wdmc==null?"":fpVo.wdmc;
							var fpztmc = fpVo.fpztmc==null?"":fpVo.fpztmc;
							var czfwbz = fpVo.czfwbz==null?"":fpVo.czfwbz;
							var ddh = fpVo.ddh==null?"":fpVo.ddh;
							xmlStr += "<lsh>" + fpVo.xlh + "</lsh>";
							xmlStr += "<tjrq>" + tjrq + "</tjrq>";
							xmlStr += "<xfnsrmc>" + xfnsrmc + "</xfnsrmc>";
							xmlStr += "<nsrsbh>" + nsrsbh + "</nsrsbh>";
							xmlStr += "<sphm>" + sphm + "</sphm>";
							xmlStr += "<fpdm>" + fpdm + "</fpdm>";
							xmlStr += "<fphm>" + fphm + "</fphm>";
							xmlStr += "<je>" + je + "</je>";
							xmlStr += "<se>" + se + "</se>";
							xmlStr += "<lpfsmc>" + lpfsmc + "</lpfsmc>";
							xmlStr += "<wdmc>" + wdmc + "</wdmc>";
							xmlStr += "<fpztmc>" + fpztmc + "</fpztmc>";
							xmlStr += "<czfwbz>" + czfwbz + "</czfwbz>";
							xmlStr += "<ddh>" + ddh + "</ddh>";
							xmlStr += "<sprq>" + sprq + "</sprq>";
							xmlStr += "<sprmc>" + sprmc + "</sprmc>";
							xmlStr += "</item>";
						}

						var hlmxList = gyxxList[0].hlmxList;
						if(hlmxList != null && hlmxList.length >0){
							xmlStr += "<hlmxs>";
							for(var i = 0; i < hlmxList.length; i++){
								var hwmx = hlmxList[i];
								xmlStr += "<hlmx>";
								var hwlwmc = hwmx.hwlwmc==null?"":hwmx.hwlwmc;
								var ggxh = hwmx.ggxh==null?"":hwmx.ggxh;
								var dwslDm = hwmx.dwslDm==null?"":hwmx.dwslDm;
								var hlsl = hwmx.hlsl==null?"":hwmx.hlsl;
								var hldj = hwmx.hldj==null?"":hwmx.hldj;
								var xse = hwmx.xse==null?"":hwmx.xse;
								var kce = hwmx.kce==null?"":hwmx.kce;
								var bz = hwmx.bz==null?"":hwmx.bz;
								var je = hwmx.je==null?"":hwmx.je;
								var jylbDm = hwmx.jylbDm==null?"":hwmx.jylbDm;
								xmlStr += "<hwlwmc>" + hwlwmc + "</hwlwmc>";
								xmlStr += "<ggxh>" + ggxh + "</ggxh>";
								xmlStr += "<dwslDm>" + dwslDm + "</dwslDm>";
								xmlStr += "<hlsl>" + hlsl + "</hlsl>";
								xmlStr += "<hldj>" + hldj + "</hldj>";
								xmlStr += "<xse>" + xse + "</xse>";
								xmlStr += "<kce>" + kce + "</kce>";
								xmlStr += "<bz>" + bz + "</bz>";
								xmlStr += "<je>" + je + "</je>";
								xmlStr += "<jylbDm>" + jylbDm + "</jylbDm>";
								xmlStr += "</hlmx>";
							}
							xmlStr += "</hlmxs>";
						}
					}
					xmlStr += "</items>";
					copyDataToGrid(grid_selector, xmlStr);

					/*// 复制数据到grid2
					var dataStr = xmlStr;
					var start='<hlmxs>';
					var end='</hlmxs>';
					//alert(dataStr.indexOf('<hlmxs>'));
					var dataStr1;
					while(dataStr.indexOf('<hlmxs>') > 0){
						dataStr1=foreseeFp.ggcx.eventFunction.cutstr(dataStr,start,end);
						dataStr=dataStr.replace(dataStr1,'');
						//	alert(dataStr);
					}*/
					//copyDataToGrid(grid_selector_for_print, dataStr);
				}else{
					layer.alert(result.msg,{icon:2,title:"温馨提示"});
				}
			}
			//重新加载表单数据
			/*$(grid_selector).jqGrid('setGridParam', {
				datatype : "xmlstring",
				datastr:dataStr,
				//url : queryUrl,
				page : 1
			}).trigger("reloadGrid");*/ // 重新载入

			//foreseeFp.ggcx.searchField.removeSearchField();
		},

		/**
		 * 初始化申请单状态下拉列表
		 */
		initSqdztList : function() {
			$("#lst_fpzt").append("<option value='02'>已提交</option>");
			$("#lst_fpzt").append("<option value='10'>审核已通过</option>");
			$("#lst_fpzt").append("<option value='11'>审核不通过</option>");
			$("#lst_fpzt").append("<option value='20'>已开税票</option>");
			$("#lst_fpzt").append("<option value='30'>已扣税款</option>");
			$("#lst_fpzt").append("<option value='50'>打印成功（待校对）</option>");
			$("#lst_fpzt").append("<option value='52'>打印错误（待校对）</option>");
			$("#lst_fpzt").append("<option value='51'>发票未打（待校对）</option>");
			$("#lst_fpzt").append("<option value='60'>打印成功</option>");
			$("#lst_fpzt").append("<option value='62'>打印错误</option>");
			$("#lst_fpzt").append("<option value='61'>发票未打</option>");
			$("#lst_fpzt").append("<option value='21'>增值税已开税票</option>");
			$("#lst_fpzt").append("<option value='31'>增值税已扣税款</option>");
			$("#lst_fpzt").append("<option value='22'>城市维护建设税已开税票</option>");
			$("#lst_fpzt").append("<option value='32'>城市维护建设税已扣税款</option>");
			$("#lst_fpzt").append("<option value='23'>教育费附加已开税票</option>");
			$("#lst_fpzt").append("<option value='33'>教育费附加已扣税款</option>");
			$("#lst_fpzt").append("<option value='24'>代征附加税已开税票</option>");
			$("#lst_fpzt").append("<option value='34'>代征附加税已扣税款</option>");
			$("#lst_fpzt").append("<option value='63'>发票已作废</option>");
			$("#lst_fpzt").append("<option value='70'>发票已投单</option>");
			$("#lst_fpzt").append("<option value='41'>发票已保存</option>");
			$("#lst_fpzt").append("<option value='80'>发票已领取</option>");
			$("#lst_fpzt").append("<option value='40'>已分配开票员</option>");
			$("#lst_fpzt").append("<option value='71'>已通知取单</option>");
			$("#lst_fpzt").append("<option value='90'>已生成快递单号</option>");
			$("#lst_fpzt").append("<option value='91'>已核对交接</option>");
			$("#lst_fpzt").append("<option value='92'>配送中</option>");
			$("#lst_fpzt").append("<option value='93'>已签收</option>");
			$("#lst_fpzt").append("<option value='94'>无法送达</option>");
			$("#lst_fpzt").append("<option value='98'>自助终端已处理</option>");
			$("#lst_fpzt").append("<option value='68'>防伪已红冲</option>");
			$("#lst_fpzt").append("<option value='69'>核心已红冲</option>");
		},
		/** 发票公共查询打印* */
		print : function() {
			var falgs=foreseeFp.ggcx.eventFunction.isSelectPrint();
			if(falgs!='falg'){
				var start='<hlmxs>';
				var end='</hlmxs>';
				var dataStr1;
				while(falgs.indexOf('<hlmxs>') > 0){
					dataStr1=foreseeFp.ggcx.eventFunction.cutstr(falgs,start,end);
					falgs=falgs.replace(dataStr1,'');
				}
				$(grid_selector_for_print).jqGrid('clearGridData');
				copyDataToGrid(grid_selector_for_print, falgs);
			}
			printGrid("grid-table_for_print", "发票公共查询", "开票日期:" + $("#txt_tjrqq").val() + "至" + $("#txt_tjrqz").val());
			location.reload();
		},
		//判斷是否選擇數據打印
		isSelectPrint :  function(){
			var falg='falg';
			var xmlToSend="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
			xmlToSend+="<items>"
			var rowIds = $('#grid-table').jqGrid('getGridParam', 'selarrrow');// 多行
			if(rowIds==null || rowIds.length==0) {
				return falg;
			}
			for (var i = 0; i < rowIds.length; i++) {
				var row = $("#grid-table").getRowData(rowIds[i]);
				xmlToSend+= row.jq_row_xml;
			}
			xmlToSend+="</items>";
			//alert(xmlToSend);
			return xmlToSend;
		}
	},

	// 查询条件
	searchField : {
		// 设置表单属性为查询条件
		addSearchField : function() {
			$("#hid_zfbz").attr("isSearchField", "true");
			$("#hid_dzfpbz").attr("isSearchField", "true");
			$("#txt_tjrqq").attr("isSearchField", "true");
			$("#txt_tjrqz").attr("isSearchField", "true");
			$("#lst_lpfs").attr("isSearchField", "true");
			$("#txt_skfnsrsbh").attr("isSearchField", "true");
			// $("#lst_fplx").attr("isSearchField", "true");
			$("#lst_fpzt").attr("isSearchField", "true");
			$("#txt_lsh").attr("isSearchField", "true");
			$("#txt_fpdm").attr("isSearchField", "true");
			$("#txt_fphm").attr("isSearchField", "true");
			$("#lst_kprdm").attr("isSearchField", "true");
			$("#lst_swjg").attr("isSearchField", "true");
		},
		// 移除查询条件属性
		/*removeSearchField : function() {
			$("#hid_zfbz").removeAttr("isSearchField");
			$("#txt_tjrqq").removeAttr("isSearchField");
			$("#txt_tjrqz").removeAttr("isSearchField");
			$("#lst_lpfs").removeAttr("isSearchField");
			$("#txt_skfnsrsbh").removeAttr("isSearchField");
			// $("#lst_fplx").removeAttr("isSearchField");
			$("#lst_fpzt").removeAttr("isSearchField");
			$("#txt_lsh").removeAttr("isSearchField");
			$("#txt_fpdm").removeAttr("isSearchField");
			$("#txt_fphm").removeAttr("isSearchField");
			$("#lst_czy").removeAttr("isSearchField");
		}*/
	},
	/**
	 * 快递单详情页
	 */
	popupViewDdh: function(ddh){
		var action = "view";
		var viIndex = layer.open({
			type : 2,
			title : '订单明细',
			shadeClose : true,
			shade : 0.5,
			offset: '20px',
			area : ['880px', '480px'],
			content : rootUrl+'/taxstaff/common/kddd?kddd='+ddh,
			cancel : function(index) {
				layer.close(index);
			}
		});
	},

	/**
	 * 导出发票公共查询excel
	 */
	exportGgcxXx : function(){
//		var queryType = $("#lst_fplx").val();
//		var xmlToSend="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//		xmlToSend+="<items>";
//	    xmlToSend+="<querytype>"+queryType+"</querytype>";
//		var rowIds = $(grid_selector).jqGrid('getGridParam', 'selarrrow');// 多行
//		if(rowIds==null || rowIds.length==0) {
//			layer.alert("请选择要导出的申请单！");
//			return false;
//		}
//		for (var i = 0; i < rowIds.length; i++) {
//		    var row = $(grid_selector).getRowData(rowIds[i]);
//		    xmlToSend+= row.jq_row_xml;
//		}
//		xmlToSend+="</items>";

		var queryType = $("#lst_fplx").val();
		var queryUrl = "";
		if (queryType == "zzsptfp" || queryType == "zzsdzfp") {
			queryUrl = foreseeFp.ggcx.urls.ptfpQueryApiUrl;
		} else if (queryType == "zzszyfp") {
			queryUrl = foreseeFp.ggcx.urls.zyfpQueryApiUrl;
		}
		foreseeFp.ggcx.getAllFpxx(queryUrl);//获取满足条件的所有数据
		var xmlToSend=$('#hid_export_data').val();
		$("#frm_export_xml").val(xmlToSend);
		$("#frm_export").submit();
	},

	getAllFpxx:function(queryUrl){
		var strUrl=queryUrl;
		var queryData = {
			action:"fpcx",
			pageNo:1,
			pageSize:1000,
			sfDm:sfDm,
			zfbz:$('#hid_zfbz').val(),
			nsrsbh:$('#txt_skfnsrsbh').val(),
			tjrqq:$('#txt_tjrqq').val(),
			tjrqz:$('#txt_tjrqz').val(),
			fpzt:$('#lst_fpzt').val(),
			lpfs:$('#lst_lpfs').val(),
			lsh:$('#txt_lsh').val(),
			fpdm:$('#txt_fpdm').val(),
			fphm:$('#txt_fphm').val(),
			kprdm:$('#lst_kprdm').val(),
			lyqd:$('#lst_lyqd').val(),
			czfwbz:$('#lst_czfwbz').val()
		};
		var queryType = $("#lst_fplx").val();
		if(queryType == "zzsdzfp"){
			queryData.dzfpbz = $('#hid_dzfpbz').val();
		}
		foreseeFp.common.ajaxJson(strUrl, queryData, false, callback);
		function callback(result) {
			if (result.code == "0") {
				var bodyArr = result.body;
				var count = bodyArr[0].totalCount;
				var gyxxList = bodyArr[0].items;
				var xmlStr = "<items>";
				xmlStr += "<count>" + count + "</count>";
				for (var i = 0; i < gyxxList.length; i++) {
					var fpVo = gyxxList[i];
					xmlStr += "<item>";
					var tjrq = fpVo.tjrq==null?"":fpVo.tjrq;
					var xfnsrmc = fpVo.xhfnsrmc==null?"":fpVo.xhfnsrmc;
					var nsrsbh = fpVo.xhfnsrsbh==null?"":fpVo.xhfnsrsbh;
					var sphm = fpVo.sphm==null?"":fpVo.sphm;
					var fpdm = fpVo.fpDm==null?"":fpVo.fpDm;
					var fphm = fpVo.fphm==null?"":fpVo.fphm;
					var je = fpVo.je==null?"":fpVo.je;
					var se = fpVo.se==null?"":fpVo.se;
					var lpfsmc = "";
					if ($("#lst_fplx").val() == "zzsptfp" || $("#lst_fplx").val() == "zzsdzfp") {
						lpfsmc = fpVo.lpfsMc==null?"":fpVo.lpfsMc;
					} else if ($("#lst_fplx").val() == "zzszyfp") {
						lpfsmc = fpVo.lpfsmc==null?"":fpVo.lpfsmc;
					}
					var wdmc = fpVo.wdmc==null?"":fpVo.wdmc;
					var fpztmc = fpVo.fpztmc==null?"":fpVo.fpztmc;
					var czfwbz = fpVo.czfwbz==null?"":fpVo.czfwbz;
					var ddh = fpVo.ddh==null?"":fpVo.ddh;
					xmlStr += "<lsh>" + fpVo.xlh + "</lsh>";
					xmlStr += "<tjrq>" + tjrq + "</tjrq>";
					xmlStr += "<xfnsrmc>" + xfnsrmc + "</xfnsrmc>";
					xmlStr += "<nsrsbh>" + nsrsbh + "</nsrsbh>";
					xmlStr += "<sphm>" + sphm + "</sphm>";
					xmlStr += "<fpdm>" + fpdm + "</fpdm>";
					xmlStr += "<fphm>" + fphm + "</fphm>";
					xmlStr += "<je>" + je + "</je>";
					xmlStr += "<se>" + se + "</se>";
					xmlStr += "<lpfsmc>" + lpfsmc + "</lpfsmc>";
					xmlStr += "<wdmc>" + wdmc + "</wdmc>";
					xmlStr += "<fpztmc>" + fpztmc + "</fpztmc>";
					xmlStr += "<czfwbz>" + czfwbz + "</czfwbz>";
					xmlStr += "<ddh>" + ddh + "</ddh>";
					xmlStr += "</item>";
				}
				xmlStr += "</items>";
				copyDataToGrid(grid_selector, xmlStr);
			}
		}
		// 拼接请求
		/*var fieldNames = new Array();
		var fieldValues = new Array();
		var i=0;

		fieldNames[i] = "pageNo";
		fieldValues[i] = "1";
		i++;

		fieldNames[i] = "pageSize";
		fieldValues[i] = "1000";
		i++;

		fieldNames[i] = "sfDm";
		fieldValues[i] = sfDm;
		i++;

		fieldNames[i] = "zfbz";
		fieldValues[i] = $('#hid_zfbz').val();
		i++;

		var queryType = $("#lst_fplx").val();
		if(queryType == "zzsdzfp"){
			fieldNames[i] = "dzfpbz";
			fieldValues[i] = $('#hid_dzfpbz').val();
			i++;
		}

		fieldNames[i] = "nsrsbh";
		fieldValues[i] = $('#txt_skfnsrsbh').val();
		i++;

		fieldNames[i] = "tjrqq";
		fieldValues[i] = $('#txt_tjrqq').val();
		i++;

		fieldNames[i] = "tjrqz";
		fieldValues[i] = $('#txt_tjrqz').val();
		i++;

		fieldNames[i] = "fpzt";
		fieldValues[i] = $('#lst_fpzt').val();
		i++;

		fieldNames[i] = "lpfs";
		fieldValues[i] = $('#lst_lpfs').val();
		i++;

		fieldNames[i] = "lsh";
		fieldValues[i] = $('#txt_lsh').val();
		i++;

		fieldNames[i] = "fpdm";
		fieldValues[i] = $('#txt_fpdm').val();
		i++;

		fieldNames[i] = "fphm";
		fieldValues[i] = $('#txt_fphm').val();
		i++;

		fieldNames[i] = "kprdm";
		fieldValues[i] = $('#lst_kprdm').val();
		i++;

		fieldNames[i] = "lyqd";
		fieldValues[i] = $('#lst_lyqd').val();
		i++;

		var requestXml = getFieldsXmlRequest(fieldNames, fieldValues);
		var data = requestXml.replace('<?xml version="1.0" encoding="gbk"?>', "");*/
		// 发送请求,获取满足条件的所有数据
		//foreseeFp.common.ajax(strUrl, data, false, callback);
		// 请求回调
		function callback(result) {
			if(result.code == '0'){
				var bodyArr = result.body;
				var count = bodyArr[0].totalCount;
				var gyxxList = bodyArr[0].items;
				var xmlStr = "<items>";
				xmlStr += "<querytype>" + $("#lst_fplx").val() + "</querytype>";
				for (var i = 0; i < gyxxList.length; i++) {
					var fpVo = gyxxList[i];
					xmlStr += "<item>";
					var tjrq = fpVo.tjrq==null?"":fpVo.tjrq;
					var xfnsrmc = fpVo.xhfnsrmc==null?"":fpVo.xhfnsrmc;
					var nsrsbh = fpVo.xhfnsrsbh==null?"":fpVo.xhfnsrsbh;
					var sphm = fpVo.sphm==null?"":fpVo.sphm;
					var fpdm = fpVo.fpDm==null?"":fpVo.fpDm;
					var fphm = fpVo.fphm==null?"":fpVo.fphm;
					var je = fpVo.je==null?"":fpVo.je;
					var se = fpVo.se==null?"":fpVo.se;
					var lpfsmc = "";
					if ($("#lst_fplx").val() == "zzsptfp" || $("#lst_fplx").val() == "zzsdzfp") {
						lpfsmc = fpVo.lpfsMc==null?"":fpVo.lpfsMc;
					} else if ($("#lst_fplx").val() == "zzszyfp") {
						lpfsmc = fpVo.lpfsmc==null?"":fpVo.lpfsmc;
					}
					var wdmc = fpVo.wdmc==null?"":fpVo.wdmc;
					var fpztmc = fpVo.fpztmc==null?"":fpVo.fpztmc;
					var czfwbz = fpVo.czfwbz==null?"":fpVo.czfwbz;
					var ddh = fpVo.ddh==null?"":fpVo.ddh;
					xmlStr += "<lsh>" + fpVo.xlh + "</lsh>";
					xmlStr += "<tjrq>" + tjrq + "</tjrq>";
					xmlStr += "<xfnsrmc>" + xfnsrmc + "</xfnsrmc>";
					xmlStr += "<nsrsbh>" + nsrsbh + "</nsrsbh>";
					xmlStr += "<sphm>" + sphm + "</sphm>";
					xmlStr += "<fpdm>" + fpdm + "</fpdm>";
					xmlStr += "<fphm>" + fphm + "</fphm>";
					xmlStr += "<je>" + je + "</je>";
					xmlStr += "<se>" + se + "</se>";
					xmlStr += "<lpfsmc>" + lpfsmc + "</lpfsmc>";
					xmlStr += "<wdmc>" + wdmc + "</wdmc>";
					xmlStr += "<fpztmc>" + fpztmc + "</fpztmc>";
					xmlStr += "<czfwbz>" + czfwbz + "</czfwbz>";
					xmlStr += "<ddh>" + ddh + "</ddh>";
					xmlStr += "</item>";
				}
				xmlStr += "</items>";
				$("#hid_export_data").val(xmlStr);
			}
			/*if($(result).find("replyCode").text() == "0"){
				var dataStr = xmlToString(result);
				var queryType = $("#lst_fplx").val();
				var newDataStr=dataStr.substring(0,dataStr.indexOf('<items>')+7)+'<querytype>'+queryType+'</querytype>'+dataStr.substring(dataStr.indexOf('<items>')+7,dataStr.length);
				$("#hid_export_data").val(newDataStr);
			}*/
		}

	},
	reloadTree : function(){
		foreseeFp.swjgTree.eventFunction.hideMenu('swjgTree');
		foreseeFp.swjgTree.treeFunction.swjgTreeSetting.async.dataFilter=foreseeFp.ggcx.filterQx;
		foreseeFp.swjgTree.treeFunction.initTree('swjgTree',function(treeNode){
			$("#lst_swjg").val(treeNode.id);
		});

	},
	filterQx:function(treeId, parentNode, responseData){
		var data = [];
		if (responseData) {
			for(var i =0; i < responseData.length; i++) {
				if("000000"==responseData[i].id.substring(5,12)){
					data.push(responseData[i]);
				}
			}
		}
		return data;
	}
};

