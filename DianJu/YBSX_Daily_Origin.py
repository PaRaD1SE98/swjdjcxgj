import datetime
import os

import xlsxwriter

from DianJu.YBSX_ALL import All
from DianJu.YBSX_Daily import InputParamsMixin, ReadSwryCsvMixin


class YbsxDailyOrigin(ReadSwryCsvMixin,
                      InputParamsMixin,
                      All):
    """
    已办事项_日结数据原始格式
    输出选择的每个税务人员的日结数据细节
    """
    version = '3.6.2'
    swj = '14401060011'

    def get_all_json(self):
        """
        输出选定税务人员的全部数据

        :return:
            [
                {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '主管税务所': data[j]['zgswskfj'],(可能出错)
                    '受理人': data[j]['blryMc'],
                    '受理人代码': data[j]['blryDm'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                    '任务编号': data[j]['rwbh'],
                    '税务事项代码': data[j]['swsxDm'],
                    '申请序号': data[j]['sqxh'],
                },
            ]
        :rtype: list[dict[str]]
        """
        print('开始获取数据')
        all_json = []
        member = self.read_swry_csv('swry')

        # 获取税务人员代码
        for swry in member:
            total_count = self.get_total_count(swry=swry[0])
            total_count_extra = self.get_total_count(swry=swry[0], extra_data=True)

            def get_total_page(count):
                PAGE_SIZE = 100
                if count % PAGE_SIZE == 0:
                    total = count // PAGE_SIZE
                else:
                    total = count // PAGE_SIZE + 1
                return total

            # 总页数（包括最后一页）
            total_page = get_total_page(total_count)

            # 上月末尾总页数（包括最后一页）
            total_page_extra = get_total_page(total_count_extra)

            print(f'{swry[1]}:')
            print(f'总数据量:{total_count + total_count_extra},总页数:{total_page + total_page_extra}')

            # 获取每一页的数据
            for i in range(total_page):
                print(f'正在获取第{i + 1}页的数据')
                data = self.grab_data(i, swry=swry[0])
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '受理人代码': data[j]['blryDm'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                        '任务编号': data[j]['rwbh'],
                        '税务事项代码': data[j]['swsxDm'],
                        '申请序号': data[j]['sqxh'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    all_json.append(sx_data)

            # 获取上月末尾每一页的数据
            for i in range(total_page_extra):
                print(f'正在获取第{i + 1}页的额外申请时间范围的数据')
                data = self.grab_data(i, swry=swry[0], extra_data=True)
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '受理人代码': data[j]['blryDm'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                        '任务编号': data[j]['rwbh'],
                        '税务事项代码': data[j]['swsxDm'],
                        '申请序号': data[j]['sqxh'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    all_json.append(sx_data)

        print('数据获取成功')
        return all_json

    @staticmethod
    def get_latest_data_time(all_json):
        """获取结果中的最新时间"""
        datetime_li = []
        for i in all_json:
            datetime_str = i['办结时间']
            datetime_obj = datetime.datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S')
            datetime_li.append(datetime_obj)
        latest_datetime = max(datetime_li).strftime('%Y%m%d%H%M%S')
        return latest_datetime

    def write_excel(self):
        """
        输出excel文件
        """
        if not os.path.exists('results'):
            os.mkdir('results')
        # 获取数据
        data = self.add_spyjxx(
            self.add_blyj(
                self.get_all_json()
            )
        )
        # 创建excel文件
        workbook = xlsxwriter.Workbook(
            f'results/已办事项日结细节_截至{self.get_latest_data_time(data)}.xlsx'
        )

        # 创建工作表
        swry = self.read_swry_csv('swry')
        for i in swry:
            sheet = workbook.add_worksheet(i[1])
            selected_data = []
            for sx in data:
                if i[0] == sx['受理人代码']:
                    selected_data.append(sx)

            # 录入表头
            self.insert_thead(sheet)
            # 录入数据
            self.insert_data(selected_data, sheet)

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main():
    ybsx_daily_origin = YbsxDailyOrigin(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1',
        query_blyj_api='ybsx_queryYbsxxq.do',
        query_spyj_api='ybsx_queryBlyj.do'
    )
    ybsx_daily_origin.write_excel()
