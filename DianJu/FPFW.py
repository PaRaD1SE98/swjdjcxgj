import base64
import datetime
import json
import os
from typing import Union, Dict, Tuple, List

import requests
import xlsxwriter

from DianJu.ShuiWuJu import ShiXiang
from DianJu.exceptions import InputError


class FPFW(ShiXiang):
    """
    发票服务查询
    """
    version = '3.8.11'

    def __init__(self,
                 sx_url: str,
                 query_host: str,
                 query_api: str,
                 query_gnmkId: str,
                 query_api_ptfp: str) -> None:
        super().__init__(sx_url, query_host, query_api, query_gnmkId)
        self.query_api_ptfp = query_api_ptfp

        input_params = self.get_input_params()
        self.SQSJSTARTDATE = input_params[0]
        self.SQSJENDDATE = input_params[1]
        self.SHSJSTARTDATE = input_params[2]
        self.SHSJENDDATE = input_params[3]

        self.query_page_size = 10

    @staticmethod
    def get_input_params() -> Tuple[str, str, datetime.datetime, datetime.datetime]:
        """
        获取输入的查询参数

        :return: tuple： 申请开始，申请结束，审核开始，审核结束
        """
        try:
            sqks = input('申请开始（格式：20220101）:')
            sqjz = input('申请截至（格式：20220101）:')
            shks = input('审核开始（格式：20220101235959）:')
            shjz = input('审核截至（格式：20220101235959）:')

            sqrqstartdate = datetime.datetime.strptime(
                sqks, "%Y%m%d").strftime("%Y-%m-%d")  # 申请日期开始
            sqrqenddate = datetime.datetime.strptime(
                sqjz, "%Y%m%d").strftime("%Y-%m-%d")  # 申请日期截至
            shrqstartdate = datetime.datetime.strptime(
                shks, "%Y%m%d%H%M%S")  # 审核日期开始
            shrqenddate = datetime.datetime.strptime(
                shjz, "%Y%m%d%H%M%S")  # 审核日期截至
            return sqrqstartdate, sqrqenddate, shrqstartdate, shrqenddate
        except ValueError:
            raise InputError

    @staticmethod
    def encode(data: dict) -> bytes:
        """
        :param data: 原始数据字典
        :return: 加密过的 byte string
        """
        d = json.dumps(data)
        return base64.b64encode(d.encode('utf8')).replace(b'M', b'-')

    @staticmethod
    def decode(data: str) -> str:
        """
        :param data: 加密数据字符串
        :return: 解密后的数据
        """
        return base64.b64decode(data.replace('-', 'M')).decode('utf8')

    def test_search(self) -> Union[str, list, dict]:
        test_param = {
            "action": "fpcx",
            "pageNo": "1",
            "pageSize": 10,
            "zfbz": "N",
            "dzfpbz": "N",
            "tjrqq": self.SQSJSTARTDATE,
            "tjrqz": self.SQSJENDDATE,
            "lpfs": "5",
            "nsrsbh": "",
            "fpzt": "11",
            "lsh": "",
            "fpdm": "",
            "fphm": "",
            "kprdm": "",
            "lyqd": "",
            "czfwbz": "N",
            "swjgggcx": ""
        }
        data = self.encode(test_param)
        search_data = requests.post(
            f'{self.query_host}{self.query_api}',
            headers=self.make_header(),
            data={'specialToken': data}
        )
        result = json.loads(self.decode(search_data.text))
        print(result)
        return result

    def get_search_params(self,
                          pageIndex: int = 0,
                          pageSize: int = None,
                          zy: bool = True) -> Dict[str, bytes]:
        """
        :param pageIndex: 页码索引，从0开始
        :param pageSize: 每页数据量
        :param zy: 是否专用发票
        :return: 构造好的字符串
        """
        params = {
            "action": "fpcx",
            "pageNo": f"{pageIndex + 1}",  # 从1开始，从0开始则一次返回全部数据
            "pageSize": pageSize if pageSize is not None else self.query_page_size,
            "zfbz": "N",  # ????
            "dzfpbz": "N",  # 电子发票??
            "tjrqq": self.SQSJSTARTDATE,  # 添加日期起
            "tjrqz": self.SQSJENDDATE,  # 添加日期止
            "lpfs": "5" if zy else "",  # 领票方式（5：自助终端受理，专票选5，普票全部）
            "nsrsbh": "",  # 纳税人识别号
            "fpzt": "10,11",  # 发票状态（10：审核已通过，11：审核不通过）
            "lsh": "",  # 流水号（申请单号）
            "fpdm": "",  # 发票代码
            "fphm": "",  # 发票号码
            "kprdm": "",  # 开票人代码
            "lyqd": "",  # 来源渠道
            "czfwbz": "N",  # 出租房屋标志
            "swjgggcx": ""  # 税务机关
        }
        return {'specialToken': self.encode(params)}

    def get_total_count(self, zyfp_api: bool = True) -> int:
        """
        获取总数据数量
        :param zyfp_api: 是否专用发票api，否为普通发票api
        :return: 结果总数
        """
        search_data = requests.post(
            f'{self.query_host}{self.query_api if zyfp_api else self.query_api_ptfp}',
            headers=self.make_header(),
            data=self.get_search_params(pageIndex=0, pageSize=1, zy=zyfp_api)
        )
        result = json.loads(self.decode(search_data.text))['body'][0]['totalCount']
        return int(result)

    def grab_data(self, page: int, zyfp_api: bool = True) -> List[Dict[str, Union[int, float, str]]]:
        """
        获取某一页的数据

        :param page: 页码索引（从0开始）
        :param zyfp_api: 是否专用发票api，否为普通发票api
        :return: json数据
        """
        search_data = requests.post(
            f'{self.query_host}{self.query_api if zyfp_api else self.query_api_ptfp}',
            headers=self.make_header(),
            data=self.get_search_params(pageIndex=page, zy=zyfp_api)
        )
        result = json.loads(self.decode(search_data.text))['body'][0]['items']
        return result

    def get_all_json(self, zy: bool = True) -> List[Dict[str, Union[str, float, Dict[str, str]]]]:
        """
        输出全部数据
        :param zy: 获取专用发票数据，否则获取普通发票数据
        :return:
            [
                {
                    'sprDm': 审批人代码,
                    'sprmc': 审批人名称,
                    'tjrq': 申请日期,
                    'sprq': 审批日期,
                    'dkfpztDm': 代开发票状态代码,
                    'fpztmc': 发票状态名称，
                    ...
                }
            ]
        """
        all_json = []
        total_count = self.get_total_count(zyfp_api=zy)

        def get_total_page(count: int) -> int:
            """
            计算总页数

            :param count: 数据总数
            """
            PAGE_SIZE = self.query_page_size
            if count % PAGE_SIZE == 0:
                total = count // PAGE_SIZE
            else:
                total = count // PAGE_SIZE + 1
            return total

        # 总页数（包括最后一页）
        total_page = get_total_page(total_count)
        print(f'服务器统计：总数据量:{total_count},总页数:{total_page}')

        # 获取并拼接每一页的数据
        for page_idx in range(total_page):
            data = self.grab_data(page_idx, zyfp_api=zy)
            for d in data:
                all_json.append(d)
        print('本地统计：源数据量', len(all_json))
        return all_json

    def process_all_json(self, raw_data) -> List[Dict[str, Union[str, List[Dict[str, str]]]]]:
        """
        整合成需要的数据格式
        :param raw_data: get_all_json返回的数据
        :return:
            [
                {
                    'sprDm': 审批人代码,
                    'sprmc': 审批人名称,
                    'data': [
                        {
                            'tjrq': 申请日期,
                            'sprq': 审批日期,
                            'dkfpztDm': 代开发票状态代码,
                            'fpztmc': 发票状态名称，
                        }
                    ],
                },
            ]
        """
        processed_data = []
        swry = self.read_swry_csv('FaPiaoDaiKai')
        for spr in swry:
            spr_all = {
                'sprDm': spr[0],
                'sprmc': spr[1],
                'data': []
            }
            for record in raw_data:
                if record['sprDm'] == spr[0]:
                    filtered_record = {
                        'tjrq': record['tjrq'],
                        'sprq': record['sprq'],
                        'dkfpztDm': record['dkfpztDm'],
                        'fpztmc': record['fpztmc'],
                    }
                    spr_all['data'].append(filtered_record)
            processed_data.append(spr_all)
            if len(spr_all['data']) != 0:
                print('审核人：', spr_all['sprmc'], '数据量：', len(spr_all['data']))
        return processed_data

    def shrq_filter(self, processed_data) -> List[Dict[str, Union[str, List[Dict[str, str]]]]]:
        """
        审核日期过滤
        :param processed_data: 输出的数据结构
        :return: 与输入的数据相同格式
            [
                {
                    'sprDm': 审批人代码,
                    'sprmc': 审批人名称,
                    'data': [
                        {
                            'tjrq': 申请日期,
                            'sprq': 审批日期,
                            'dkfpztDm': 代开发票状态代码,
                            'fpztmc': 发票状态名称，
                        }
                    ],
                },
            ]
        """
        filtered_data = []
        for spr_data in processed_data:
            filtered_spr_data = {
                'sprDm': spr_data['sprDm'],
                'sprmc': spr_data['sprmc'],
                'data': []
            }
            for data in spr_data['data']:
                sprq = datetime.datetime.strptime(data['sprq'], '%Y-%m-%d %H:%M:%S')
                if self.SHSJSTARTDATE <= sprq <= self.SHSJENDDATE:
                    filtered_spr_data['data'].append(data)
            filtered_data.append(filtered_spr_data)
            if len(filtered_spr_data['data']) != 0:
                print('审核日期过滤后：', '审核人：', filtered_spr_data['sprmc'], '数据量：', len(filtered_spr_data['data']))
        return filtered_data

    @staticmethod
    def get_stats(data: List[Dict[str, Union[str, List[Dict[str, str]]]]]) -> List[Dict[str, Union[str, int]]]:
        """
        统计数据
        :return:
        [
            {
                'sprmc': 审批人名称,
                'cleared': 审核已通过,
                'refused': 审核不通过，
            }
        ]
        """
        result = []
        for person in data:
            records = person['data']
            review_cleared = 0
            for record in records:
                if record['fpztmc'] == "审核已通过":
                    review_cleared += 1
            review_refused = len(records) - review_cleared
            stats = {
                'sprmc': person['sprmc'],
                'cleared': review_cleared,
                'refused': review_refused
            }
            result.append(stats)
            # print('统计后：', '审核人:', stats['sprmc'], '通过：', stats['cleared'], '不通过：', stats['refused'])
        return result

    @staticmethod
    def insert_thead(sheet: xlsxwriter.workbook.Worksheet) -> None:
        """
        录入表头，原始数据格式

        :param sheet: 工作表（tab）对象
        """
        # 录入基础数据表头
        sheet.write(0, 0, '姓名')
        sheet.write(0, 1, '专用审核已通过')
        sheet.write(0, 2, '专用审核不通过')
        sheet.write(0, 3, '普通审核已通过')
        sheet.write(0, 4, '普通审核不通过')

    @staticmethod
    def insert_data(data, sheet: xlsxwriter.workbook.Worksheet, zy: bool = True) -> None:
        """
        录入数据
        :param data: 数据
        :param sheet: 工作表（tab）对象
        :param zy: 是否专用
        """
        if zy:
            for index, i in enumerate(data):
                sheet.write(index + 1, 0, i['sprmc'])
                sheet.write(index + 1, 1, i['cleared'])
                sheet.write(index + 1, 2, i['refused'])
        else:
            for index, i in enumerate(data):
                sheet.write(index + 1, 3, i['cleared'])
                sheet.write(index + 1, 4, i['refused'])

    def write_excel(self) -> None:
        """
        输出excel文件
        """
        print('获取专用发票数据')
        data_zy = self.get_stats(self.shrq_filter(self.process_all_json(self.get_all_json())))
        print('获取普通发票数据')
        data_pt = self.get_stats(self.shrq_filter(self.process_all_json(self.get_all_json(zy=False))))
        # 创建excel文件
        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(
            f'results/代开发票_{self.SQSJSTARTDATE}-{self.SQSJENDDATE}.xlsx'
        )
        # 创建工作表
        sheet = workbook.add_worksheet('代开发票数据')

        # 录入表头
        self.insert_thead(sheet)
        # 录入数据
        self.insert_data(data_zy, sheet)
        self.insert_data(data_pt, sheet, zy=False)

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main() -> None:
    fpfw = FPFW(
        sx_url='http://fpyy.dzgzpt.gdgs.tax/dzgzpt-fpfw/doLogin?actionType=ppdk.ggcx',
        query_host='http://fpyy.dzgzpt.gdgs.tax/dzgzpt-fpfw/',
        query_api='swry/fpDkZzsZyfp/queryZyfpGyxx',
        query_api_ptfp='swry/dkfp/ggcx/getPtfpGyxx',
        query_gnmkId='49E7D25F1CC10194E053C0A8141844D0')
    fpfw.write_excel()
