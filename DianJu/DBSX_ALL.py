from DianJu.DBSX import DBSX


class All(DBSX):
    """
    待办事项
    输出全部数据
    """
    version = '3.4.3'


def main():
    dbsx = All(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/dbsx_tjxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='dbsx_queryDbsxTjxx.do',
        query_gnmkId='344882DA77640120E053C0A81418D7E6',
        query_sfdcsl_api='dbsx_queryByslCount.do'
    )
    dbsx.write_excel(name='全部数据',
                     data=dbsx.add_sfdcsl(
                         dbsx.time_filter(
                             dbsx.get_all_json()
                         )
                     ))
