class LoginError(Exception):
    def __str__(self):
        return '用户名或密码错误'


class AccountNotSetError(Exception):
    def __str__(self):
        return 'account.txt 文件中未检测到 USERNAME 和 PASSWORD 参数'


class AccountError(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f'账号配置格式错误，请仔细检查 {self.name}=xxx 格式'


class LoginConnectionError(Exception):
    def __str__(self):
        return '网络连接失败'


class InputError(Exception):
    def __str__(self):
        return '输入日期格式错误'


class DataObtainError(Exception):
    def __str__(self):
        return '获取数据时发生网络错误，请重试'
