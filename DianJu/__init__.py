"""初始化模块"""
from .DBSX import DBSX as dbsx
from .DBSX import main as DBSX
from .DBSX_ALL import All as dbsx_all
from .DBSX_ALL import main as DBSX_ALL
from .ShuiWuJu import main as account_setter
from .YBSX import YBSX as ybsx
from .YBSX import main as YBSX
from .YBSX_ALL import All as ybsx_all
from .YBSX_ALL import main as YBSX_ALL
from .YBSX_BYSL import BYSL as ybsx_bysl
from .YBSX_BYSL import main as YBSX_BYSL
from .YBSX_Daily import YBSXDaily as ybsx_daily
from .YBSX_Daily import main as YBSX_Daily
from .YBSX_Daily_Analysis import YbsxDailyAnalysis as ybsx_daily_analysis
from .YBSX_Daily_Analysis import main as YBSX_Daily_Analysis
from .YBSX_Daily_Origin import YbsxDailyOrigin as ybsx_daily_origin
from .YBSX_Daily_Origin import main as YBSX_Daily_Origin
from .YBSX_NSRSBH import Nsrsbh as ybsx_nsrsbh
from .YBSX_NSRSBH import main as YBSX_NSRSBH
from .YBSX_SWSXTJ import SwsxTj as ybsx_swsxtj
from .YBSX_SWSXTJ import main as YBSX_SWSXTJ
from .YBSX_SwsxDmGenerator import SwsxDmGenerator as ybsx_swsxdmgenerator
from .YBSX_SwsxDmGenerator import main as YBSX_SwsxDmGenerator
from .FPFW import FPFW as fpfw
from .FPFW import main as FPFW


def get_latest_version():
    # 版本号
    v_ybsx = ybsx.version
    v_ybsx_all = ybsx_all.version
    v_ybsx_bysl = ybsx_bysl.version
    v_ybsx_nsrsbh = ybsx_nsrsbh.version
    v_dbsx = dbsx.version
    v_dbsx_all = dbsx_all.version
    v_ybsx_swsxdmgenerator = ybsx_swsxdmgenerator.version
    v_ybsx_swsxtj = ybsx_swsxtj.version
    v_ybsx_daily = ybsx_daily.version
    v_ybsx_daily_origin = ybsx_daily_origin.version
    v_ybsx_daily_analysis = ybsx_daily_analysis.version
    v_fpfw = fpfw.version

    v_apps = (
        v_ybsx,
        v_ybsx_all,
        v_ybsx_bysl,
        v_ybsx_nsrsbh,
        v_dbsx,
        v_dbsx_all,
        v_ybsx_swsxdmgenerator,
        v_ybsx_swsxtj,
        v_ybsx_daily,
        v_ybsx_daily_origin,
        v_ybsx_daily_analysis,
        v_fpfw
    )

    v_max = max(v_apps)
    return v_max


__version__ = get_latest_version()
