import csv
import os

from .YBSX_ALL import All
from .YBSX_SWSXTJ import ReadShiXiangDaiMaCsvMixin


class SwsxDmGenerator(ReadShiXiangDaiMaCsvMixin,
                      All):
    """
    已办事项_当月事项代码获取工具
    原理：
        搜集一个月的所有事项的代码
    用法：
        1.如果当月有新增事项，在query_params/ShiXiang.csv中添加该事项名称，然后运行一次，输入当月
        2.如果当月的事项不完整，运行一次输入其他含有该事项的月份，程序会自动比对并补上新增的事项代码
    """
    version = '3.4.0'

    def __init__(self,
                 sx_url,
                 query_host,
                 query_api,
                 query_gnmkId,
                 second_query_api='',
                 query_spyj_api=''):
        super().__init__(sx_url,
                         query_host,
                         query_api,
                         query_gnmkId,
                         second_query_api,
                         query_spyj_api)

    def write_swsxdm(self, all_json):
        print('开始写入事项代码')
        dm_mc = []
        for sx in all_json:
            if [sx['税务事项代码'], sx['事项名称']] not in dm_mc:
                dm_mc.append([sx['税务事项代码'], sx['事项名称']])

        # 读取旧事项，过滤添加新事项
        try:
            old_dm_mc = self.read_sxdm_csv('ShiXiangDaiMa')
            new_dm_mc = []
            for dmmc in dm_mc:
                if dmmc not in old_dm_mc:
                    new_dm_mc.append(dmmc)
        except FileNotFoundError:
            new_dm_mc = dm_mc

        if not os.path.exists('query_params'):
            os.mkdir('query_params')

        with open('query_params/ShiXiangDaiMa.csv', 'a', encoding='utf-8', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(new_dm_mc)

        print('税务事项代码写入成功，添加了下列事项代码')
        for sx in new_dm_mc:
            print(f'{sx[0]}，{sx[1]}')


def main():
    generator = SwsxDmGenerator(sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
                                query_host='http://app2.dzgzpt.gdgs.tax/',
                                query_api='ybsx_queryYbsx.do',
                                query_gnmkId='352B0F8EF3280008E053C0A81418DFD1')
    generator.write_swsxdm(
        generator.get_all_json()
    )
