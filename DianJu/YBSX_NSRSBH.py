import json
import os
import traceback

import requests
import xlsxwriter

from DianJu.ShuiWuJu import ShiXiang
from DianJu.YBSX_ALL import ExcelFormatMixin, SpyjMixin, BlyjMixin
from DianJu.YBSX_BYSL import InputParamsMixin, SearchParamsMixin
from DianJu.exceptions import DataObtainError


class Nsrsbh(InputParamsMixin,
             SearchParamsMixin,
             BlyjMixin,
             SpyjMixin,
             ExcelFormatMixin,
             ShiXiang):
    """
    已办事项_纳税人识别号查询
    输入纳税人识别号和时间段，查询该识别号的所有已办事项
    添加了办理意见和审批意见
    """
    version = '3.4.0'

    def __init__(self,
                 sx_url,
                 query_host,
                 query_api,
                 query_gnmkId,
                 query_blyj_api,
                 query_spyj_api):
        super().__init__(sx_url,
                         query_host,
                         query_api,
                         query_gnmkId)

        input_params = self.get_input_params()
        self.SQSJSTARTDATE = input_params[0]
        self.SQSJENDDATE = input_params[1]
        self.BLRQSTARTDATE = input_params[2]
        self.BLRQENDDATE = input_params[3]
        self.EXTRA_SQSJSTARTDATE = input_params[4]
        self.EXTRA_SQSJENDDATE = input_params[5]
        self.NSRSBH = input_params[6]
        self.query_blyj_api = query_blyj_api
        self.query_spyj_api = query_spyj_api

    @staticmethod
    def get_input_params():
        nsrsbh = input('纳税人识别号:')

        (sqsjstartdate,
         sqsjenddate,
         blrqstartdate,
         blrqenddate,
         extra_sqsjstartDate,
         extra_sqsjendDate) = super(Nsrsbh, Nsrsbh).get_input_params()

        return (sqsjstartdate,
                sqsjenddate,
                blrqstartdate,
                blrqenddate,
                extra_sqsjstartDate,
                extra_sqsjendDate,
                nsrsbh)

    def get_total_count(self, nsrsbh, extra_data=False):
        """
        获取总数据数量

        :param str nsrsbh: 纳税人识别号
        :param bool extra_data: 是否查询多余的上月数据
        :return: 结果总数
        :rtype: int
        """
        try:
            search_data = requests.post(
                self.addr,
                headers=self.make_header(),
                data=self.get_search_params(nsrsbh=nsrsbh,
                                            extra_data=extra_data,
                                            pageIndex=0,
                                            pageSize=1,
                                            blztDm='',
                                            swryszswjg='14401060000')
            )
            result = search_data.json()['total']
            return int(result)
        except json.decoder.JSONDecodeError:
            raise DataObtainError

    def grab_data(self, page, nsrsbh, extra_data=False):
        """
        获取某一页的数据

        :param nsrsbh: 纳税人识别号
        :param bool extra_data: 是否查询多余的上月数据
        :param int page: 页码索引（从0开始）
        :return: result: json数据
        """
        try:
            search_data = requests.post(
                self.addr,
                headers=self.make_header(),
                data=self.get_search_params(nsrsbh=nsrsbh,
                                            extra_data=extra_data,
                                            pageIndex=page,
                                            blztDm='',
                                            swryszswjg='14401060000')
            )
            result = search_data.json()['data']
            return result
        except json.decoder.JSONDecodeError:
            raise DataObtainError

    def get_all_json(self, nsrsbh):
        """
        输出一个纳税人识别号的全部记录

        :return:
            [
                {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '主管税务所': data[j]['zgswskfj'],(可能出错)
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                    '任务编号': data[j]['rwbh'],
                    '税务事项代码': data[j]['swsxDm'],
                    '申请序号': data[j]['sqxh'],
                },
            ]
        """
        all_json = []
        total_count = self.get_total_count(nsrsbh)
        total_count_extra = self.get_total_count(nsrsbh, extra_data=True)

        def get_total_page(count):
            PAGE_SIZE = 100
            if count % PAGE_SIZE == 0:
                total = count // PAGE_SIZE
            else:
                total = count // PAGE_SIZE + 1
            return total

        # 总页数（包括最后一页）
        total_page = get_total_page(total_count)

        # 上月末尾总页数（包括最后一页）
        total_page_extra = get_total_page(total_count_extra)

        print(f'{nsrsbh}:')
        print(f'总数据量:{total_count},总页数:{total_page}')
        print(f'上月末总数据量:{total_count_extra},总页数{total_page_extra}')

        # 获取每一页的数据
        for i in range(total_page):
            data = self.grab_data(i, nsrsbh)
            # 获取一页中的每条数据
            for j in range(len(data)):
                sx_data = {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                    '任务编号': data[j]['rwbh'],
                    '税务事项代码': data[j]['swsxDm'],
                    '申请序号': data[j]['sqxh'],
                }
                try:
                    sx_data['主管税务所'] = data[j]['zgswskfj']
                except KeyError:
                    sx_data['主管税务所'] = ''
                    print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                all_json.append(sx_data)

        # 获取上月末尾每一页的数据
        for i in range(total_page_extra):
            data = self.grab_data(i, nsrsbh, extra_data=True)
            # 获取一页中的每条数据
            for j in range(len(data)):
                sx_data = {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                    '任务编号': data[j]['rwbh'],
                    '税务事项代码': data[j]['swsxDm'],
                    '申请序号': data[j]['sqxh'],
                }
                try:
                    sx_data['主管税务所'] = data[j]['zgswskfj']
                except KeyError:
                    sx_data['主管税务所'] = ''
                    print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                all_json.append(sx_data)

        print('数据获取成功')
        return all_json

    def add_blyj(self, all_json):
        """
        向获得的初始数据中添加办理意见数据

        :param list[dict[str]] all_json: 第一次查询得到的json
        :return
            [
                {
                    ...
                    '办理意见': 'blyj',
                },
            ]
        :rtype: list[dict[str]]
        """
        print('正在添加办理意见')

        result = all_json
        for index_sx, sx in enumerate(result):
            rwbh = sx['任务编号']
            blyj = self.grab_blyj(rwbh)
            result[index_sx]['办理意见'] = blyj

        print('添加成功')
        return result

    def write_excel(self):
        """
        输出excel文件
        """
        # 创建excel文件
        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(
            f'results/纳税人识别号_{self.NSRSBH}.xlsx')

        data = self.add_spyjxx(
            self.add_blyj(
                self.get_all_json(self.NSRSBH)
            )
        )

        # 创建工作表
        sheet = workbook.add_worksheet('纳税人识别号统计')

        # 录入表头
        self.insert_thead(sheet)
        # 录入数据
        self.insert_data(data, sheet)

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main():
    nsrsbh = Nsrsbh(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1',
        query_blyj_api='ybsx_queryYbsxxq.do',
        query_spyj_api='ybsx_queryBlyj.do'
    )
    nsrsbh.write_excel()


if __name__ == '__main__':
    print('\033[32m已办事项_纳税人识别号搜索\033[0m')
    try:
        main()
    except Exception:
        print(traceback.format_exc())
    finally:
        input('回车键退出')
