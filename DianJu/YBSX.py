import datetime
import json
import os
import traceback

import requests
import xlsxwriter

from DianJu.ShuiWuJu import ShiXiang, ReadShiXiangCsvMixin
from DianJu.exceptions import DataObtainError, InputError


class YBSX(ReadShiXiangCsvMixin, ShiXiang):
    """
    已办事项统计
    统计事项的总受理数和退件数
    """
    version = '3.4.0'

    def __init__(self,
                 sx_url,
                 query_host,
                 query_api,
                 query_gnmkId):
        super().__init__(sx_url,
                         query_host,
                         query_api,
                         query_gnmkId)

        input_params = self.get_input_params()
        self.SQSJSTARTDATE = input_params[0]
        self.SQSJENDDATE = input_params[1]
        self.BLRQSTARTDATE = input_params[2]
        self.BLRQENDDATE = input_params[3]
        self.EXTRA_SQSJSTARTDATE = input_params[4]
        self.EXTRA_SQSJENDDATE = input_params[5]

    @staticmethod
    def get_input_params():
        """
        获取输入的查询参数

        :return: tuple： 申请开始，申请结束，办理开始，办理结束，上月末申请开始，上月末申请结束
        :rtype: tuple[str]
        """
        try:
            blny = input('办理年月（格式：202101）:')

            def last_day_of_month(any_day):
                """
                获取获得一个月中的最后一天
                :param any_day: 任意日期
                :return: 日期
                :rtype: datetime.date
                """
                next_month = any_day.replace(
                    day=28) + datetime.timedelta(days=4)  # this will never fail
                return next_month - datetime.timedelta(days=next_month.day)

            blrqstartdate = datetime.datetime.strptime(
                blny + "01", "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期开始
            blrqenddate = last_day_of_month(datetime.datetime.strptime(blny + '01', "%Y%m%d")).strftime(
                "%Y-%m-%d")  # 办理日期截至
            sqsjstartdate = blrqstartdate
            sqsjenddate = blrqenddate

            extra_sqsjstartDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-8)
            ).strftime('%Y-%m-%d')
            extra_sqsjendDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-1)
            ).strftime('%Y-%m-%d')

            print(
                f'申请时间开始{sqsjstartdate}\n'
                f'申请时间截至{sqsjenddate}\n'
                f'办理时间开始{blrqstartdate}\n'
                f'办理时间截至{blrqenddate}\n'
                f'上月末尾申请时间开始{extra_sqsjstartDate}\n'
                f'上月末尾申请时间截至{extra_sqsjendDate}'
            )
            return sqsjstartdate, sqsjenddate, blrqstartdate, blrqenddate, extra_sqsjstartDate, extra_sqsjendDate
        except ValueError:
            raise InputError

    def get_search_params(self,
                          pageIndex,
                          nsrsbh='',
                          swsxDm='',
                          blztDm='',
                          swryszswjg='14401060011',
                          swry='',
                          sfyqbl='',
                          jrqd='',
                          pageSize=100,
                          sortField='',
                          sortOrder='',
                          extra_data=False):
        """
        获取搜索参数

        :param int pageIndex: 页码
        :param str nsrsbh: 纳税人识别号
        :param str swsxDm: 税务事项代码
            票种核定: SXE080100000
            注销登记: SLSXA011005001
            变更登记: SXE010800000
            增值税一般纳税人登记: SXE010500000
            定期定额户核定定额: SXA071001002
            增值税专用发票最高开票限额申请: SXE080200000
        :param str blztDm: 办理状态代码 不予受理: 02 已受理： 12
        :param str swryszswjg: 税务人员所在税务机关代码
        :param str swry: 税务人员
        :param sfyqbl: 是否逾期办理， 类型未知
        :param int pageSize: 每页返回数量，小于等于100
        :param jrqd: 未知参数
        :param sortField: 筛选栏目
        :param sortOrder: 排列顺序
        :param bool extra_data: 是否查询上月申请本月办理的数据
        :return: 格式化后的查询参数
        :rtype: str
        """
        data = f'nsrsbh={nsrsbh}' + \
               f'&swsxDm={swsxDm}' + \
               f'&blztDm={blztDm}' + \
               f'&swryszswjg={swryszswjg}' + \
               f'&swry={swry}' + \
               f'&sfyqbl={sfyqbl}' + \
               f'&sqsjstartDate={self.SQSJSTARTDATE if not extra_data else self.EXTRA_SQSJSTARTDATE}' + \
               f'&sqsjendDate={self.SQSJENDDATE if not extra_data else self.EXTRA_SQSJENDDATE}' + \
               f'&blrqstartDate={self.BLRQSTARTDATE}' + \
               f'&blrqendDate={self.BLRQENDDATE}' + \
               f'&jrqd={jrqd}' + \
               f'&pageIndex={pageIndex}' + \
               f'&pageSize={pageSize}' + \
               f'&sortField={sortField}' + \
               f'&sortOrder={sortOrder}'
        return data

    def get_total_count(self, swry='', extra_data=False):
        """
        获取总数据数量

        :param bool extra_data: 是否查询多余的上月数据
        :param str swry: 税务人员代码
        :return: 结果总数
        :rtype: int
        """
        try:
            search_data = requests.post(
                self.addr,
                headers=self.make_header(),
                data=self.get_search_params(
                    swry=swry, extra_data=extra_data, pageIndex=0, pageSize=1)
            )
            result = search_data.json()['total']
            return int(result)
        except json.decoder.JSONDecodeError:
            raise DataObtainError

    def grab_data(self, page, swry='', extra_data=False):
        """
        获取某一页的数据

        :param bool extra_data: 是否查询多余的上月数据
        :param int page: 页码索引（从0开始）
        :param str swry: 税务人员代码
        :return: json数据
        :rtype: list[dict[str]]
        """
        try:
            search_data = requests.post(
                self.addr,
                headers=self.make_header(),
                data=self.get_search_params(
                    swry=swry, extra_data=extra_data, pageIndex=page)
            )
            result = search_data.json()['data']
            return result
        except json.decoder.JSONDecodeError:
            raise DataObtainError

    def get_all_json(self, csv_filename):
        """
        输出一个包含全部内容的json

        :param csv_filename: 税务人员列表csv
        :return: json format = [
            {
                '税务人员': '',
                '事项': [
                    {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '主管税务所': data[j]['zgswskfj'],(可能出错)
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                    },
                ]
            },
        ]
        :rtype: list[dict[str or list[dict[str]]]]
        """
        print('开始获取数据')
        all_json = []
        member = self.read_swry_csv(csv_filename)

        # 获取税务人员代码
        for swry in member:
            total_count = self.get_total_count(swry=swry[0])
            total_count_extra = self.get_total_count(
                swry=swry[0], extra_data=True)

            def get_total_page(count):
                PAGE_SIZE = 100
                if count % PAGE_SIZE == 0:
                    total = count // PAGE_SIZE
                else:
                    total = count // PAGE_SIZE + 1
                return total

            # 总页数（包括最后一页）
            total_page = get_total_page(total_count)

            # 上月末尾总页数（包括最后一页）
            total_page_extra = get_total_page(total_count_extra)

            print(f'{swry[1]}:')
            print(f'本月总数据量:{total_count},总页数:{total_page}')
            print(f'上月末总数据量:{total_count_extra},总页数{total_page_extra}')

            # 构建每个税务人员的数据
            total_data = []
            # 获取每一页的数据
            for i in range(total_page):
                data = self.grab_data(i, swry=swry[0])
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    total_data.append(sx_data)
            # 获取上月末尾每一页的数据
            for i in range(total_page_extra):
                data = self.grab_data(i, swry=swry[0], extra_data=True)
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    total_data.append(sx_data)

            # 将每个人的数据加入all_json
            swry_data = {
                '税务人员': swry,
                '事项': total_data,
            }
            all_json.append(swry_data)

        print('数据获取成功')
        return all_json

    @staticmethod
    def process_all_json(all_json):
        """
        输出用于生成统计表的json

        :param list[dict[str or list[dict[str]]]] all_json: 包含所有数据的json
        :return: json 用于生成excel的处理好的json format = [
                {
                    '姓名': '',
                    '事项': [
                        {
                            '事项名称': 'sxmc',
                            '总量': 'len(selected_total_list)',
                            '退件数量': 'len(selected_rejected_list)',
                        },
                    ]
                },
            ]
        :rtype: list[dict[str or list[dict[str]]]]
        """
        print('开始处理数据')
        data = []
        # 处理每个人的数据
        for i in all_json:
            print('----------------------------------------')
            print(f"税务人员：{i['税务人员'][1]}")
            person = {
                '姓名': i['税务人员'][1],
                '事项': [],
            }
            # 处理每个事项
            # 获取事项名称列表并判断是否退件
            sx_list = []
            for sx in i['事项']:
                is_rejected = False
                if sx['办理状态'] == '不予受理':
                    is_rejected = True
                sx_list.append((sx['事项名称'], is_rejected))

            # 去重列表事项名称
            sx_list_sorted = []
            for sx in sx_list:
                if sx[0] not in sx_list_sorted:
                    sx_list_sorted.append(sx[0])

            # 统计每个事项总量及其退件数量
            for sxmc in sx_list_sorted:
                selected_total_list = [sx for sx in sx_list if sx[0] == sxmc]
                selected_rejected_list = [
                    sx for sx in selected_total_list if sx[1]]
                print(sxmc, len(selected_total_list))
                print('退件数量', len(selected_rejected_list))
                sx = {
                    '事项名称': sxmc,
                    '总量': len(selected_total_list),
                    '退件数量': len(selected_rejected_list),
                }
                person['事项'].append(sx)
            print('----------------------------------------')
            data.append(person)
        print('数据处理成功')
        return data

    def insert_thead(self, sheet):
        """
        录入表头

        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        """
        sheet.write(0, 0, '事项名称/受理人')
        sheet.write(0, len(self.read_sx_csv('ShiXiang')) * 2 + 1, '总受理量（人）')
        sheet.write(0, len(self.read_sx_csv('ShiXiang')) * 2 + 2, '总退件量（每人）')

        for j, i in enumerate(self.read_sx_csv('ShiXiang')):
            sheet.write(0, j * 2 + 1, i)
            sheet.write(0, j * 2 + 2, '退件数量')

        print('表头录入成功')

    def insert_data(self, data, sheet):
        """
        录入数据

        :param list data: 数据
        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        """

        # 每人
        for j, i in enumerate(data):
            # 统计总受理量和总退件量
            total = 0
            total_rejected = 0

            sheet.write(j + 1, 0, i['姓名'])

            # 每个事项
            for v, k in enumerate(self.read_sx_csv('ShiXiang')):
                # 先录入0
                # if keep0:
                sheet.write(j + 1, v * 2 + 1, 0)
                sheet.write(j + 1, v * 2 + 2, 0)
                # 比对事项名称并统计和录入符合的
                for sx in i['事项']:
                    if sx['事项名称'] == k:
                        total += sx['总量']
                        total_rejected += sx['退件数量']
                        sheet.write(j + 1, v * 2 + 1, sx['总量'])
                        sheet.write(j + 1, v * 2 + 2, sx['退件数量'])

            sheet.write(j + 1, len(self.read_sx_csv('ShiXiang')) * 2 + 1, total)
            sheet.write(j + 1, len(self.read_sx_csv('ShiXiang')) * 2 + 2, total_rejected)
        print('数据录入成功')

    def write_excel(self, csv_filename):
        """
        输出excel文件

        :param csv_filename: 税务人员列表csv
        """
        # 创建excel文件
        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(
            f'results/{csv_filename}_{self.BLRQSTARTDATE + "-" + self.BLRQENDDATE}.xlsx')
        sheet = workbook.add_worksheet('原始数据')

        # 录入表头
        self.insert_thead(sheet)

        # 录入数据
        self.insert_data(self.process_all_json(self.get_all_json(csv_filename)), sheet)

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main():
    filenames = os.listdir('swry')
    for index in range(len(filenames)):
        filenames[index] = filenames[index].replace('.csv', '')

    ybsx = YBSX(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1'
    )
    for file in filenames:
        ybsx.write_excel(file)


if __name__ == '__main__':
    print('\033[32m已办事项\033[0m')
    try:
        main()
    except Exception:
        print(traceback.format_exc())
    finally:
        input('回车键退出')
