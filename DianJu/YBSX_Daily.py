import csv
import datetime
import os

import xlsxwriter

from DianJu.YBSX import YBSX
from DianJu.exceptions import InputError


class ReadSwryCsvMixin:
    @staticmethod
    def read_swry_csv(csv_filename):
        """
        读取daily_check文件夹的csv文件，输出税务人员代码和名字

        :param str csv_filename: csv文件名不带扩展名
        :return: list member [[税务人员代码， 姓名]]
        :rtype: list[list[str, str]]
        """
        with open(f'daily_check/{csv_filename}.csv', 'r', encoding='utf-8') as f:
            reader = csv.reader(f, delimiter='\t')
            member = []
            for i in reader:
                member.append(i)
        return member


class InputParamsMixin:
    @staticmethod
    def get_input_params():
        """
        获取输入的查询参数

        :return: tuple： 申请开始，申请结束，办理开始，办理结束，上月末申请开始，上月末申请结束
        :rtype: tuple[str]
        """
        try:
            blnyr = input('办理时间（格式:20210101）:')
            sqsjFrom = input('申请时间从（格式:20210101）:')

            def last_day_of_month(any_day):
                """
                获取获得一个月中的最后一天
                :param any_day: 任意日期
                :return: 日期
                :rtype: datetime.date
                """
                next_month = any_day.replace(
                    day=28) + datetime.timedelta(days=4)  # this will never fail
                return next_month - datetime.timedelta(days=next_month.day)

            blrqstartdate = datetime.datetime.strptime(
                blnyr, "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期开始
            blrqenddate = blrqstartdate  # 办理日期截至
            sqsjstartdate = blrqstartdate
            sqsjenddate = blrqenddate

            extra_sqsjstartDate = datetime.datetime.strptime(sqsjFrom, "%Y%m%d").strftime("%Y-%m-%d")
            extra_sqsjendDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-1)
            ).strftime('%Y-%m-%d')

            print(
                f'申请时间开始{sqsjstartdate}\n'
                f'申请时间截至{sqsjenddate}\n'
                f'办理时间开始{blrqstartdate}\n'
                f'办理时间截至{blrqenddate}\n'
                f'额外申请时间开始{extra_sqsjstartDate}\n'
                f'额外申请时间截至{extra_sqsjendDate}'
            )
            return (sqsjstartdate,
                    sqsjenddate,
                    blrqstartdate,
                    blrqenddate,
                    extra_sqsjstartDate,
                    extra_sqsjendDate,
                    blnyr)
        except ValueError:
            raise InputError


class YBSXDaily(InputParamsMixin,
                ReadSwryCsvMixin,
                YBSX):
    """
    已办事项_受理量退件量日结数据
    将查询的人员编号和姓名填入daily_check/swry.csv文件中
    输入办理日期和申请日期，查询在申请日期到办理日期范围内申请，办理日期当天办理的事项
    """
    version = '3.5.0'

    def get_all_json(self, csv_filename):
        """
        输出一个包含全部内容的json

        :param csv_filename: 税务人员列表csv
        :return: json format = [
            {
                '税务人员': '',
                '事项': [
                    {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '主管税务所': data[j]['zgswskfj'],(可能出错)
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                    },
                ]
            },
        ]
        :rtype: list[dict[str or list[dict[str]]]]
        """
        print('开始获取数据')
        all_json = []
        member = self.read_swry_csv(csv_filename)

        # 获取税务人员代码
        for swry in member:
            total_count = self.get_total_count(swry=swry[0])
            total_count_extra = self.get_total_count(
                swry=swry[0], extra_data=True)

            def get_total_page(count):
                PAGE_SIZE = 100
                if count % PAGE_SIZE == 0:
                    total = count // PAGE_SIZE
                else:
                    total = count // PAGE_SIZE + 1
                return total

            # 总页数（包括最后一页）
            total_page = get_total_page(total_count)

            # 上月末尾总页数（包括最后一页）
            total_page_extra = get_total_page(total_count_extra)

            print(f'{swry[1]}:')
            print(f'本月总数据量:{total_count},总页数:{total_page}')
            print(f'上月末总数据量:{total_count_extra},总页数{total_page_extra}')

            # 构建每个税务人员的数据
            total_data = []
            # 获取每一页的数据
            for i in range(total_page):
                data = self.grab_data(i, swry=swry[0])
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    total_data.append(sx_data)
            # 获取上月末尾每一页的数据
            for i in range(total_page_extra):
                data = self.grab_data(i, swry=swry[0], extra_data=True)
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    total_data.append(sx_data)

            # 将每个人的数据加入all_json
            swry_data = {
                '税务人员': swry,
                '事项': total_data,
            }
            all_json.append(swry_data)

        print('数据获取成功')
        return all_json

    def write_excel(self, csv_filename):
        """
        输出excel文件

        :param csv_filename: 税务人员列表csv
        """
        # 创建excel文件
        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(
            f'results/已办事项日结_{csv_filename}_{self.BLRQSTARTDATE}.xlsx')
        sheet = workbook.add_worksheet('原始数据')

        # 录入表头
        self.insert_thead(sheet)

        # 录入数据
        self.insert_data(self.process_all_json(self.get_all_json(csv_filename)), sheet)

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main():
    filenames = os.listdir('daily_check')
    for index in range(len(filenames)):
        filenames[index] = filenames[index].replace('.csv', '')

    ybsx = YBSXDaily(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1'
    )
    for file in filenames:
        ybsx.write_excel(file)
