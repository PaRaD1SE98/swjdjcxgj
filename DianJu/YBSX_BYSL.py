import datetime
import os
import traceback

import requests
import xlsxwriter

from DianJu.YBSX import YBSX
from DianJu.exceptions import InputError


class InputParamsMixin:
    @staticmethod
    def get_input_params():
        """
        获取输入的查询参数

        :return: tuple： 申请开始，申请结束，办理开始，办理结束，上月末申请开始，上月末申请结束
        """
        try:
            blks = input('办理开始（格式：20210101）:')
            bljz = input('办理截至（格式：20210101）:')

            blrqstartdate = datetime.datetime.strptime(
                blks, "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期开始
            blrqenddate = datetime.datetime.strptime(
                bljz, "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期截至

            sqsjstartdate = blrqstartdate
            sqsjenddate = blrqenddate

            extra_sqsjstartDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-7)).strftime(
                '%Y-%m-%d')
            extra_sqsjendDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-1)).strftime(
                '%Y-%m-%d')

            return sqsjstartdate, sqsjenddate, blrqstartdate, blrqenddate, extra_sqsjstartDate, extra_sqsjendDate
        except ValueError:
            raise InputError


class SearchParamsMixin:
    def get_search_params(self,
                          extra_data,
                          pageIndex,
                          swry='',
                          pageSize=100,
                          nsrsbh='',
                          swsxDm='',
                          blztDm='02',
                          swryszswjg='14401060011'):
        """
        获取搜索参数

        :param int pageIndex: 页码
        :param int pageSize: 每页返回数量
        :param nsrsbh: 纳税人识别号
        :param swsxDm: 税务事项代码
        :param blztDm: 办理状态代码 不予受理: 02 已受理： 12
        :param str swry: 税务人员
        :param bool extra_data: 是否查询多余的上月数据
        :param swryszswjg: 税务人员所在税务机关, 默认1所
        :return: 格式化后的查询参数
        """
        """
        swsxDm(税务事项代码)

        票种核定: SXE080100000
        注销登记: SLSXA011005001
        变更登记: SXE010800000
        增值税一般纳税人登记: SXE010500000
        定期定额户核定定额: SXA071001002
        增值税专用发票最高开票限额申请: SXE080200000
        """

        sfyqbl = ''
        jrqd = ''
        sortField = ''
        sortOrder = ''

        data = f'nsrsbh={nsrsbh}' + \
               f'&swsxDm={swsxDm}' + \
               f'&blztDm={blztDm}' + \
               f'&swryszswjg={swryszswjg}' + \
               f'&swry={swry}' + \
               f'&sfyqbl={sfyqbl}' + \
               f'&sqsjstartDate={self.SQSJSTARTDATE if not extra_data else self.EXTRA_SQSJSTARTDATE}' + \
               f'&sqsjendDate={self.SQSJENDDATE if not extra_data else self.EXTRA_SQSJENDDATE}' + \
               f'&blrqstartDate={self.BLRQSTARTDATE}' + \
               f'&blrqendDate={self.BLRQENDDATE}' + \
               f'&jrqd={jrqd}' + \
               f'&pageIndex={pageIndex}' + \
               f'&pageSize={pageSize}' + \
               f'&sortField={sortField}' + \
               f'&sortOrder={sortOrder}'
        return data


class GetAllJsonMixin:
    def get_all_json(self, csv_filename):
        """
        输出一个包含全部内容的json，
        重写YBSX的方法，添加任务编号字段

        :param csv_filename: 税务人员列表csv
        :return:
            [
                {
                    '税务人员': '',
                    '事项': [
                        {
                            '事项名称': data[j]['swsxMc'],
                            '纳税人识别号': data[j]['sqr'],
                            '纳税人名称': data[j]['nsrmc'],
                            '主管税务所': data[j]['zgswskfj'],(可能出错)
                            '受理人': data[j]['blryMc'],
                            '申请日期': data[j]['cjsj'],
                            '受理日期': data[j]['blsj'],
                            '办理期限': data[j]['blqx'],
                            '办结时间': data[j]['bjsj'],
                            '办理状态': data[j]['blztMc'],
                            '实名信息-姓名': data[j]['smxxXm'],
                            '是否逾期办理': data[j]['sfyqblMc'],
                            '来源渠道': data[j]['jrqdmc'],
                            '任务编号': data[j]['rwbh'],
                            '税务事项代码': data[j]['swsxDm'],
                        },
                    ]
                },
            ]
        """
        print('开始获取不予受理基础数据')

        all_json = []
        member = self.read_swry_csv(csv_filename)

        # 获取税务人员代码
        for swry in member:
            total_count = self.get_total_count(swry=swry[0])
            total_count_extra = self.get_total_count(swry=swry[0], extra_data=True)

            def get_total_page(count):
                PAGE_SIZE = 100
                if count % PAGE_SIZE == 0:
                    total = count // PAGE_SIZE
                else:
                    total = count // PAGE_SIZE + 1
                return total

            # 总页数（包括最后一页）
            total_page = get_total_page(total_count)

            # 上月末尾总页数（包括最后一页）
            total_page_extra = get_total_page(total_count_extra)

            print(f'{swry[1]}:')
            print(f'总数据量:{total_count},总页数:{total_page}')
            print(f'上月末总数据量:{total_count_extra},总页数{total_page_extra}')

            # 构建每个税务人员的数据
            total_data = []
            # 获取每一页的数据
            for i in range(total_page):
                data = self.grab_data(i, swry=swry[0])
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                        '任务编号': data[j]['rwbh'],
                        '税务事项代码': data[j]['swsxDm'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    total_data.append(sx_data)
            # 获取上月末尾每一页的数据
            for i in range(total_page_extra):
                data = self.grab_data(i, swry=swry[0], extra_data=True)
                # 获取一页中的每条数据
                for j in range(len(data)):
                    sx_data = {
                        '事项名称': data[j]['swsxMc'],
                        '纳税人识别号': data[j]['sqr'],
                        '纳税人名称': data[j]['nsrmc'],
                        '受理人': data[j]['blryMc'],
                        '申请日期': data[j]['cjsj'],
                        '受理日期': data[j]['blsj'],
                        '办理期限': data[j]['blqx'],
                        '办结时间': data[j]['bjsj'],
                        '办理状态': data[j]['blztMc'],
                        '实名信息-姓名': data[j]['smxxXm'],
                        '是否逾期办理': data[j]['sfyqblMc'],
                        '来源渠道': data[j]['jrqdmc'],
                        '任务编号': data[j]['rwbh'],
                        '税务事项代码': data[j]['swsxDm'],
                    }
                    try:
                        sx_data['主管税务所'] = data[j]['zgswskfj']
                    except KeyError:
                        sx_data['主管税务所'] = ''
                        print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                    total_data.append(sx_data)

            # 将每个人的数据加入all_json
            swry_data = {
                '税务人员': swry,
                '事项': total_data,
            }
            all_json.append(swry_data)

        print('数据获取成功')
        return all_json


class ZxztMixin:
    def grab_zxzt(self, nsrsbh, swsxDm):
        """
        根据纳税人识别号和税务事项代码组合查询单个事项的全部记录，获取最新记录的办理状态

        :param str nsrsbh: 纳税人识别号
        :param str swsxDm: 税务事项代码
        :return: str zxzt: 最新状态
        """
        print(f'正在获取纳税人识别号：{nsrsbh}，税务事项代码：{swsxDm}的事项最新状态')

        search_data = requests.post(
            self.addr,
            headers=self.make_header(),
            data=self.get_search_params(
                swry='',
                extra_data=False,
                pageIndex=0,  # 找最新状态只需要第一页结果
                pageSize=1,  # 找最新状态只需要最新结果
                nsrsbh=nsrsbh,
                swsxDm=swsxDm,
                blztDm='',  # 输入为空获取所有状态
                swryszswjg='14401060000'  # 改为天河区税务所
            )
        )
        if search_data.json()['data']:
            zxzt = search_data.json()['data'][0]['blztMc']
        else:
            print(search_data.json())
            zxzt = '没有结果'
        return zxzt

    def add_zxzt(self, all_json):
        """
        向获得的初始数据中添加最新状态数据

        :param list[dict[str,list[dict[str]]]] all_json: 第一次查询得到的json
        :return:
            [
                {
                    '税务人员': '',
                    '事项': [
                        {
                            ...
                            '最新状态': zxzt
                        },
                    ]
                },
            ]
        """
        print('正在添加最新状态到所有不予受理事项')

        result = all_json
        # 遍历每个税务人员
        for i, swry in enumerate(result):
            # 根据纳税人识别号和税务事项代码去重
            nsrsbh_swsxDm = []
            for sx in swry['事项']:
                if (sx['纳税人识别号'], sx['税务事项代码']) not in nsrsbh_swsxDm:
                    nsrsbh_swsxDm.append((sx['纳税人识别号'], sx['税务事项代码']))
            # 获取组合参数去重后的事项最新状态, 结构为((nsrsbh,swsxDm), zxzt)
            zxzt_list = []
            for j in nsrsbh_swsxDm:
                zxzt = self.grab_zxzt(nsrsbh=j[0], swsxDm=j[1])
                zxzt_list.append((j, zxzt))
            # 遍历每个事项，添加最新状态到每一个事项
            for k, sx in enumerate(swry['事项']):
                for zt in zxzt_list:
                    if (sx['纳税人识别号'], sx['税务事项代码']) == zt[0]:
                        result[i]['事项'][k]['最新状态'] = zt[1]

        print('添加成功')
        return result


class BlyjMixin:
    def grab_blyj(self, rwbh):
        """
        搜索获得办理意见数据

        :param str rwbh: 第一次搜索获得的结果里的任务编号字段
        :return: dict result: 返回的结果字典
        """
        print(f'正在获取{rwbh}的办理意见')

        search_data = requests.post(
            self.query_host + self.query_blyj_api + '?gnmkId=' + self.query_gnmkId,
            headers=self.make_header(),
            data=f'rwbh={rwbh}'
        )
        blyj = search_data.json()['data']['blyj']

        print('获取成功')
        return blyj

    def add_blyj(self, all_json):
        """
        向获得的初始数据中添加办理意见数据

        :param list[dict[str,list[dict[str]]]] all_json: 第一次查询得到的json
        :return:
            [
                {
                    '税务人员': '',
                    '事项': [
                        {
                            ...
                            '办理意见': 'blyj',
                        },
                    ]
                },
            ]
        """
        result = all_json
        for index_swry, swry in enumerate(result):
            for index_sx, sx in enumerate(swry['事项']):
                rwbh = sx['任务编号']
                blyj = self.grab_blyj(rwbh)
                result[index_swry]['事项'][index_sx]['办理意见'] = blyj
        return result


class ProcessAllJsonMixin:
    @staticmethod
    def process_all_json(all_json):
        """
        处理第二次搜索获得的数据，
        统计相同事项名称相同申请人出现大于三次的事项，
        输出这些事项的所有信息包括办理意见

        :param list[dict[str,list[dict[str]]]] all_json: 第二次搜索获得的数据
        :return:
            [
                {
                    '事项名称': 'swsxMc',
                    '税务事项代码': 'swsxDm',
                    '纳税人识别号': 'sqr',
                    '纳税人名称': 'nsrmc',
                    '不予受理次数': int,
                    '详细信息': [
                        {
                            '主管税务所': 'zgswskfj',
                            '受理人': 'blryMc',
                            '申请日期': 'cjsj',
                            '受理日期': 'blsj',
                            '办理期限': 'blqx',
                            '办结时间': 'bjsj',
                            '实名信息-姓名': 'smxxXm',
                            '是否逾期办理': 'sfyqblMc',
                            '任务编号': 'rwbh',
                            '最新状态': zxzt
                            '办理意见': 'blyj',
                        },
                    ]
                },
            ]
        """
        # 合并所有事项
        data = []
        for swry in all_json:
            data.extend(swry['事项'])

        # 去重事项列表，获得相同事项且纳税人为同一个人的列表
        sx_sorted = []
        for sx in data:
            sxmc, sxdm, sqr, nsrmc = sx['事项名称'], sx['税务事项代码'], sx['纳税人识别号'], sx['纳税人名称']
            if (sxmc, sxdm, sqr, nsrmc) not in sx_sorted:
                sx_sorted.append((sxmc, sxdm, sqr, nsrmc))

        # 构建输出结果
        result = []

        # 构建基本信息
        for sx in sx_sorted:
            result_sx = {
                '事项名称': sx[0],
                '税务事项代码': sx[1],
                '纳税人识别号': sx[2],
                '纳税人名称': sx[3],
                '不予受理次数': 0,
                '详细信息': [],
            }

            # 构建详细信息
            for sx_raw in data:
                result_sx_xxxx = {}
                if (sx_raw['事项名称'], sx_raw['纳税人识别号']) == (sx[0], sx[2]):
                    result_sx['不予受理次数'] += 1
                    result_sx_xxxx = {
                        '主管税务所': sx_raw['主管税务所'],
                        '受理人': sx_raw['受理人'],
                        '申请日期': sx_raw['申请日期'],
                        '受理日期': sx_raw['受理日期'],
                        '办理期限': sx_raw['办理期限'],
                        '办结时间': sx_raw['办结时间'],
                        '实名信息-姓名': sx_raw['实名信息-姓名'],
                        '是否逾期办理': sx_raw['是否逾期办理'],
                        '任务编号': sx_raw['任务编号'],
                        '最新状态': sx_raw['最新状态'],
                        '办理意见': sx_raw['办理意见'],
                    }
                if result_sx_xxxx:
                    result_sx['详细信息'].append(result_sx_xxxx)

            result.append(result_sx)

        # 进一步筛选出不予受理次数大于等于三次的结果
        result_sorted = [i for i in result if i['不予受理次数'] >= 3]

        return result_sorted


class InsertTheadMixin:
    @staticmethod
    def insert_thead(sheet):
        """
        录入表头，原始数据格式

        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        """
        # 录入基础数据表头
        sheet.write(0, 0, '纳税人名称')
        sheet.write(0, 1, '事项名称')
        sheet.write(0, 2, '纳税人识别号')
        sheet.write(0, 3, '不予受理次数')

        # 录入详细信息表头
        sheet.write(0, 4, '主管税务所')
        sheet.write(0, 5, '受理人')
        sheet.write(0, 6, '申请日期')
        sheet.write(0, 7, '受理日期')
        sheet.write(0, 8, '办理期限')
        sheet.write(0, 9, '办结时间')
        sheet.write(0, 10, '实名信息-姓名')
        sheet.write(0, 11, '是否逾期办理')
        sheet.write(0, 12, '最新状态')
        sheet.write(0, 13, '办理意见')


class InsertDataMixin:
    @staticmethod
    def insert_data(data, sheet, pos_index=None):
        """
        录入数据

        :param list data: 数据
        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        :param pos_index: 表格录入位置参照索引
        """
        xxxx = data['详细信息']
        for j, i in enumerate(xxxx):
            # 录入基础数据
            sheet.write(pos_index + j + 1, 0, data['纳税人名称'])
            sheet.write(pos_index + j + 1, 1, data['事项名称'])
            sheet.write(pos_index + j + 1, 2, data['纳税人识别号'])
            sheet.write(pos_index + j + 1, 3, data['不予受理次数'])
            # 录入详细信息
            sheet.write(pos_index + j + 1, 4, i['主管税务所'])
            sheet.write(pos_index + j + 1, 5, i['受理人'])
            sheet.write(pos_index + j + 1, 6, i['申请日期'])
            sheet.write(pos_index + j + 1, 7, i['受理日期'])
            sheet.write(pos_index + j + 1, 8, i['办理期限'])
            sheet.write(pos_index + j + 1, 9, i['办结时间'])
            sheet.write(pos_index + j + 1, 10, i['实名信息-姓名'])
            sheet.write(pos_index + j + 1, 11, i['是否逾期办理'])
            sheet.write(pos_index + j + 1, 12, i['最新状态'])
            sheet.write(pos_index + j + 1, 13, i['办理意见'])


class BYSL(InputParamsMixin,
           SearchParamsMixin,
           GetAllJsonMixin,
           ZxztMixin,
           BlyjMixin,
           ProcessAllJsonMixin,
           InsertTheadMixin,
           InsertDataMixin,
           YBSX):
    """
    已办事项_不予受理统计
    统计同一个申请人同一个事项不予受理大于等于三次的事件
    """
    version = '3.4.2'

    def __init__(self,
                 sx_url,
                 query_host,
                 query_api,
                 query_gnmkId,
                 query_blyj_api):
        super().__init__(sx_url,
                         query_host,
                         query_api,
                         query_gnmkId)

        self.query_blyj_api = query_blyj_api

    def write_excel(self, csv_filename):
        """
        输出excel文件

        :param str csv_filename: 税务人员列表csv
        """
        # 创建excel文件
        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(
            f'results/BYSL_{self.BLRQSTARTDATE + "-" + self.BLRQENDDATE}.xlsx')

        data = self.process_all_json(
            self.add_blyj(
                self.add_zxzt(
                    self.get_all_json(csv_filename)
                )
            )
        )

        # 创建工作表
        sheet = workbook.add_worksheet('不予受理统计')

        # 录入表头
        self.insert_thead(sheet)

        pos_index = 0
        for sx in data:
            # 录入数据
            self.insert_data(sx, sheet, pos_index)

            pos_index += len(sx['详细信息'])

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main():
    filenames = os.listdir('swry')
    for index in range(len(filenames)):
        filenames[index] = filenames[index].replace('.csv', '')

    bysl = BYSL(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1',
        query_blyj_api='ybsx_queryYbsxxq.do'
    )
    for file in filenames:
        bysl.write_excel(file)


if __name__ == '__main__':
    print('\033[32m已办事项_不予受理\033[0m')
    try:
        main()
    except Exception:
        print(traceback.format_exc())
    finally:
        input('回车键退出')
