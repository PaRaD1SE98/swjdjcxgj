import csv
import os

import execjs
import requests
import stdiomask
from lxml import etree

from DianJu.LoginMd5 import get_md5
from DianJu.exceptions import LoginError, LoginConnectionError, AccountError, AccountNotSetError
from SwjDjCxgj import DEBUG


class BaseInitMixin:
    def __init__(self, sx_url, query_host, query_api, query_gnmkId):
        self.print_version()
        self.query_host = query_host
        self.query_api = query_api
        self.query_gnmkId = query_gnmkId
        self.sx_url = sx_url + '?gnmkId=' + query_gnmkId
        self.addr = query_host + query_api + '?gnmkId=' + query_gnmkId
        self.username = None
        self.password = None
        self.cookie = self.login()


class VersionMixin:
    version = '3.7.0'

    def print_version(self):
        print(f'\033[32m版本：{self.version}\033[0m')


class LoginMixin:
    @staticmethod
    def read_account(param):
        with open('account.txt', 'r') as account:
            content = account.readlines()
            if content:
                if 'USERNAME' not in content[0]:
                    if 'USERNAME' not in content[1]:
                        raise AccountError('USERNAME')
                if 'PASSWORD' not in content[0]:
                    if 'PASSWORD' not in content[1]:
                        raise AccountError('PASSWORD')
                for var in content:
                    if param in var:
                        var = var.replace(param + '=', '').replace('\n', '').replace('\r\n', '')
                        return var
            else:
                raise AccountNotSetError

    @staticmethod
    def write_account(username, password):
        with open('account.txt', 'w') as account:
            account.write('USERNAME=' + username + '\n')
            account.write('PASSWORD=' + password)
        print('帐号密码保存成功')

    def login(self):
        """
        通过直接访问查询页面获得跳转登陆页面并登陆
        """
        try:
            username = self.read_account('USERNAME')
            password = self.read_account('PASSWORD')
        except FileNotFoundError:
            print('可以将用户名密码按格式:\n'
                  'USERNAME=xxx\n'
                  'PASSWORD=yyy\n'
                  '手动写入到程序相同文件夹下account.txt文件中\n'
                  '也可以运行保存登陆账号密码程序自动设置')
            username = input('用户名：')
            password = stdiomask.getpass('密码：')
        except AccountError as err:
            print(str(err))
            username = input('用户名：')
            password = stdiomask.getpass('密码：')
        except AccountNotSetError as err:
            print(str(err))
            username = input('用户名：')
            password = stdiomask.getpass('密码：')
        except:
            username = input('用户名：')
            password = stdiomask.getpass('密码：')
        finally:
            self.username = username
            self.password = password
        if DEBUG:
            return 'some_cookie'
        try:
            s = requests.session()
            sx_login_page = s.get(self.sx_url)
        except requests.exceptions.ConnectionError:
            raise LoginConnectionError

        wb_pg = etree.HTML(sx_login_page.text)
        try:
            lt = wb_pg.xpath('//*[@id="upLoginForm"]/input[@name="lt"]//@value')[0]
        except IndexError:
            if os.path.exists('account.txt'):
                os.remove('account.txt')
            # print('登陆失败，已删除保存的帐号信息，运行 保存登陆账号密码 程序重新保存')
            raise LoginError
        execution = wb_pg.xpath('//*[@id="upLoginForm"]/input[@name="execution"]//@value')[0]

        # 加密密码
        context = execjs.compile(get_md5())
        password = context.call('md5', password)

        # 登录
        login_data = {
            'lt': lt,
            'execution': execution,
            '_eventId': 'submit',
            'loginName': username,
            'passWord': password,
        }
        login_post = s.post(sx_login_page.url, data=login_data)
        # print(login_post.url)
        login_cookie = {
            'JSESSIONID': login_post.url[-32:]
        }
        s.get(self.sx_url, cookies=login_cookie)
        print('登录成功')
        return login_post.url[-32:]


class HeaderMixin:
    def make_header(self):
        """
        制作http请求头

        :return: http请求头
        :rtype: dict[str]
        """
        cookie_value = self.cookie

        cookie = 'JSESSIONID=' + cookie_value

        header = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Cookie': cookie,
        }
        return header


class ReadSwryCsvMixin:
    @staticmethod
    def read_swry_csv(csv_filename):
        """
        读取swry文件夹的csv文件，输出税务人员代码和名字

        :param str csv_filename: csv文件名不带扩展名
        :return: list member [[税务人员代码， 姓名]]
        :rtype: list[list[str, str]]
        """
        with open(f'swry/{csv_filename}.csv', 'r', encoding='utf-8') as f:
            reader = csv.reader(f, delimiter='\t')
            member = []
            for i in reader:
                member.append(i)
        return member


class ReadShiXiangCsvMixin:
    @staticmethod
    def read_sx_csv(csv_filename):
        """
        读取query_params文件夹的事项csv文件，输出事项名称

        :param str csv_filename: csv文件名不带扩展名
        :return: 事项名称列表
        :rtype: list[str]
        """
        with open(f'query_params/{csv_filename}.csv', 'r', encoding='utf-8') as f:
            reader = csv.reader(f, delimiter='\t')
            member = []
            for i in reader:
                member.append(i[0])
            # print(member)
        return member


class ShiXiang(BaseInitMixin,
               VersionMixin,
               HeaderMixin,
               LoginMixin,
               ReadSwryCsvMixin):
    """
    所有事项的通用模板
    实现登陆功能
    """


class ShiXiangNoInitMixin(VersionMixin,
                          HeaderMixin,
                          LoginMixin,
                          ReadSwryCsvMixin):
    """
    为了方便继承，使用没有__init__方法的类，继承后必须要添加__init__方法
    """


def main():
    """
    保存登陆账号密码小程序
    如果登陆失败，重新运行本程序即可
    或手动更改 account.txt 文件
    """
    SX = ShiXiang(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1'
    )
    SX.write_account(SX.username, SX.password)
