import csv
import datetime
import os

import xlsxwriter

import DianJu
from DianJu.ShuiWuJu import ShiXiang, ReadShiXiangCsvMixin
from DianJu.YBSX_ALL import TotalCountMixin, SearchParamsMixin
from DianJu.exceptions import InputError


class ReadShiXiangDaiMaCsvMixin:
    @staticmethod
    def read_sxdm_csv(csv_filename):
        """
        读取query_params文件夹的csv文件，输出事项代码和名字

        :param str csv_filename: csv文件名不带扩展名
        :return: [
            [税务事项代码， 事项名称],
            ...
        ]
        :rtype: list[list[str, str]]
        """
        with open(f'query_params/{csv_filename}.csv', 'r', encoding='utf-8', newline='') as f:
            reader = csv.reader(f, delimiter=',')
            member = []
            for i in reader:
                member.append(i)
        return member


class SwsxTj(ReadShiXiangCsvMixin,
             ReadShiXiangDaiMaCsvMixin,
             SearchParamsMixin,
             TotalCountMixin,
             ShiXiang):
    """
    已办事项_全年事项受理统计
    用法：
        如有新增事项，在query_params/ShiXiang.csv中添加该事项名称，
        然后运行一次事项代码获取工具再运行本程序
    """
    version = '3.4.0'

    def __init__(self,
                 sx_url,
                 query_host,
                 query_api,
                 query_gnmkId):
        super().__init__(sx_url,
                         query_host,
                         query_api,
                         query_gnmkId)

        # 静态
        self.BLNF = None
        self.BLNY_li = self.get_blny()

        # 动态
        self.BLNY_next = self.BLNY_li[0]

        self.SQSJSTARTDATE = None
        self.SQSJENDDATE = None
        self.BLRQSTARTDATE = None
        self.BLRQENDDATE = None
        self.EXTRA_SQSJSTARTDATE = None
        self.EXTRA_SQSJENDDATE = None
        self.BLNY = None

    def get_blny(self):
        """
        输出办理年月
        :return: [
            '202101',
            ...
            '202112'
        ]
        :rtype: list[str]
        """
        blnf = input('输入办理年份(格式：2021):')
        self.BLNF = blnf
        blyf_li = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
        blny_li = []
        for i in blyf_li:
            blny_li.append(blnf + i)
        return blny_li

    @staticmethod
    def get_input_params(**kwargs):
        """
        获取输入的查询参数

        :keyword str blny: 办理年月
        :return: 申请开始，申请结束，办理开始，办理结束，上月末申请开始，上月末申请结束
        :rtype: tuple[str]
        """
        try:
            blny = kwargs.get('blny')

            def last_day_of_month(any_day):
                """
                获取获得一个月中的最后一天
                :param any_day: 任意日期
                :return: 日期
                :rtype: datetime.date
                """
                next_month = any_day.replace(
                    day=28) + datetime.timedelta(days=4)  # this will never fail
                return next_month - datetime.timedelta(days=next_month.day)

            blrqstartdate = datetime.datetime.strptime(
                blny + "01", "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期开始
            blrqenddate = last_day_of_month(
                datetime.datetime.strptime(blny + '01', "%Y%m%d")
            ).strftime(
                "%Y-%m-%d"
            )  # 办理日期截至
            sqsjstartdate = blrqstartdate
            sqsjenddate = blrqenddate

            extra_sqsjstartDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-8)
            ).strftime(
                '%Y-%m-%d'
            )
            extra_sqsjendDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-1)
            ).strftime(
                '%Y-%m-%d'
            )

            return (sqsjstartdate,
                    sqsjenddate,
                    blrqstartdate,
                    blrqenddate,
                    extra_sqsjstartDate,
                    extra_sqsjendDate,
                    blny)
        except ValueError:
            raise InputError

    def update_input_params(self):
        input_params = self.get_input_params(blny=self.BLNY_next)

        self.SQSJSTARTDATE = input_params[0]
        self.SQSJENDDATE = input_params[1]
        self.BLRQSTARTDATE = input_params[2]
        self.BLRQENDDATE = input_params[3]
        self.EXTRA_SQSJSTARTDATE = input_params[4]
        self.EXTRA_SQSJENDDATE = input_params[5]
        self.BLNY = input_params[6]

        print(
            f'申请时间开始{self.SQSJSTARTDATE}\n'
            f'申请时间截至{self.SQSJENDDATE}\n'
            f'办理时间开始{self.BLRQSTARTDATE}\n'
            f'办理时间截至{self.BLRQENDDATE}\n'
            f'上月末尾申请时间开始{self.EXTRA_SQSJSTARTDATE}\n'
            f'上月末尾申请时间截至{self.EXTRA_SQSJENDDATE}'
        )
        try:
            self.BLNY_next = self.BLNY_li[self.BLNY_li.index(self.BLNY_next) + 1]
        except IndexError:
            self.BLNY_next = self.BLNY_li[0]

    def get_all_json(self):
        """
        获取一年中每个事项每个月的的受理总数
        :return: {
            事项名称1: [0,...,11],
            事项名称2: [0,...,11],
            ...
        }
        :rtype: dict[list[int]]
        """
        all_json = {}
        try:
            swsxDm = self.read_sxdm_csv('ShiXiangDaiMa')
        except FileNotFoundError:
            print('\033[33m还未获取事项代码，开始获取事项代码\033[0m')
            print('\033[32m已办事项_当月事项代码获取工具\033[0m')
            DianJu.YBSX_SwsxDmGenerator()
            swsxDm = self.read_sxdm_csv('ShiXiangDaiMa')

        # 用ShiXiang.csv筛选并排序需要的事项
        # 无论是否符合都不会出错只会缺少
        # 因此需要做交叉检查
        swsxDm_sorted = []
        swsxMc = self.read_sx_csv('ShiXiang')
        for sxmc in swsxMc:
            for dm, mc in swsxDm:
                if mc == sxmc:
                    swsxDm_sorted.append((dm, mc))

        # 双向对照检查，如果事项代码里的名称不在编辑的ShiXiang.csv里，发出警告
        print('开始执行双向检查')
        sx_jcjg = False
        for dm, mc in swsxDm:
            if mc not in swsxMc:
                sx_jcjg = True
                print(f'\033[32m{mc}\033[0m')
        if sx_jcjg:
            print('\033[32m--------------------------------------------------------------------------------\n'
                  '以上事项在ShiXiang.csv中未找到\n'
                  '如不需要统计该事项可忽略，否则要手动添加到ShiXiang.csv中\n'
                  '--------------------------------------------------------------------------------\033[0m')
        # 如果ShiXiang.csv中有名称不在事项代码的名称里，发出警告
        sxdm_jcjg = False
        mc_swsxDm = [mc[1] for mc in swsxDm]
        for mc in swsxMc:
            if mc not in mc_swsxDm:
                sxdm_jcjg = True
                print(f'\033[33m{mc}\033[0m')
        if sxdm_jcjg:
            print('\033[33m--------------------------------------------------------------------------------\n'
                  '以上事项在ShiXiangDaiMa.csv中未找到\n'
                  '可能在执行选项[7]时填写的办理年月的结果中没有记录，也可能已改名或已删除\n'
                  '可以尝试执行选项[7],办理年月填写一个包含需要的事项的时间\n'
                  '或者把正确名字填入ShiXiang.csv中\n'
                  '--------------------------------------------------------------------------------\033[0m')
        print('双向检查完成')

        print('开始收集数据')
        # 写入数据
        for swsx in swsxDm_sorted:
            total_count = []
            for _ in range(12):
                self.update_input_params()
                month_count = self.get_total_count(swsxDm=swsx[0])
                month_count_extra = self.get_total_count(swsxDm=swsx[0], extra_data=True)
                total_count.append(month_count + month_count_extra)
            all_json[swsx[1]] = total_count
        print('收集数据成功')

        return all_json

    def insert_thead(self, sheet, **kwargs):
        """
        录入表头

        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        :keyword data: 数据
        """
        data = kwargs['data']

        # 纵坐标
        for i, sxmc in enumerate(data):
            sheet.write(i + 1, 0, sxmc)
        sheet.write(len(data) + 1, 0, '总数')

        # 横坐标
        for i, blny in enumerate(self.BLNY_li):
            sheet.write(0, i + 1, blny)
        sheet.write(0, len(self.BLNY_li) + 1, '总数')

    def insert_data(self, sheet, data):
        """

        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        :param dict data: 数据
        :return:
        """
        # 写入数据和单事项统计
        for i_sxmc, sxmc in enumerate(data):
            for i_blny, _ in enumerate(self.BLNY_li):
                sheet.write(i_sxmc + 1, i_blny + 1, data[sxmc][i_blny])
                # 单事项统计
                total_sxmc = sum(data[sxmc])
                sheet.write(i_sxmc + 1, len(self.BLNY_li) + 1, total_sxmc)

        # 单月统计
        for i_blny, _ in enumerate(self.BLNY_li):
            total_month = 0
            for i_sxmc, sxmc in enumerate(data):
                total_month += data[sxmc][i_blny]
            sheet.write(len(data) + 1, i_blny + 1, total_month)

        # 总数统计
        all_sum = 0
        for i in data:
            all_sum += sum(data[i])
        sheet.write(len(data) + 1, len(self.BLNY_li) + 1, all_sum)

    def write_excel(self):
        data = self.get_all_json()

        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(f'results/税务事项年度统计_{self.BLNF}.xlsx')
        sheet = workbook.add_worksheet('税务事项年度统计表')
        print('开始写入Excel')
        self.insert_thead(sheet, data=data)
        self.insert_data(sheet, data=data)
        workbook.close()
        print('写入Excel成功')


def main():
    swsxtj = SwsxTj(sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
                    query_host='http://app2.dzgzpt.gdgs.tax/',
                    query_api='ybsx_queryYbsx.do',
                    query_gnmkId='352B0F8EF3280008E053C0A81418DFD1')
    swsxtj.write_excel()
