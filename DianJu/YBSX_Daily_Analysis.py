import datetime
import os

import xlsxwriter

from DianJu.YBSX_Daily_Origin import YbsxDailyOrigin


class YbsxDailyAnalysis(YbsxDailyOrigin):
    """
    已办事项_日结数据分析
    分析税务人员事项办理的效率

    维度：
        最早办理时间
        最新办理时间
        已办理数据量
        平均办理间隔（已删除大于一小时的时间间隔，午休时间）
        最大办理间隔（已删除最大值后的数据）
        最多办理事项
    """
    version = '3.7.3'

    @staticmethod
    def get_swry_data(all_json, swryDm):
        """
        过滤出当前税务人员的所有事项

        :rtype: list[dict[str]]
        """
        swry_data = []
        for i in all_json:
            if i['受理人代码'] == swryDm:
                swry_data.append(i)
        return swry_data

    @staticmethod
    def get_earliest_time(swry_data):
        """
        获取特定税务人员的最早办理记录时间

        :param list[dict[str]] swry_data: 过滤后的单个税务人员的数据
        :rtype: str
        """
        datetime_li = []
        for i in swry_data:
            datetime_str = i['办结时间']
            datetime_obj = datetime.datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S')
            datetime_li.append(datetime_obj)
        try:
            earliest_datetime = min(datetime_li).strftime('%Y-%m-%d %H:%M:%S')
        except ValueError:
            earliest_datetime = '空'
        return earliest_datetime

    @staticmethod
    def get_latest_time(swry_data):
        """
        获取特定税务人员的最新办理记录时间

        :param list[dict[str]] swry_data: 过滤后的单个税务人员的数据
        :rtype: str
        """
        datetime_li = []
        for i in swry_data:
            datetime_str = i['办结时间']
            datetime_obj = datetime.datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S')
            datetime_li.append(datetime_obj)
        try:
            latest_datetime = max(datetime_li).strftime('%Y-%m-%d %H:%M:%S')
        except ValueError:
            latest_datetime = '空'
        return latest_datetime

    @staticmethod
    def get_sorted_sx_time(all_json):
        """
        获取从旧到新排序后的时间列表

        :param list[dict[str]] all_json: 通用数据
        :return: 排序后的时间对象列表
        :rtype:list[datetime.datetime]
        """
        datetime_obj_li = []
        for i in all_json:
            datetime_str = i['办结时间']
            datetime_obj = datetime.datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S')
            datetime_obj_li.append(datetime_obj)
        datetime_obj_li = sorted(datetime_obj_li)
        return datetime_obj_li

    @staticmethod
    def get_time_gaps(time_list):
        """
        获取相邻时间差

        :param list[datetime.datetime] time_list: 时间对象列表
        :return: 时间差列表
        :rtype: list[datetime.timedelta]
        """
        time_gaps_li = [time_list[i + 1] - time_list[i] for i in range(len(time_list) - 1)]
        return time_gaps_li

    @staticmethod
    def get_favourite_sx(swry_data):
        """
        获取特定税务人员办理最多的事项

        :param list[dict[str]] swry_data: 过滤后的单个税务人员的数据
        :rtype: str
        """
        all_sxmc = []
        for i in swry_data:
            all_sxmc.append(i['事项名称'])
        # 返回列表中最多的元素，如果两个元素数量相同，则返回靠前的元素
        try:
            max_sx = max(all_sxmc, key=all_sxmc.count)
        except ValueError:
            max_sx = '空'
        return max_sx

    def person_analysis(self):
        """
        分析每个税务人员的数据

        :return:
            [
                {
                    '税务人员姓名': person[1],
                    '最早办理时间': earliest_time,
                    '最新办理时间': latest_time,
                    '已办理数据量': len(swry_data),
                    '办理平均间隔': average_interval,
                    '最大办理间隔': str(max(time_gaps)),
                    '办理最多事项': favourite_sx
                }
            ]
        :rtype: list[dict[str]]
        """
        data = self.get_all_json()
        swry = self.read_swry_csv('swry')
        all_output = []
        for person in swry:
            swry_data = self.get_swry_data(data, person[0])

            earliest_time = self.get_earliest_time(swry_data)
            latest_time = self.get_latest_time(swry_data)

            time_list = self.get_sorted_sx_time(swry_data)
            time_gaps = self.get_time_gaps(time_list)
            try:
                if max(time_gaps) > datetime.timedelta(hours=1):
                    time_gaps.remove(max(time_gaps))  # 删除大于一小时的时间间隔（午休）
                average_interval = str(sum(time_gaps, datetime.timedelta()) / len(time_gaps))
            except ValueError:
                average_interval = '空'
            except ZeroDivisionError:
                average_interval = '空'
            try:
                max_interval = str(max(time_gaps))
            except ValueError:
                max_interval = '空'

            favourite_sx = self.get_favourite_sx(swry_data)
            swry_output = {
                '税务人员姓名': person[1],
                '最早办理时间': earliest_time,
                '最新办理时间': latest_time,
                '已办理数据量': len(swry_data),
                '办理平均间隔': average_interval,
                '最大办理间隔': max_interval,
                '办理最多事项': favourite_sx
            }
            all_output.append(swry_output)
        return all_output

    @staticmethod
    def zero_filter(swry_data):
        """
        过滤掉没有数据的税务人员
        :param list[dict[str]] swry_data: 单个税务人员的数据
        :rtype: list[dict[str]]
        """
        result = [i for i in swry_data if i['已办理数据量'] != 0]
        return result

    @staticmethod
    def insert_thead(sheet):
        sheet.write(0, 0, '税务人员姓名')
        sheet.write(0, 1, '最早办理时间')
        sheet.write(0, 2, '最新办理时间')
        sheet.write(0, 3, '已办理数据量')
        sheet.write(0, 4, '办理平均间隔')
        sheet.write(0, 5, '最大办理间隔')
        sheet.write(0, 6, '办理最多事项')

    @staticmethod
    def insert_data(data, sheet):
        for index, i in enumerate(data):
            sheet.write(index + 1, 0, i['税务人员姓名'])
            sheet.write(index + 1, 1, i['最早办理时间'])
            sheet.write(index + 1, 2, i['最新办理时间'])
            sheet.write(index + 1, 3, i['已办理数据量'])
            sheet.write(index + 1, 4, i['办理平均间隔'])
            sheet.write(index + 1, 5, i['最大办理间隔'])
            sheet.write(index + 1, 6, i['办理最多事项'])

    def write_excel(self):
        """
        输出excel文件
        """
        if not os.path.exists('results'):
            os.mkdir('results')
        # 创建excel文件
        workbook = xlsxwriter.Workbook('results/已办事项日结细节_税务人员数据分析.xlsx')
        sheet = workbook.add_worksheet('数据分析')
        data = self.zero_filter(self.person_analysis())
        self.insert_thead(sheet)
        self.insert_data(data, sheet)
        workbook.close()
        print('写入Excel成功')


def main():
    ybsx_daily_analysis = YbsxDailyAnalysis(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1',
        query_blyj_api='ybsx_queryYbsxxq.do',
        query_spyj_api='ybsx_queryBlyj.do'
    )
    print('正在分析')
    ybsx_daily_analysis.write_excel()
