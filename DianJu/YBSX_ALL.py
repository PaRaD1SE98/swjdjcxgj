import datetime
import json
import os
import traceback

import requests
import xlsxwriter

from DianJu.ShuiWuJu import ShiXiang
from DianJu.exceptions import DataObtainError, InputError


class SpyjMixin:
    def grab_spyjxx(self, sqxh):
        """
        搜索获得审批意见信息

        :param str sqxh: 第一次搜索获得的结果里的申请序号字段
        :return: 返回的结果
        :rtype: str
        """
        print(f'正在获取{sqxh}的审批意见')
        try:
            search_data = requests.post(
                self.query_host + self.query_spyj_api + '?gnmkId=' + self.query_gnmkId + '&sqxh=' + sqxh,
                headers=self.make_header(),
            )
            spyj = search_data.json()[1]['spyjxx']
        except Exception:
            spyj = ''

        print('获取成功')
        return spyj

    def add_spyjxx(self, all_json):
        """
        向获得的初始数据中添加审批意见信息

        :param list[dict[str]] all_json: 第一次查询得到的json
        :return:
            [
                {
                    ...
                    '审批意见': 'spyj',
                },
            ]
        :rtype: list[dict[str]]
        """
        print('正在添加审批意见')

        result = all_json
        for index_sx, sx in enumerate(result):
            sqxh = sx['申请序号']
            spyj = self.grab_spyjxx(sqxh)
            result[index_sx]['审批意见'] = spyj

        print('添加成功')
        return result


class BlyjMixin:
    def grab_blyj(self, rwbh):
        """
        搜索获得办理意见数据

        :param str rwbh: 第一次搜索获得的结果里的任务编号字段
        :return: 返回的结果
        :rtype: str
        """
        print(f'正在获取{rwbh}的办理意见')

        search_data = requests.post(
            self.query_host + self.query_blyj_api + '?gnmkId=' + self.query_gnmkId,
            headers=self.make_header(),
            data=f'rwbh={rwbh}'
        )
        blyj = search_data.json()['data']['blyj']

        print('获取成功')
        return blyj

    def add_blyj(self, all_json):
        """
        向获得的初始数据中添加办理意见数据

        :param list[dict[str]] all_json: 第一次查询得到的json
        :return:
            [
                {
                    ...
                    '办理意见': 'blyj',
                },
            ]
        :rtype: list[dict[str]]
        """
        print('正在添加办理意见')

        result = all_json
        for index_sx, sx in enumerate(result):
            rwbh = sx['任务编号']
            blyj = self.grab_blyj(rwbh)
            result[index_sx]['办理意见'] = blyj

        print('添加成功')
        return result


class ExcelFormatMixin:
    @staticmethod
    def insert_thead(sheet):
        """
        录入表头，原始数据格式

        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        """
        # 录入基础数据表头
        sheet.write(0, 0, '纳税人名称')
        sheet.write(0, 1, '事项名称')
        sheet.write(0, 2, '纳税人识别号')
        sheet.write(0, 3, '主管税务所')
        sheet.write(0, 4, '受理人')
        sheet.write(0, 5, '申请日期')
        sheet.write(0, 6, '受理日期')
        sheet.write(0, 7, '办理期限')
        sheet.write(0, 8, '办结时间')
        sheet.write(0, 9, '实名信息-姓名')
        sheet.write(0, 10, '是否逾期办理')
        sheet.write(0, 11, '办理意见')
        sheet.write(0, 12, '审批意见')

    @staticmethod
    def insert_data(data, sheet):
        """
        录入数据

        :param data: 数据
        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        """
        for index, i in enumerate(data):
            sheet.write(index + 1, 0, i['纳税人名称'])
            sheet.write(index + 1, 1, i['事项名称'])
            sheet.write(index + 1, 2, i['纳税人识别号'])
            sheet.write(index + 1, 3, i['主管税务所'])
            sheet.write(index + 1, 4, i['受理人'])
            sheet.write(index + 1, 5, i['申请日期'])
            sheet.write(index + 1, 6, i['受理日期'])
            sheet.write(index + 1, 7, i['办理期限'])
            sheet.write(index + 1, 8, i['办结时间'])
            sheet.write(index + 1, 9, i['实名信息-姓名'])
            sheet.write(index + 1, 10, i['是否逾期办理'])
            sheet.write(index + 1, 11, i['办理意见'])
            sheet.write(index + 1, 12, i['审批意见'])


class InputParamsMixin:
    @staticmethod
    def get_input_params():
        """
        获取输入的查询参数

        :return: 申请开始，申请结束，办理开始，办理结束，上月末申请开始，上月末申请结束
        :rtype: tuple[str]
        """
        try:
            blny = input('办理年月（格式：202101）:')

            def last_day_of_month(any_day):
                """
                获取获得一个月中的最后一天
                :param any_day: 任意日期
                :return: 日期
                :rtype: datetime.date
                """
                next_month = any_day.replace(
                    day=28) + datetime.timedelta(days=4)  # this will never fail
                return next_month - datetime.timedelta(days=next_month.day)

            blrqstartdate = datetime.datetime.strptime(
                blny + "01", "%Y%m%d").strftime("%Y-%m-%d")  # 办理日期开始
            blrqenddate = last_day_of_month(
                datetime.datetime.strptime(blny + '01', "%Y%m%d")
            ).strftime(
                "%Y-%m-%d"
            )  # 办理日期截至
            sqsjstartdate = blrqstartdate
            sqsjenddate = blrqenddate

            extra_sqsjstartDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-8)
            ).strftime(
                '%Y-%m-%d'
            )
            extra_sqsjendDate = (
                    datetime.datetime.strptime(blrqstartdate, '%Y-%m-%d') + datetime.timedelta(days=-1)
            ).strftime(
                '%Y-%m-%d'
            )

            print(
                f'申请时间开始{sqsjstartdate}\n'
                f'申请时间截至{sqsjenddate}\n'
                f'办理时间开始{blrqstartdate}\n'
                f'办理时间截至{blrqenddate}\n'
                f'上月末尾申请时间开始{extra_sqsjstartDate}\n'
                f'上月末尾申请时间截至{extra_sqsjendDate}'
            )
            return (sqsjstartdate,
                    sqsjenddate,
                    blrqstartdate,
                    blrqenddate,
                    extra_sqsjstartDate,
                    extra_sqsjendDate,
                    blny)
        except ValueError:
            raise InputError


class SearchParamsMixin:
    def get_search_params(self,
                          pageIndex,
                          nsrsbh='',
                          swsxDm='',
                          blztDm='',
                          swryszswjg='',
                          swry='',
                          sfyqbl='',
                          jrqd='',
                          pageSize=100,
                          sortField='',
                          sortOrder='',
                          extra_data=False):
        """
        获取搜索参数

        :param int pageIndex: 页码
        :param str nsrsbh: 纳税人识别号
        :param str swsxDm: 税务事项代码
        :param str blztDm: 办理状态代码 不予受理: 02 已受理： 12
        :param str swryszswjg: 税务人员所在税务机关代码
        :param str swry: 税务人员
        :param sfyqbl: 是否逾期办理， 类型未知
        :param int pageSize: 每页返回数量，小于等于100
        :param jrqd: 未知参数
        :param sortField: 筛选栏目
        :param sortOrder: 排列顺序
        :param bool extra_data: 是否查询上月申请本月办理的数据
        :return: 格式化后的查询参数
        :rtype: str
        """
        data = f'nsrsbh={nsrsbh}' + \
               f'&swsxDm={swsxDm}' + \
               f'&blztDm={blztDm}' + \
               f'&swryszswjg={swryszswjg}' + \
               f'&swry={swry}' + \
               f'&sfyqbl={sfyqbl}' + \
               f'&sqsjstartDate={self.SQSJSTARTDATE if not extra_data else self.EXTRA_SQSJSTARTDATE}' + \
               f'&sqsjendDate={self.SQSJENDDATE if not extra_data else self.EXTRA_SQSJENDDATE}' + \
               f'&blrqstartDate={self.BLRQSTARTDATE}' + \
               f'&blrqendDate={self.BLRQENDDATE}' + \
               f'&jrqd={jrqd}' + \
               f'&pageIndex={pageIndex}' + \
               f'&pageSize={pageSize}' + \
               f'&sortField={sortField}' + \
               f'&sortOrder={sortOrder}'
        return data


class TotalCountMixin:
    swj = '14401060000'

    def get_total_count(self,
                        nsrsbh='',
                        swsxDm='',
                        blztDm='',
                        swryszswjg=swj,
                        swry='',
                        sfyqbl='',
                        jrqd='',
                        sortField='',
                        sortOrder='',
                        extra_data=False):
        """
        获取总数据数量

        :param str nsrsbh: 纳税人识别号
        :param str swsxDm: 税务事项代码
        :param str blztDm: 办理状态代码 不予受理: 02 已受理： 12
        :param str swryszswjg: 税务人员所在税务机关代码
        :param str swry: 税务人员
        :param sfyqbl: 是否逾期办理， 类型未知
        :param jrqd: 未知参数
        :param sortField: 筛选栏目
        :param sortOrder: 排列顺序
        :param bool extra_data: 是否查询上月申请本月办理的数据
        :return: 结果总数
        :rtype: int
        """
        try:
            search_data = requests.post(
                self.addr,
                headers=self.make_header(),
                data=self.get_search_params(nsrsbh=nsrsbh,
                                            swsxDm=swsxDm,
                                            blztDm=blztDm,
                                            swryszswjg=swryszswjg,
                                            swry=swry,
                                            sfyqbl=sfyqbl,
                                            jrqd=jrqd,
                                            sortField=sortField,
                                            sortOrder=sortOrder,
                                            extra_data=extra_data,
                                            pageIndex=0,
                                            pageSize=1)
            )
            result = search_data.json()['total']
            return int(result)
        except json.decoder.JSONDecodeError:
            raise DataObtainError


class All(InputParamsMixin,
          SearchParamsMixin,
          TotalCountMixin,
          BlyjMixin,
          SpyjMixin,
          ExcelFormatMixin,
          ShiXiang):
    """
    已办事项原始数据
    不筛选税务人员，输出全部数据
    输出平台原格式数据，并添加办理意见
    """
    version = '3.4.0'
    swj = '14401060000'

    def __init__(self,
                 sx_url,
                 query_host,
                 query_api,
                 query_gnmkId,
                 query_blyj_api,
                 query_spyj_api):
        super().__init__(sx_url,
                         query_host,
                         query_api,
                         query_gnmkId)

        input_params = self.get_input_params()
        self.SQSJSTARTDATE = input_params[0]
        self.SQSJENDDATE = input_params[1]
        self.BLRQSTARTDATE = input_params[2]
        self.BLRQENDDATE = input_params[3]
        self.EXTRA_SQSJSTARTDATE = input_params[4]
        self.EXTRA_SQSJENDDATE = input_params[5]
        self.BLNY = input_params[6]

        self.query_blyj_api = query_blyj_api
        self.query_spyj_api = query_spyj_api

    def grab_data(self,
                  page,
                  nsrsbh='',
                  swsxDm='',
                  blztDm='',
                  swryszswjg=swj,
                  swry='',
                  sfyqbl='',
                  jrqd='',
                  sortField='',
                  sortOrder='',
                  extra_data=False):
        """
        获取某一页的数据

        :param int page: 页码索引（从0开始）

        :param str nsrsbh: 纳税人识别号
        :param str swsxDm: 税务事项代码
        :param str blztDm: 办理状态代码 不予受理: 02 已受理： 12
        :param str swryszswjg: 税务人员所在税务机关代码
        :param str swry: 税务人员
        :param sfyqbl: 是否逾期办理， 类型未知
        :param jrqd: 未知参数
        :param sortField: 筛选栏目
        :param sortOrder: 排列顺序
        :param bool extra_data: 是否查询多余的上月数据
        :return: json数据
        :rtype: list[dict[str]]
        """
        try:
            search_data = requests.post(
                self.addr,
                headers=self.make_header(),
                data=self.get_search_params(nsrsbh=nsrsbh,
                                            swsxDm=swsxDm,
                                            blztDm=blztDm,
                                            swryszswjg=swryszswjg,
                                            swry=swry,
                                            sfyqbl=sfyqbl,
                                            jrqd=jrqd,
                                            sortField=sortField,
                                            sortOrder=sortOrder,
                                            extra_data=extra_data,
                                            pageIndex=page)
            )
            result = search_data.json()['data']
            return result
        except json.decoder.JSONDecodeError:
            raise DataObtainError

    def get_all_json(self):
        """
        输出全部数据

        :return:
            [
                {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '主管税务所': data[j]['zgswskfj'],(可能出错)
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                    '任务编号': data[j]['rwbh'],
                    '税务事项代码': data[j]['swsxDm'],
                    '申请序号': data[j]['sqxh'],
                },
            ]
        :rtype: list[dict[str]]
        """
        print('开始获取数据')
        all_json = []
        total_count = self.get_total_count()
        total_count_extra = self.get_total_count(extra_data=True)

        def get_total_page(count):
            PAGE_SIZE = 100
            if count % PAGE_SIZE == 0:
                total = count // PAGE_SIZE
            else:
                total = count // PAGE_SIZE + 1
            return total

        # 总页数（包括最后一页）
        total_page = get_total_page(total_count)

        # 上月末尾总页数（包括最后一页）
        total_page_extra = get_total_page(total_count_extra)

        print(f'总数据量:{total_count},总页数:{total_page}')
        print(f'上月末总数据量:{total_count_extra},总页数{total_page_extra}')

        # 获取每一页的数据
        for i in range(total_page):
            print(f'正在获取第{i + 1}页的数据')
            data = self.grab_data(i)
            # 获取一页中的每条数据
            for j in range(len(data)):
                sx_data = {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                    '任务编号': data[j]['rwbh'],
                    '税务事项代码': data[j]['swsxDm'],
                    '申请序号': data[j]['sqxh'],
                }
                try:
                    sx_data['主管税务所'] = data[j]['zgswskfj']
                except KeyError:
                    sx_data['主管税务所'] = ''
                    print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                all_json.append(sx_data)

        # 获取上月末尾每一页的数据
        for i in range(total_page_extra):
            print(f'正在获取第{i + 1}页的上月末尾数据')
            data = self.grab_data(i, extra_data=True)
            # 获取一页中的每条数据
            for j in range(len(data)):
                sx_data = {
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '受理人': data[j]['blryMc'],
                    '申请日期': data[j]['cjsj'],
                    '受理日期': data[j]['blsj'],
                    '办理期限': data[j]['blqx'],
                    '办结时间': data[j]['bjsj'],
                    '办理状态': data[j]['blztMc'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '是否逾期办理': data[j]['sfyqblMc'],
                    '来源渠道': data[j]['jrqdmc'],
                    '任务编号': data[j]['rwbh'],
                    '税务事项代码': data[j]['swsxDm'],
                    '申请序号': data[j]['sqxh'],
                }
                try:
                    sx_data['主管税务所'] = data[j]['zgswskfj']
                except KeyError:
                    sx_data['主管税务所'] = ''
                    print(f'主管税务所第{i + 1}页第{j + 1}条为空')
                all_json.append(sx_data)

        print('数据获取成功')
        return all_json

    def write_excel(self):
        """
        输出excel文件
        """
        # 创建excel文件
        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(
            f'results/已办事项_原始格式_{self.BLNY}.xlsx'
        )

        data = self.add_spyjxx(
            self.add_blyj(
                self.get_all_json()
            )
        )

        # 创建工作表
        sheet = workbook.add_worksheet('当月全部数据')

        # 录入表头
        self.insert_thead(sheet)
        # 录入数据
        self.insert_data(data, sheet)

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main():
    ybsx_all = All(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/ybsx_sxxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='ybsx_queryYbsx.do',
        query_gnmkId='352B0F8EF3280008E053C0A81418DFD1',
        query_blyj_api='ybsx_queryYbsxxq.do',
        query_spyj_api='ybsx_queryBlyj.do'
    )
    ybsx_all.write_excel()


if __name__ == '__main__':
    print('\033[32m已办事项_全部数据\033[0m')
    try:
        main()
    except Exception:
        print(traceback.format_exc())
    finally:
        input('回车键退出')
