import datetime
import os
import traceback

import requests
import xlsxwriter

from DianJu.ShuiWuJu import ShiXiang, ReadShiXiangCsvMixin
from DianJu.exceptions import InputError


class DBSX(ReadShiXiangCsvMixin, ShiXiang):
    """
    待办事项
    电局原格式数据
    只统计query_params/ShiXiang.csv文件中指定的事项
    """
    version = '3.4.3'

    def __init__(self,
                 sx_url,
                 query_host,
                 query_api,
                 query_gnmkId,
                 query_sfdcsl_api):
        super().__init__(sx_url,
                         query_host,
                         query_api,
                         query_gnmkId)
        self.query_sfdcsl_api = query_sfdcsl_api

        input_params = self.get_input_params()
        self.SQSJSTARTDATE = input_params[0]
        self.SQSJENDDATE = input_params[1]

    @staticmethod
    def get_input_params():
        """
        获取输入的查询参数

        :return: tuple： 申请开始，申请结束
        """
        try:
            sqks = input('申请开始（格式：20210101）:')
            sqjz = input('申请截至（格式：20210101）:')

            sqrqstartdate = datetime.datetime.strptime(
                sqks, "%Y%m%d").strftime("%Y-%m-%d")  # 申请日期开始
            sqrqenddate = datetime.datetime.strptime(
                sqjz, "%Y%m%d").strftime("%Y-%m-%d")  # 申请日期截至
            return sqrqstartdate, sqrqenddate
        except ValueError:
            raise InputError

    @staticmethod
    def get_search_params(pageIndex, pageSize=100, **kwargs):
        """
        获取搜索参数

        :param int pageIndex: 页码
        :param int pageSize: 每页返回数量，小于等于100
        :keyword str nsrsbh: 纳税人识别号
        :keyword str swsxDm: 税务事项代码
        :keyword str swryszswjg: 税务人员所在税务机关代码
        :keyword str ssglyDm: 税收管理员代码
        :keyword str startDate: 申请时间开始
        :keyword str endDate: 申请时间结束
        :keyword str dbsxtjxx: 待办事项统计信息
        :keyword str blqxstartDate: 办理期限开始
        :keyword str blqxendDate: 办理期限结束
        :keyword str qtcxtj: 其他查询条件
        :keyword str jrqd: 未知参数
        :keyword str cxlx: 未知参数 (1)
        :keyword str rwlxDm: 未知参数 (01)
        :keyword str sortField: 筛选栏目
        :keyword str sortOrder: 排列顺序
        :return: 格式化后的查询参数
        :rtype: str
        """
        data = f"nsrsbh={kwargs.get('nsrsbh', '')}" \
               f"&swsxDm={kwargs.get('swsxDm', '')}" \
               f"&swryszswjg={kwargs.get('swryszswjg', '')}" \
               f"&ssglyDm={kwargs.get('ssglyDm', '')}" \
               f"&startDate={kwargs.get('startDate', '')}" \
               f"&endDate={kwargs.get('endDate', '')}" \
               f"&dbsxtjxx={kwargs.get('dbsxtjxx', '')}" \
               f"&blqxstartDate={kwargs.get('blqxstartDate', '')}" \
               f"&blqxendDate={kwargs.get('blqxendDate', '')}" \
               f"&qtcxtj={kwargs.get('qtcxtj', '')}" \
               f"&jrqd={kwargs.get('jrqd', '')}" \
               f"&cxlx={kwargs.get('cxlx', '')}" \
               f"&rwlxDm={kwargs.get('rwlxDm', '')}" \
               f"&pageIndex={pageIndex}" \
               f"&pageSize={pageSize}" \
               f"&sortField={kwargs.get('sortField', '')}" \
               f"&sortOrder={kwargs.get('sortOrder', '')}"
        return data

    def get_total_count(self):
        """
        获取总数据数量

        :return: 结果总数
        :rtype: int
        """
        search_data = requests.post(
            self.addr,
            headers=self.make_header(),
            data=self.get_search_params(startDate=self.SQSJSTARTDATE,
                                        endDate=self.SQSJENDDATE,
                                        cxlx='1',
                                        rwlxDm='01',
                                        pageIndex=0,
                                        pageSize=1)
        )
        result = search_data.json()['total']
        return int(result)

    def grab_data(self, page):
        """
        获取某一页的数据

        :param int page: 页码索引（从0开始）
        :return: json数据
        :rtype: list[dict[str]]
        """
        search_data = requests.post(
            self.addr,
            headers=self.make_header(),
            data=self.get_search_params(startDate=self.SQSJSTARTDATE,
                                        endDate=self.SQSJENDDATE,
                                        cxlx='1',
                                        rwlxDm='01',
                                        pageIndex=page)
        )
        result = search_data.json()['data']
        return result

    def get_all_json(self):
        """
        输出全部数据

        :return:
            [
                {
                    '是否优先': data[j]['sfyxzd'],
                    '事项名称': data[j]['swsxMc'],
                    '税务事项代码': data[j]['swsxDm'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '主管税务所（科，分局）': data[j]['zgswskfj'],
                    '申请日期': data[j]['cjsj'],
                    '办理期限': data[j]['blqx'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '来源渠道': data[j]['jrqdmc'],
                },
            ]
        :rtype: list[dict[str]]
        """
        all_json = []
        total_count = self.get_total_count()

        def get_total_page(count):
            PAGE_SIZE = 100
            if count % PAGE_SIZE == 0:
                total = count // PAGE_SIZE
            else:
                total = count // PAGE_SIZE + 1
            return total

        # 总页数（包括最后一页）
        total_page = get_total_page(total_count)
        print(f'总数据量:{total_count},总页数:{total_page}')

        # 获取每一页的数据
        for i in range(total_page):
            data = self.grab_data(i)
            # 获取一页中的每条数据
            for j, _ in enumerate(data):
                sx_data = {
                    # 去除N标识增加可读性
                    '是否优先': data[j]['sfyxzd'] if not data[j]['sfyxzd'] == 'N' else '',
                    '事项名称': data[j]['swsxMc'],
                    '税务事项代码': data[j]['swsxDm'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '主管税务所（科，分局）': data[j]['swjgskfjmc'],
                    '申请日期': data[j]['cjsj'],
                    '办理期限': data[j]['blqx'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '来源渠道': data[j]['jrqdmc'],
                }
                all_json.append(sx_data)

        print('数据获取成功')
        return all_json

    def grab_sfdcsl(self, nsrsbh, swsxDm):
        """
        搜索获得是否多次不予受理数据

        :param str nsrsbh: 第一次搜索获得的结果里的纳税人识别号字段
        :param str swsxDm: 第一次搜索获得的结果里的税务事项代码字段
        :return: 返回的结果(Y,N)
        :rtype: str
        """
        print(f'正在查询纳税人{nsrsbh}的{swsxDm}事项是否多次不予受理')

        search_data = requests.post(
            self.query_host + self.query_sfdcsl_api + '?gnmkId=' + self.query_gnmkId,
            headers=self.make_header(),
            data=f'nsrsbh={nsrsbh}&swsxDm={swsxDm}'
        )
        sfdcsl = search_data.json()['yjcl']
        # 去除N标识增加可读性
        sfdcsl = '' if sfdcsl == 'N' else sfdcsl

        print('获取成功')
        return sfdcsl

    def add_sfdcsl(self, all_json):
        """
        向获得的初始数据中添加是否多次不予受理数据

        :param list[dict[str]] all_json: 第一次查询得到的json
        :return:
            [
                {
                    ...
                    '是否多次不予受理': 'dcbysl',
                },
            ]
        :rtype: list[dict[str]]
        """
        result = all_json
        for index_sx, sx in enumerate(result):
            nsrsbh = sx['纳税人识别号']
            swsxDm = sx['税务事项代码']
            dcbysl = self.grab_sfdcsl(nsrsbh, swsxDm)
            result[index_sx]['是否多次不予受理'] = dcbysl

        print('添加成功')
        return result

    def sx_filter(self, all_json):
        """
        根据事项列表从数据中筛选出需要的事项

        :param list[dict[str]] all_json: 查询得到的数据
        :return:
            [
                {
                    '是否优先': data[j]['sfyxzd'],
                    '事项名称': data[j]['swsxMc'],
                    '税务事项代码': data[j]['swsxDm'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '主管税务所（科，分局）': data[j]['zgswskfj'],
                    '申请日期': data[j]['cjsj'],
                    '办理期限': data[j]['blqx'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '来源渠道': data[j]['jrqdmc'],
                },
            ]
        :rtype: list[dict[str]]
        """
        new_json = []
        sxmc = self.read_sx_csv('ShiXiang')
        for i, sx in enumerate(all_json):
            if sx['事项名称'] in sxmc:
                new_json.append(all_json[i])
        return new_json

    def time_filter(self, all_json):
        """
        根据事项列表从数据中筛选出规定时间范围的事项

        :param list[dict[str]] all_json: 查询得到的数据
        :return:
            [
                {
                    '是否优先': data[j]['sfyxzd'],
                    '事项名称': data[j]['swsxMc'],
                    '纳税人识别号': data[j]['sqr'],
                    '纳税人名称': data[j]['nsrmc'],
                    '主管税务所（科，分局）': data[j]['zgswskfj'],
                    '申请日期': data[j]['cjsj'],
                    '办理期限': data[j]['blqx'],
                    '实名信息-姓名': data[j]['smxxXm'],
                    '来源渠道': data[j]['jrqdmc'],
                },
            ]
        :rtype: list[dict[str]]
        """
        print('填入精确时间范围，输出范围内结果，不填输出全部结果')

        time_start = input('输入开始时间年月日时分秒（格式：20210101095959）：')
        time_end = input('输入结束时间年月日时分秒（格式：20210101095959）：')

        if time_start is '':
            time_start = datetime.datetime.strptime(f'{self.SQSJSTARTDATE} 00:00:00', '%Y-%m-%d %H:%M:%S')
        else:
            time_start = datetime.datetime.strptime(time_start, "%Y%m%d%H%M%S")

        if time_end is '':
            time_end = datetime.datetime.strptime(f'{self.SQSJENDDATE} 23:59:59', '%Y-%m-%d %H:%M:%S')
        else:
            time_end = datetime.datetime.strptime(time_end, "%Y%m%d%H%M%S")

        new_json = []
        for sx in all_json:
            time_point = datetime.datetime.strptime(sx['申请日期'], "%Y-%m-%d %H:%M:%S")
            if time_start < time_point < time_end:
                new_json.append(sx)

        return new_json

    @staticmethod
    def insert_thead(sheet):
        """
        录入表头，原始数据格式

        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        """
        # 录入基础数据表头
        sheet.write(0, 0, '是否优先')
        sheet.write(0, 1, '是否多次不予受理')
        sheet.write(0, 2, '事项名称')
        sheet.write(0, 3, '纳税人识别号')
        sheet.write(0, 4, '纳税人名称')
        sheet.write(0, 5, '主管税务所（科，分局）')
        sheet.write(0, 6, '申请日期')
        sheet.write(0, 7, '办理期限')
        sheet.write(0, 8, '实名信息-姓名')
        sheet.write(0, 9, '来源渠道')

    @staticmethod
    def insert_data(data, sheet):
        """
        录入数据

        :param data: 数据
        :param xlsxwriter.workbook.Worksheet sheet: 工作表（tab）对象
        """
        for index, i in enumerate(data):
            sheet.write(index + 1, 0, i['是否优先'])
            sheet.write(index + 1, 1, i['是否多次不予受理'])
            sheet.write(index + 1, 2, i['事项名称'])
            sheet.write(index + 1, 3, i['纳税人识别号'])
            sheet.write(index + 1, 4, i['纳税人名称'])
            sheet.write(index + 1, 5, i['主管税务所（科，分局）'])
            sheet.write(index + 1, 6, i['申请日期'])
            sheet.write(index + 1, 7, i['办理期限'])
            sheet.write(index + 1, 8, i['实名信息-姓名'])
            sheet.write(index + 1, 9, i['来源渠道'])

    def write_excel(self, name, data):
        """
        输出excel文件
        :param str name: 输出表的主要名称
        :param list[dict[str]] data: 处理好的数据
        """
        # 创建excel文件
        if not os.path.exists('results'):
            os.mkdir('results')
        workbook = xlsxwriter.Workbook(
            f'results/待办事项_{name}_{self.SQSJSTARTDATE}-{self.SQSJENDDATE}.xlsx'
        )
        # 创建工作表
        sheet = workbook.add_worksheet('待办事项数据')

        # 录入表头
        self.insert_thead(sheet)
        # 录入数据
        self.insert_data(data, sheet)

        # 保存文件
        workbook.close()
        print('写入Excel成功')


def main():
    dbsx = DBSX(
        sx_url='http://app2.dzgzpt.gdgs.tax/dzgzpt/pages/wtgl/dbsx_tjxx.html',
        query_host='http://app2.dzgzpt.gdgs.tax/',
        query_api='dbsx_queryDbsxTjxx.do',
        query_gnmkId='344882DA77640120E053C0A81418D7E6',
        query_sfdcsl_api='dbsx_queryByslCount.do'
    )
    dbsx.write_excel(name='事项筛选',
                     data=dbsx.add_sfdcsl(
                         dbsx.time_filter(
                             dbsx.sx_filter(
                                 dbsx.get_all_json()
                             )
                         )
                     ))


if __name__ == '__main__':
    print('\033[32m待办事项\033[0m')
    try:
        main()
    except Exception:
        print(traceback.format_exc())
    finally:
        input('回车键退出')
